# PHPUnit Extensions

## Powerful and useful tools for constraints and mock-object building

#### General Extensions:
- Constraints Builder
- Mock-Object Builder
- Proxy Factory API

#### For more explanations take a look at src/Tests

```
$ bin/phpunit
```

## Constraints Builder

Constranints building API for any tree-structures

```
use Mia\PHPUnit\Constraint\Tree\Builder\TreeBuilder;

$json = '{
    settings: {
        share: {
            shortenerURL: "http://www.mtv-l.mtvi.com/feeds/bitly",
            twitter: {
                name: "MTV"
            },
            email: {
                subject: "Watch {{title}} on www.mtv.com"
            },
            options: [
                {
                    title: "Facebook",
                    typeClass: "facebook"
                },
                {
                    title: "Twitter",
                    typeClass: "twitter"
                },
                {
                    title: "Email",
                    typeClass: "email"
                },
                {
                    title: "Embed",
                    typeClass: "embed"
                }
            ]
        }
    }
}';

$objectGraph = json_decode($json);

$this->assertSameDefinitionTree(
    $objectGraph,
    function (TreeBuilder $builder) {
        // Configure builder
        $builder->root()
            ->children()
                ->objectNode('settings')
                    ->children()
                        ->objectNode('share')
                            ->children()
                                ->scalarNode('shortenerURL')->end()
                                ->objectNode('twitter')
                                    ->children()
                                        ->scalarNode('name')
                                            ->constraints()
                                                ->assertIsEqual('MTV')
                                                    ->message('Is not equal "MTV"')
                                                ->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                                ->objectNode('email')
                                    ->children()
                                        ->scalarNode('subject')->end()
                                    ->end()
                                ->end()
                                ->arrayNode('options')
                                    ->constraints()
                                        ->assertCount(4)
                                            ->message('Count is not equal 4')
                                        ->end()
                                    ->end()
                                    ->prototype('object')
                                        ->constraints()
                                            ->assertCallback(
                                                function ($other) {
                                                    return $other['typeClass'] === strtolower($other['title']);
                                                }
                                            )
                                                ->message('Properties "title" and "typeClass" are not similar')
                                            ->end()
                                        ->end()
                                        ->children()
                                            ->scalarNode('title')->end()
                                            ->scalarNode('typeClass')
                                                ->constraints()
                                                    ->assertOr()
                                                        ->constraints()
                                                            ->assertIsEqual('facebook')->end()
                                                            ->assertIsEqual('twitter')->end()
                                                            ->assertIsEqual('email')->end()
                                                            ->assertIsEqual('embed')->end()
                                                        ->end()
                                                        ->message('The value is not in the list: "facebook", "twitter", "email", "embed"')
                                                    ->end()
                                                ->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
);
```

## Mock-Object Builder

Mock-object building API

```
// src/Tests/Utility/PipelineTest.php

use MockBuilderFactoryTrait,
    MockFactoryAwareTrait,
    PropertyValueFactoryTrait,
    MethodStubFactoryTrait,
    MockDefinitionRegistryTrait;

...

public function testOtherMockObjects()
{
    /** @var ContainerInterface|\PHPUnit_Framework_MockObject_MockObject $container */
    $container = $this->getMockBuilder(ContainerInterface::class)
        ->setMethods(get_class_methods(ContainerInterface::class))
        ->getMock();

    $container->messages = [];

    $container
        ->method('has')
        ->with(
            $this->isType('string')
        )
        ->will(
            $this->returnCallback(
                function ($name) {
                    return in_array($name, ['container', 'logger']);
                }
            )
        );

    $container
        ->method('get')
        ->with(
            $this->equalTo('logger')
        )
        ->will(
            $this->returnCallback(
                function () use ($container) {

                    $logger = $this->getMockBuilder(LoggerInterface::class)
                        ->setMethods(get_class_methods(LoggerInterface::class))
                        ->getMock();

                    $logger
                        ->method('info')
                        ->with(
                            $this->isType('string')
                        )
                        ->will(
                            $this->returnCallback(
                                function ($message) use ($container) {
                                    $container->messages[] = $message;
                                }
                            )
                        );

                    return $logger;
                }
            )
        );

    $logger = $container->get('logger');
    $logger->info('Hello, world #1');
    $logger->info('Hello, world #2');

    $this->assertEquals('Hello, world #1', array_shift($container->messages));
    $this->assertEquals('Hello, world #2', array_shift($container->messages));
}

```

## Proxy Factory

Dynamic creation of proxy objects via Proxy Factory API

```
// src/Tests/Proxy/ProxyTest.php

use MockBuilderFactoryTrait,
    MockFactoryAwareTrait,
    ProxyFactoryAwareTrait,
    PropertyValueFactoryTrait,
    MethodStubFactoryTrait,
    MockDefinitionRegistryTrait;

...

public function testForTreeStructures()
{
    $proxyNode = $this->getProxyFactory()->createProxy(new TestNode());

    $tree = new TestTree($proxyNode, 12);
    $root = $tree->getRoot();

    $this->assertInstanceOf(TestNode::class,       $root);
    $this->assertInstanceOf(ProxyInterface::class, $root);

    /** @var TestNode|ProxyInterface $node */
    foreach ($tree as $index => $node) {

        /** @var TestNode|ProxyInterface $proxyNode */
        $proxyNode = $this->getProxyFactory()->createProxy($node);

        $this->assertInstanceOf(TestNode::class,       $proxyNode);
        $this->assertInstanceOf(ProxyInterface::class, $proxyNode);

        $mockBuilder = $proxyNode->getMockBuilder();
        $mockBuilder
            ->property('uuid')
            ->will(
                $this->callableValue(
                    function () {
                        return 'TestId';
                    }
                )
            )
        ;

        $mockBuilder
            ->method('getId')
            ->will(
                $this->returnPropertyContext(
                    function (ContextInterface $context) {
                        return $context->getPropertyValue('uuid');
                    }
                )
            )
        ;

        $this->assertInstanceOf(\DateTime::class, $proxyNode->getCreatedAt());
        $this->assertEquals('TestId',             $proxyNode->getId());
    }

    /** @var TestNode|ProxyInterface $proxyNode */
    $proxyNode = $this->getProxyFactory()->createProxy(TestNode::class);

    $this->assertInstanceOf(ProxyInterface::class, $proxyNode);

    $mockBuilder = $proxyNode->getMockBuilder();
    $mockBuilder
        ->property('uuid')
        ->will(
            $this->callableValue(
                function () {
                    return 'TestId';
                }
            )
        )
    ;

    $mockBuilder
        ->method('getId')
        ->will(
            $this->returnPropertyContext(
                function (ContextInterface $context) {
                    return $context->getPropertyValue('uuid');
                }
            )
        )
    ;

    $this->assertEquals('TestId', $proxyNode->getId());
}

```