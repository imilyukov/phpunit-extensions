<?php

namespace Mia\PHPUnit\Mock;

/**
 * Class GeneratorAwareTrait
 * @package Mia\PHPUnit\Mock
 */
trait GeneratorAwareTrait
{
    /**
     * @var \PHPUnit_Framework_MockObject_Generator
     */
    protected $mockObjectGenerator;

    /**
     * @return \PHPUnit_Framework_MockObject_Generator
     */
    protected function getMockObjectGenerator()
    {
        if (!$this->mockObjectGenerator) {

            $this->mockObjectGenerator = GeneratorFactory::createGenerator();
        }

        return $this->mockObjectGenerator;
    }

    /**
     * @param \PHPUnit_Framework_MockObject_Generator $generator
     * @return mixed
     */
    public function setMockObjectGenerator(\PHPUnit_Framework_MockObject_Generator $generator)
    {
        $this->mockObjectGenerator = $generator;

        return $this;
    }
}
