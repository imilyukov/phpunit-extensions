<?php

namespace Mia\PHPUnit\Mock;

/**
 * Class MockBuilderFactoryTrait
 * @package Mia\PHPUnit\Mock
 */
trait MockBuilderFactoryTrait
{
    use GeneratorAwareTrait;

    /**
     * @param $className
     * @return \PHPUnit_Framework_MockObject_MockBuilder
     */
    public function getMockBuilder($className)
    {
        $reflectionClass = new \ReflectionClass(\PHPUnit_Framework_MockObject_MockBuilder::class);

        /** @var \PHPUnit_Framework_MockObject_MockBuilder $mockBuilder */
        $mockBuilder = $reflectionClass->newInstanceArgs([$this, $className]);

        // Fix for versions 5.x over
        if ($reflectionClass->hasProperty('generator')) {

            $property = $reflectionClass->getProperty('generator');
            $property->setAccessible(true);
            $property->setValue($mockBuilder, $this->getMockObjectGenerator());
        }

        return $mockBuilder;
    }
}
