<?php

namespace Mia\PHPUnit\Mock;

use Mia\PHPUnit\Mock\Definition\MockDefinitionFactoryInterface;

/**
 * Interface MockFactoryInterface
 * @package Mia\PHPUnit\Mock
 */
interface MockFactoryInterface
{
    /**
     * @return \PHPUnit_Framework_TestCase
     */
    public function getTestCase();

    /**
     * @return MockDefinitionFactoryInterface
     */
    public function getMockDefinitionFactory();

    /**
     * @param $class
     * @param callable|null $initializer
     * @return mixed
     */
    public function createMock($class, callable $initializer = null);
}
