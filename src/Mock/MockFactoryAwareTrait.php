<?php

namespace Mia\PHPUnit\Mock;

/**
 * Class MockFactoryAwareTrait
 * @package Mia\PHPUnit\Mock
 */
trait MockFactoryAwareTrait
{
    /**
     * @var MockFactoryInterface
     */
    protected $mockFactory;

    /**
     * @return MockFactoryInterface
     */
    public function getMockFactory()
    {
        if (!$this->mockFactory) {
            $this->mockFactory = new MockFactory($this);
        }

        return $this->mockFactory;
    }

    /**
     * @param MockFactoryInterface|null $factory
     * @return mixed
     */
    public function setMockFactory(MockFactoryInterface $factory = null)
    {
        $this->mockFactory = $factory;

        return $this;
    }
}
