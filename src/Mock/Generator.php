<?php

namespace Mia\PHPUnit\Mock;

/**
 * Class Generator
 * @package Mia\PHPUnit\Mock
 */
class Generator extends \PHPUnit_Framework_MockObject_Generator
{
    /**
     * @param string $code
     * @param string $className
     */
    protected function evalClass($code, $className)
    {
        if (!class_exists($className, false)) {

            // Fixed auto-generated methods for eval's constraints
            $code = str_replace(
                [
                    '__get()',
                    '__set()',
                    '__call()',
                    '__isset()',
                    '__unset()',
                ],
                [
                    '__get($name)',
                    '__set($name,  $value)',
                    '__call($name, $arguments)',
                    '__isset($name)',
                    '__unset($name)',
                ],
                $code
            );

            eval($code);
        }
    }
}
