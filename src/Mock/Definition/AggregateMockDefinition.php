<?php

namespace Mia\PHPUnit\Mock\Definition;

use Mia\PHPUnit\Mock\Builder\MockBuilderInterface;
use Mia\PHPUnit\Mock\MockFactoryAwareInterface;
use Mia\PHPUnit\Mock\MockFactoryAwareTrait;

/**
 * Class AggregateMockDefinition
 * @package Mia\PHPUnit\Mock\Definition
 */
class AggregateMockDefinition implements MockDefinitionInterface, MockFactoryAwareInterface
{
    use MockFactoryAwareTrait;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var \Traversable|array
     */
    protected $definitions = [];

    /**
     * AggregateMockDefinition constructor.
     * @param $name
     * @param $type
     */
    public function __construct($name, $type)
    {
        $this->name = $name;
        $this->type = $type;
    }

    /**
     * @param MockBuilderInterface $builder
     */
    public function define(MockBuilderInterface $builder)
    {
        foreach ($this->definitions as $definitionClass) {

            /** @var MockDefinitionInterface $definition */
            $definition = new $definitionClass;

            if ($definition instanceof MockFactoryAwareInterface) {
                $definition->setMockFactory($this->getMockFactory());
            }

            $definition->define($builder);
        }
    }

    /**
     * @param \Traversable $definitions
     * @return $this
     */
    public function setDefinitions(\Traversable $definitions)
    {
        $this->definitions = $definitions;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
