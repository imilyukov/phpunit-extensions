<?php

namespace Mia\PHPUnit\Mock\Definition;

use Mia\PHPUnit\Mock\MockFactoryAwareInterface;
use Mia\PHPUnit\Mock\MockFactoryAwareTrait;

/**
 * Class AbstractMockDefinition
 * @package Mia\PHPUnit\Mock\Definition
 */
abstract class AbstractMockDefinition implements MockDefinitionInterface, MockFactoryAwareInterface
{
    use MockFactoryAwareTrait;

    /**
     * @return \PHPUnit_Framework_TestCase
     */
    public function getTestCase()
    {
        return $this->getMockFactory()->getTestCase();
    }

    /**
     * @return string
     */
    public function getName()
    {
        if (!defined('static::NAME')) {

            throw new \BadMethodCallException(
                sprintf(
                    "Constant NAME is not defined for method %s",
                    __METHOD__
                )
            );
        }

        return static::NAME;
    }

    /**
     * @return string
     */
    public function getType()
    {
        if (!defined('static::TYPE')) {

            throw new \BadMethodCallException(
                sprintf(
                    "Constant TYPE is not defined for method %s",
                    __METHOD__
                )
            );
        }

        return static::TYPE;
    }
}
