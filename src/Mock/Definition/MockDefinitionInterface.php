<?php

namespace Mia\PHPUnit\Mock\Definition;

use Mia\PHPUnit\Mock\Builder\MockBuilderInterface;

/**
 * Interface MockDefinitionInterface
 * @package Mia\PHPUnit\Mock\Definition
 */
interface MockDefinitionInterface
{
    /**
     * @param MockBuilderInterface $builder
     */
    public function define(MockBuilderInterface $builder);

    /**
     * @return string
     */
    public function getType();

    /**
     * @return string
     */
    public function getName();
}
