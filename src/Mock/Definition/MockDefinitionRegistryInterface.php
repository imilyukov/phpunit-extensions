<?php

namespace Mia\PHPUnit\Mock\Definition;

/**
 * Interface MockDefinitionRegistryInterface
 * @package Mia\PHPUnit\Mock\Definition
 */
interface MockDefinitionRegistryInterface
{
    /**
     * @return array|\Traversable
     */
    public function getRegisteredMockDefinitions();
}
