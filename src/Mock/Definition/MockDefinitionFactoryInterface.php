<?php

namespace Mia\PHPUnit\Mock\Definition;

/**
 * Interface MockDefinitionRegistryInterface
 * @package Mia\PHPUnit\Mock\Definition
 */
interface MockDefinitionFactoryInterface
{
    /**
     * @param \Traversable|\ArrayAccess|array $definitionMap
     * @return $this
     */
    public function setDefinitionMap($definitionMap);

    /**
     * @param string $name
     * @return MockDefinitionInterface
     */
    public function createDefinition($name);
}
