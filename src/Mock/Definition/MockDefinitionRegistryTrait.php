<?php

namespace Mia\PHPUnit\Mock\Definition;

use Mia\PHPUnit\Utility\Iterator\GeneratorIterator;
use Mia\PHPUnit\Utility\Iterator\RecursiveGeneratorIterator;

/**
 * Class MockDefinitionRegistryTrait
 * @package Mia\PHPUnit\Mock\Definition
 */
trait MockDefinitionRegistryTrait
{
    /**
     * @return \Generator
     */
    public function getRegisteredMockDefinitions()
    {
        $namespaces = $this->getMockDefinitionNamespaces();

        foreach ($namespaces as $directory => $namespace) {

            if (!file_exists($directory) || __DIR__ === $directory) {
                continue;
            }

            $iterator = new \RegexIterator(
                new \RecursiveIteratorIterator(
                    new \RecursiveDirectoryIterator(
                        $directory,
                        \RecursiveDirectoryIterator::KEY_AS_PATHNAME | \RecursiveDirectoryIterator::SKIP_DOTS
                    ),
                    \RecursiveIteratorIterator::CHILD_FIRST
                ),
                '/\.php$/',
                \RecursiveRegexIterator::GET_MATCH
            );

            foreach ($iterator as $fileName => $el) {

                $reflectionClass = new \ReflectionClass($namespace . str_replace([$directory, DIRECTORY_SEPARATOR, '.php'], ['', '\\', ''], $fileName));

                if (!$reflectionClass->isSubclassOf(AbstractMockDefinition::class)) {
                    continue;
                }

                yield $reflectionClass->getConstant('NAME') => $reflectionClass->getName();
                yield $reflectionClass->getConstant('TYPE') => $reflectionClass->getName();
            }
        }
    }

    /**
     * @return \Traversable
     */
    protected function getMockDefinitionNamespaces()
    {
        return new \RecursiveIteratorIterator(
            new RecursiveGeneratorIterator(
                new GeneratorIterator(
                    $this->getMockDefinitionNamespaceGenerator(new \ReflectionClass(static::class))
                )
            ),
            \RecursiveIteratorIterator::SELF_FIRST
        );
    }

    /**
     * @param \ReflectionClass $reflectionClass
     * @return \Generator
     */
    private function getMockDefinitionNamespaceGenerator(\ReflectionClass $reflectionClass)
    {
        if ($parentClass = $reflectionClass->getParentClass()) {
            yield new GeneratorIterator($this->getMockDefinitionNamespaceGenerator($parentClass));
        }

        foreach ($reflectionClass->getTraits() as $trait) {
            yield new GeneratorIterator($this->getMockDefinitionNamespaceGenerator($trait));
        }

        $directory = dirname($reflectionClass->getFileName()) . DIRECTORY_SEPARATOR . 'Definition';
        if (file_exists($directory)) {
            yield $directory => $reflectionClass->getNamespaceName() . '\\Definition';
        }
    }
}
