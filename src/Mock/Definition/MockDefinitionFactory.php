<?php

namespace Mia\PHPUnit\Mock\Definition;

use Mia\PHPUnit\Utility\Iterator\GeneratorIterator;
use Mia\PHPUnit\Utility\Iterator\RecursiveGeneratorIterator;

/**
 * Class MockDefinitionFactory
 * @package Mia\PHPUnit\Mock\Definition
 */
class MockDefinitionFactory implements MockDefinitionFactoryInterface
{
    /**
     * @var \ArrayAccess|array
     */
    protected $definitionMap;

    /**
     * MockDefinitionFactory constructor.
     * @param \ArrayAccess|array $definitionMap
     */
    public function __construct($definitionMap = [])
    {
        $this->setDefinitionMap($definitionMap);
    }

    /**
     * @param \ArrayAccess|array $definitionMap
     * @return $this
     */
    public function setDefinitionMap($definitionMap)
    {
        if ($definitionMap instanceof \Generator) {
            $definitionMap = new GeneratorIterator($definitionMap);
        }

        $this->definitionMap = $definitionMap;

        return $this;
    }

    /**
     * @param string $className
     * @return MockDefinitionInterface
     */
    public function createDefinition($className)
    {
        if (!class_exists($className)) {

            if (!isset($this->definitionMap[$className])) {
                throw new \InvalidArgumentException(
                    sprintf(
                        'Mock definition with name %s is not registered.',
                        $className
                    )
                );
            }

            $className = constant(sprintf('%s::%s', $this->definitionMap[$className], 'TYPE'));
        }

        $definition = new AggregateMockDefinition($className, $className);
        $definition->setDefinitions(
            new \RecursiveIteratorIterator(
                new RecursiveGeneratorIterator(
                    new GeneratorIterator(
                        $this->getDefinitionGenerator(new \ReflectionClass($className))
                    )
                ),
                \RecursiveIteratorIterator::LEAVES_ONLY
            )
        );

        return $definition;
    }

    /**
     * @param \ReflectionClass $reflectionClass
     * @return \Generator
     */
    private function getDefinitionGenerator(\ReflectionClass $reflectionClass)
    {
        if (isset($this->definitionMap[$reflectionClass->getName()])) {
            yield $this->definitionMap[$reflectionClass->getName()];
        }

        foreach ($reflectionClass->getTraits() as $trait) {
            yield new GeneratorIterator($this->getDefinitionGenerator($trait));
        }

        foreach ($reflectionClass->getInterfaces() as $interface) {
            yield new GeneratorIterator($this->getDefinitionGenerator($interface));
        }

        if ($parentClass = $reflectionClass->getParentClass()) {
            yield new GeneratorIterator($this->getDefinitionGenerator($parentClass));
        }
    }
}
