<?php

namespace Mia\PHPUnit\Mock\Matcher;

use PHPUnit_Framework_MockObject_Invocation;

/**
 * Class MethodNameDecorator
 * @package Mia\PHPUnit\Mock\Matcher
 */
class MethodNameDecorator extends \PHPUnit_Framework_MockObject_Matcher_StatelessInvocation
{
    /**
     * @var \PHPUnit_Framework_MockObject_Matcher_MethodName
     */
    protected $matcher;

    /**
     * MethodNameDecorator constructor.
     * @param \PHPUnit_Framework_MockObject_Matcher_MethodName $matcher
     */
    public function __construct(\PHPUnit_Framework_MockObject_Matcher_MethodName $matcher)
    {
        $this->matcher = $matcher;
    }

    /**
     * Checks if the invocation $invocation matches the current rules. If it does
     * the matcher will get the invoked() method called which should check if an
     * expectation is met.
     *
     * @param PHPUnit_Framework_MockObject_Invocation $invocation Object containing information on a mocked or stubbed method which was invoked
     *
     * @return bool
     */
    public function matches(PHPUnit_Framework_MockObject_Invocation $invocation)
    {
        $matcherProperty = new \ReflectionProperty($this->matcher, 'constraint');
        $matcherProperty->setAccessible(true);

        $constraint = $matcherProperty->getValue($this->matcher);
        if ($constraint instanceof \PHPUnit_Framework_Constraint_IsEqual) {

            $constraintProperty = new \ReflectionProperty($constraint, 'value');
            $constraintProperty->setAccessible(true);

            return $invocation->methodName == $constraintProperty->getValue($constraint);
        }

        return $this->matcher->matches($invocation);
    }

    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return $this->matcher->toString();
    }
}
