<?php

namespace Mia\PHPUnit\Mock;

use Mia\PHPUnit\Mock\Builder\MockBuilder as ProxyMockBuilder;
use Mia\PHPUnit\Mock\Definition\MockDefinitionFactory;
use Mia\PHPUnit\Mock\Definition\MockDefinitionFactoryInterface;
use Mia\PHPUnit\Mock\Definition\MockDefinitionInterface;
use Mia\PHPUnit\Mock\Definition\MockDefinitionRegistryInterface;

/**
 * Class MockFactory
 * @package Mia\PHPUnit\Mock
 */
class MockFactory implements MockFactoryInterface
{
    /**
     * @var \PHPUnit_Framework_TestCase
     */
    protected $testCase;

    /**
     * @var MockDefinitionFactoryInterface
     */
    protected $mockDefinitionFactory;

    /**
     * MockFactory constructor.
     * @param MockDefinitionFactoryInterface $factory
     * @param \PHPUnit_Framework_TestCase $testCase
     */
    public function __construct(\PHPUnit_Framework_TestCase $testCase, MockDefinitionFactoryInterface $factory = null)
    {
        $this->testCase              = $testCase;
        $this->mockDefinitionFactory = $factory ?: new MockDefinitionFactory();

        if ($this->testCase instanceof MockDefinitionRegistryInterface) {

            $this->mockDefinitionFactory->setDefinitionMap(
                $this->testCase->getRegisteredMockDefinitions()
            );
        }
    }

    /**
     * @return \PHPUnit_Framework_TestCase
     */
    public function getTestCase()
    {
        return $this->testCase;
    }

    /**
     * @return MockDefinitionFactory|MockDefinitionFactoryInterface
     */
    public function getMockDefinitionFactory()
    {
        return $this->mockDefinitionFactory;
    }

    /**
     * @param $class
     * @param callable|null $initializer
     * @return \PHPUnit_Framework_MockObject_MockObject|mixed
     */
    public function createMock($class, callable $initializer = null)
    {
        $proxyMockBuilder = new ProxyMockBuilder();

        if ($initializer) {
            call_user_func_array($initializer, [$proxyMockBuilder]);
        }

        $mockBuilder = $this->testCase->getMockBuilder($class ?: \stdClass::class);

        return $proxyMockBuilder->build($mockBuilder);
    }

    /**
     * @param $definition
     * @param callable|null $initializer
     * @return mixed|\PHPUnit_Framework_MockObject_MockObject
     */
    public function createMockByDefinition($definition, callable $initializer = null)
    {
        if (!$definition instanceof MockDefinitionInterface) {
            $definition = $this->getMockDefinitionFactory()->createDefinition($definition);
        }

        if ($definition instanceof MockFactoryAwareInterface) {
            $definition->setMockFactory($this);
        }

        return $this->createMock(
            $definition->getType(),
            function (ProxyMockBuilder $builder) use ($definition, $initializer) {

                $definition->define($builder);

                if ($initializer) {
                    call_user_func_array($initializer, [$builder]);
                }
            }
        );
    }

    /**
     * @param $name
     * @param $arguments
     * @return \PHPUnit_Framework_MockObject_MockObject|mixed
     */
    public function __call($name, $arguments)
    {
        if (!preg_match('/^create(.+)Mock$/', $name, $match)) {
            throw new \BadMethodCallException(
                sprintf("Method %s doesn't support for %s", $name, __CLASS__)
            );
        }

        /** @var callable $initializer */
        list ($initializer) = array_pad($arguments, 1, function () {});

        if (!is_callable($initializer)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Method %s takes only one argument which is \\Closure.",
                    __METHOD__
                )
            );
        }

        return $this->createMockByDefinition(lcfirst($match[1]), $initializer);
    }
}
