<?php

namespace Mia\PHPUnit\Mock\Method;

/**
 * Class MethodOwnerTrait
 * @package Mia\PHPUnit\Mock\Method
 */
trait MethodOwnerTrait
{
    /**
     * @var MethodMockerInterface[][]
     */
    protected $methods = [];

    /**
     * @param MethodMockerInterface $method
     * @return $this
     */
    public function addMethod(MethodMockerInterface $method)
    {
        if (!$this->hasMethod($method->getName())) {
            $this->methods[$method->getName()] = [];
        }

        $this->methods[$method->getName()][] = $method->setOwner($this);

        return $this;
    }

    /**
     * @param $name
     * @return bool
     */
    public function hasMethod($name)
    {
        return array_key_exists($name, $this->methods);
    }

    /**
     * @param string $name
     * @return MethodMockerInterface[]
     */
    public function getMethod($name)
    {
        return $this->methods[$name];
    }

    /**
     * @param $name
     * @return $this
     */
    public function removeMethod($name)
    {
        unset($this->methods[$name]);

        return $this;
    }

    /**
     * @return MethodMockerInterface[][]
     */
    public function getMethods()
    {
        return $this->methods;
    }
}
