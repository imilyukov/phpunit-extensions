<?php

namespace Mia\PHPUnit\Mock\Method;

/**
 * Class MethodMocker
 * @package Mia\PHPUnit\Mock\Method
 */
class MethodMocker implements MethodMockerInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var MethodOwnerInterface
     */
    protected $owner;

    /**
     * @var \PHPUnit_Framework_MockObject_Stub
     */
    protected $returnStub;

    /**
     * @var \PHPUnit_Framework_MockObject_Matcher_Invocation
     */
    protected $expectation;

    /**
     * @var \PHPUnit_Framework_Constraint[]
     */
    protected $constraints = [];

    /**
     * MethodMocker constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param \PHPUnit_Framework_MockObject_Matcher_Invocation $expectation
     * @return $this
     */
    public function expects(\PHPUnit_Framework_MockObject_Matcher_Invocation $expectation)
    {
        $this->expectation = $expectation;

        return $this;
    }

    /**
     * @param $name
     * @return string|MethodMocker
     */
    public function method($name)
    {
        $this->name = $name;
        $this->owner->addMethod($this);

        return $this;
    }

    /**
     * @param array $constraints
     * @return $this
     */
    public function with(...$constraints)
    {
        $this->constraints = array_map(
            function ($constraint) {

                if (!$constraint instanceof \PHPUnit_Framework_Constraint) {
                    $constraint = new \PHPUnit_Framework_Constraint_IsEqual($constraint);
                }

                return $constraint;
            },
            $constraints
        );

        return $this;
    }

    /**
     * @param \PHPUnit_Framework_MockObject_Stub $stub
     * @return $this
     */
    public function will(\PHPUnit_Framework_MockObject_Stub $stub)
    {
        $this->returnStub = $stub;

        return $this;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_Matcher_Invocation
     */
    public function getExpectation()
    {
        return $this->expectation;
    }

    /**
     * @return \PHPUnit_Framework_Constraint[]
     */
    public function getConstraints()
    {
        return $this->constraints;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_Stub
     */
    public function getReturnStub()
    {
        return $this->returnStub;
    }

    /**
     * @param MethodOwnerInterface|null $owner
     * @return $this
     */
    public function setOwner(MethodOwnerInterface $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return MethodOwnerInterface
     */
    public function end()
    {
        return $this->owner;
    }

    /**
     * @param $name
     * @param $arguments
     * @return MethodMockerInterface
     */
    public function __call($name, $arguments)
    {
        if (preg_match('/^will(.+)$/', $name, $match)) {

            $namespaces = [
                sprintf('%s_%s', \PHPUnit_Framework_MockObject_Stub::class, $match[1]),
                sprintf('%s\\Stub\\%s', __NAMESPACE__, $match[1])
            ];

            foreach ($namespaces as $className) {

                if (class_exists($className)) {

                    $reflection = new \ReflectionClass($className);

                    return $this->will(
                        $reflection->newInstanceArgs($arguments)
                    );
                }
            }
        }

        throw new \BadMethodCallException(
            sprintf("Method %s doesn't support for %s", $name, __CLASS__)
        );
    }
}
