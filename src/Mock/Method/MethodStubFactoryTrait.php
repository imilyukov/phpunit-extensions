<?php

namespace Mia\PHPUnit\Mock\Method;

use Mia\PHPUnit\Mock\Property\ContextInterface;

/**
 * Class MethodStubFactoryTrait
 * @package Mia\PHPUnit\Mock\Method
 */
trait MethodStubFactoryTrait
{
    /**
     * @param array $constraintMap
     * @param array $expectations
     * @return Stub\ReturnConstraintMap
     */
    public function returnConstraintMap(array $constraintMap = [], array $expectations = [])
    {
        return new Stub\ReturnConstraintMap($constraintMap, $expectations);
    }

    /**
     * @param \ReflectionMethod $method
     * @param null $object
     * @return Stub\ReturnMethodReflector
     */
    public function returnMethodReflector(\ReflectionMethod $method, $object = null)
    {
        return new Stub\ReturnMethodReflector($method, $object);
    }

    /**
     * @param callable $handler
     * @param ContextInterface $context
     * @return Stub\ReturnPropertyContext
     */
    public function returnPropertyContext(callable $handler, ContextInterface $context = null)
    {
        return new Stub\ReturnPropertyContext($handler, $context);
    }

    /**
     * @param $reference
     * @return Stub\ReturnReference
     */
    public function returnReference(&$reference)
    {
        return new Stub\ReturnReference($reference);
    }
}
