<?php

namespace Mia\PHPUnit\Mock\Method\Stub;

use PHPUnit_Framework_MockObject_Invocation;
use Mia\PHPUnit\Mock\Property\ContextAwareInterface;
use Mia\PHPUnit\Mock\Property\ContextInterface;

/**
 * Class ReturnPropertyContext
 * @package Mia\PHPUnit\Mock\Method\Stub
 */
class ReturnPropertyContext implements \PHPUnit_Framework_MockObject_Stub, ContextAwareInterface
{
    /**
     * @var ContextInterface|null
     */
    protected $context;

    /**
     * @var callable
     */
    protected $handler;

    /**
     * ReturnPropertyContext constructor.
     * @param callable $handler
     * @param ContextInterface $context
     */
    public function __construct(callable $handler, ContextInterface $context = null)
    {
        $this->handler = $handler;
        $this->context = $context;
    }

    /**
     * @param ContextInterface|null $context
     * @return $this
     */
    public function setContext(ContextInterface $context = null)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Fakes the processing of the invocation $invocation by returning a
     * specific value.
     *
     * @param  PHPUnit_Framework_MockObject_Invocation $invocation
     *                                                             The invocation which was mocked and matched by the current method
     *                                                             and argument matchers.
     * @return mixed
     */
    public function invoke(PHPUnit_Framework_MockObject_Invocation $invocation)
    {
        if (!$invocation instanceof \PHPUnit_Framework_MockObject_Invocation_Object) {
            throw new \PHPUnit_Framework_Exception(
                'The current object can only be returned when mocking an ' .
                'object, not a static class.'
            );
        }

        $value = call_user_func_array($this->handler, array_merge([$this->context], $invocation->parameters));

        if ($value instanceof \PHPUnit_Framework_MockObject_Stub) {

            $value = $value->invoke($invocation);
        }

        return $value;
    }

    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'return a value from property context';
    }
}
