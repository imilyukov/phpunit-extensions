<?php

namespace Mia\PHPUnit\Mock\Method\Stub;

/**
 * Class ReturnMethodReflector
 * @package Mia\PHPUnit\Mock\Method\Stub
 */
class ReturnMethodReflector implements \PHPUnit_Framework_MockObject_Stub
{
    /**
     * @var \ReflectionMethod
     */
    protected $method;

    /**
     * @var mixed
     */
    protected $object;

    /**
     * ReturnMethodReflector constructor.
     * @param \ReflectionMethod $method
     * @param $object
     */
    public function __construct(\ReflectionMethod $method, $object = null)
    {
        $this->method = $method;
        $this->object = $object;
    }

    /**
     * @param \PHPUnit_Framework_MockObject_Invocation $invocation
     * @return mixed
     */
    public function invoke(\PHPUnit_Framework_MockObject_Invocation $invocation)
    {
        if (!$invocation instanceof \PHPUnit_Framework_MockObject_Invocation_Object) {
            throw new \PHPUnit_Framework_Exception(
                'The current object can only be returned when mocking an ' .
                'object, not a static class.'
            );
        }

        $this->method->setAccessible(true);

        return $this->method->invokeArgs($this->object ?: $invocation->object, $invocation->parameters);
    }

    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'return by method reflector';
    }
}
