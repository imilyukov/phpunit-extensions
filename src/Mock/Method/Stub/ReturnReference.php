<?php

namespace Mia\PHPUnit\Mock\Method\Stub;

use Mia\PHPUnit\Mock\Property\ContextInterface;
use Mia\PHPUnit\Utility\Reference\ReferenceWrapper;

/**
 * Class ReturnReference
 * @package Mia\PHPUnit\Mock\Method\Stub
 */
class ReturnReference implements \PHPUnit_Framework_MockObject_Stub
{
    /**
     * @var ContextInterface|null
     */
    protected $reference;

    /**
     * ReturnReference constructor.
     * @param $reference
     */
    public function __construct(&$reference)
    {
        $this->reference = &$reference;
    }

    /**
     * @param \PHPUnit_Framework_MockObject_Invocation $invocation
     * @return ReferenceWrapper
     */
    public function invoke(\PHPUnit_Framework_MockObject_Invocation $invocation)
    {
        return new ReferenceWrapper($this->reference);
    }

    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'return a reference';
    }
}
