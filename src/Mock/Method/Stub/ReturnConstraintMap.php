<?php

namespace Mia\PHPUnit\Mock\Method\Stub;

use PHPUnit_Framework_MockObject_Invocation;
use Mia\PHPUnit\Comparator\ConstraintComparator;

/**
 * Class ReturnConstraintMap
 * @package Mia\PHPUnit\Mock\Method\Stub
 */
class ReturnConstraintMap implements \PHPUnit_Framework_MockObject_Stub
{
    /**
     * @var array
     */
    protected $constraintMap = [];

    /**
     * @var array|\PHPUnit_Framework_MockObject_Matcher_InvokedRecorder[]
     */
    protected $expectations  = [];

    /**
     * ReturnConstraintMap constructor.
     * @param array $constraintMap
     * @param array|\PHPUnit_Framework_MockObject_Matcher_InvokedRecorder[] $expectations
     */
    public function __construct(array $constraintMap = [], array $expectations = [])
    {
        /**
         * To sort by priority
         * @see \Mia\PHPUnit\Comparator\ConstraintComparator
         */
        uasort($constraintMap, function ($cMap, $nMap) {

            $result = 0;

            $cParameters = array_slice($cMap, 0, count($cMap) - 1);
            $nParameters = array_slice($nMap, 0, count($nMap) - 1);

            do {
                $cParameter = array_shift($cParameters);
                $cParameter = $cParameter ?: new \PHPUnit_Framework_Constraint_IsAnything();
                $nParameter = array_shift($nParameters);
                $nParameter = $nParameter ?: new \PHPUnit_Framework_Constraint_IsAnything();

                $result += ConstraintComparator::compare($cParameter, $nParameter);

            } while (count($cParameters) || count($nParameters));

            return $result;
        });

        $this->constraintMap = $constraintMap;
        $this->expectations  = $expectations;
    }

    /**
     * @param PHPUnit_Framework_MockObject_Invocation $invocation
     * @return bool|mixed|null
     */
    public function invoke(PHPUnit_Framework_MockObject_Invocation $invocation)
    {
        foreach ($this->constraintMap as $index => $parameters) {

            $value = array_pop($parameters);

            if (count($invocation->parameters) < count($parameters)) {
                throw new \PHPUnit_Framework_ExpectationFailedException(
                    sprintf(
                        'Parameter count for invocation %s is too low.',
                        $invocation->toString()
                    )
                );
            }

            $expectation = $this->getExpectation($index);
            if (!$matches = $expectation->matches($invocation)) {
                continue;
            }

            $matches = array_reduce(
                $parameters,
                function ($matches, $parameter) use ($invocation) {

                    static $i = 0;

                    return $matches && $parameter->evaluate($invocation->parameters[$i++], '', true);
                },
                $matches
            );

            if ($matches) {

                $expectation->invoked($invocation);

                if ($value instanceof \PHPUnit_Framework_MockObject_Stub) {

                    $value = $value->invoke($invocation);
                }

                return $value;
            }
        }

        foreach ($this->expectations as $invokeIndex => $expectation) {

            $expectation->verify();

            $parameters = $this->constraintMap[$invokeIndex];
            $parameters = array_slice($parameters, 0, count($parameters) - 1);

            foreach ($parameters as $i => $parameter) {

                $parameter->evaluate(
                    $invocation->parameters[$i],
                    sprintf(
                        'Parameter %s for invocation #%d %s does not match expected ' .
                        'value.',
                        $i,
                        $invokeIndex,
                        $invocation->toString()
                    )
                );
            }
        }
    }

    /**
     * @param $index
     * @return mixed|\PHPUnit_Framework_MockObject_Matcher_AnyInvokedCount|\PHPUnit_Framework_MockObject_Matcher_InvokedRecorder
     */
    protected function getExpectation($index)
    {
        if (!isset($this->expectations[$index])) {
            $this->expectations[$index] = new \PHPUnit_Framework_MockObject_Matcher_AnyInvokedCount();
        }

        return $this->expectations[$index];
    }

    /**
     * @return string
     */
    public function toString()
    {
        return 'return constraint from a map';
    }
}
