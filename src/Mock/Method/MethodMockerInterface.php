<?php

namespace Mia\PHPUnit\Mock\Method;

/**
 * Interface MethodMockerInterface
 * @package Mia\PHPUnit\Mock\Method
 */
interface MethodMockerInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @param \PHPUnit_Framework_MockObject_Matcher_Invocation $expectation
     *
     * @return MethodMockerInterface
     */
    public function expects(\PHPUnit_Framework_MockObject_Matcher_Invocation $expectation);

    /**
     * @param array $constraints
     * @return MethodMockerInterface
     */
    public function with(...$constraints);

    /**
     * @param \PHPUnit_Framework_MockObject_Stub $stub
     * @return MethodMockerInterface
     */
    public function will(\PHPUnit_Framework_MockObject_Stub $stub);

    /**
     * @return \PHPUnit_Framework_MockObject_Matcher_Invocation
     */
    public function getExpectation();

    /**
     * @return \PHPUnit_Framework_Constraint[]
     */
    public function getConstraints();

    /**
     * @return \PHPUnit_Framework_MockObject_Stub
     */
    public function getReturnStub();

    /**
     * @param MethodOwnerInterface|null $owner
     * @return mixed
     */
    public function setOwner(MethodOwnerInterface $owner = null);

    /**
     * @return MethodOwnerInterface
     */
    public function end();
}
