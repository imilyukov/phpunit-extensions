<?php

namespace Mia\PHPUnit\Mock\Method;

/**
 * Interface MethodOwnerInterface
 * @package Mia\PHPUnit\Mock\Method
 */
interface MethodOwnerInterface
{
    /**
     * @param MethodMockerInterface $method
     * @return MethodOwnerInterface
     */
    public function addMethod(MethodMockerInterface $method);

    /**
     * @param string $name
     * @return bool
     */
    public function hasMethod($name);

    /**
     * @param string $name
     * @return MethodMockerInterface[]
     */
    public function getMethod($name);

    /**
     * @param string $name
     * @return MethodOwnerInterface
     */
    public function removeMethod($name);

    /**
     * @return MethodMockerInterface[][]
     */
    public function getMethods();
}
