<?php

namespace Mia\PHPUnit\Mock\Property;

/**
 * Class PropertyOwnerTrait
 * @package Mia\PHPUnit\Mock\Property
 */
trait PropertyOwnerTrait
{
    /**
     * @param PropertyMockerInterface $property
     * @return $this
     */
    public function addProperty(PropertyMockerInterface $property)
    {
        $this->properties[$property->getName()] = $property->setOwner($this);

        return $this;
    }

    /**
     * @var PropertyMockerInterface[]
     */
    protected $properties = [];

    /**
     * @param $name
     * @return PropertyMockerInterface
     */
    public function getProperty($name)
    {
        return $this->properties[$name];
    }

    /**
     * @param $name
     * @return bool
     */
    public function hasProperty($name)
    {
        return array_key_exists($name, $this->properties);
    }

    /**
     * @param $name
     */
    public function removeProperty($name)
    {
        unset($this->properties[$name]);
    }

    /**
     * @return PropertyMockerInterface[]
     */
    public function getProperties()
    {
        return $this->properties;
    }
}
