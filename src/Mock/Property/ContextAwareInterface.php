<?php

namespace Mia\PHPUnit\Mock\Property;

/**
 * Interface ContextAwareInterface
 * @package Mia\PHPUnit\Mock
 */
interface ContextAwareInterface
{
    /**
     * @param ContextInterface|null $context
     * @return $this
     */
    public function setContext(ContextInterface $context = null);
}
