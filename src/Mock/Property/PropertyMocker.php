<?php

namespace Mia\PHPUnit\Mock\Property;

use Mia\PHPUnit\Mock\Property\Value;

/**
 * Class PropertyMocker
 * @package Mia\PHPUnit\Mock\Property
 */
class PropertyMocker implements PropertyMockerInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var PropertyOwnerInterface
     */
    protected $owner;

    /**
     * @var \PHPUnit_Framework_Constraint
     */
    protected $constraint;

    /**
     * @var boolean
     */
    protected $cached = true;

    /**
     * @var PropertyValueInterface
     */
    protected $value;

    /**
     * PropertyMocker constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param \PHPUnit_Framework_Constraint $constraint
     * @return mixed
     */
    public function with(\PHPUnit_Framework_Constraint $constraint)
    {
        $this->constraint = $constraint;

        return $this;
    }

    /**
     * @param PropertyValueInterface $value
     *
     * @return $this
     */
    public function will(PropertyValueInterface $value)
    {
        $this->value = $this->cached ? new Value\CachedValue($value) : $value;

        return $this;
    }

    /**
     * @return \PHPUnit_Framework_Constraint
     */
    public function getConstraint()
    {
        return $this->constraint;
    }

    /**
     * @return PropertyValueInterface
     */
    public function getPropertyValue()
    {
        return $this->value;
    }

    /**
     * @param $cached
     * @return $this
     */
    public function setCached($cached)
    {
        $this->cached = $cached;

        return $this;
    }

    /**
     * @param PropertyOwnerInterface|null $owner
     * @return $this
     */
    public function setOwner(PropertyOwnerInterface $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return PropertyOwnerInterface
     */
    public function end()
    {
        return $this->owner;
    }

    /**
     * @param $name
     * @param $arguments
     * @return PropertyMockerInterface
     */
    public function __call($name, $arguments)
    {
        if (preg_match('/^will(.+)$/', $name, $match)) {

            $className = sprintf('%s\\Value\\%sValue', __NAMESPACE__, $match[1]);

            if (class_exists($className)) {

                $reflection = new \ReflectionClass($className);

                return $this->will(
                    $reflection->newInstanceArgs($arguments)
                );
            }
        }

        throw new \BadMethodCallException(
            sprintf("Method %s doesn't support for %s", $name, __CLASS__)
        );
    }
}
