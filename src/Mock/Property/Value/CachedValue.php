<?php

namespace Mia\PHPUnit\Mock\Property\Value;

use Mia\PHPUnit\Mock\Property\ContextInterface;
use Mia\PHPUnit\Mock\Property\PropertyValueInterface;

/**
 * Class ObjectValue
 * @package Mia\PHPUnit\Mock\Property\Value
 */
class CachedValue implements PropertyValueInterface
{
    /**
     * @var mixed
     */
    protected $cached;

    /**
     * @var PropertyValueInterface
     */
    protected $propertyValue;

    /**
     * CachedValue constructor.
     * @param PropertyValueInterface $propertyValue
     */
    public function __construct(PropertyValueInterface $propertyValue)
    {
        $this->propertyValue = $propertyValue;
    }

    /**
     * @param ContextInterface $context
     * @return mixed
     */
    public function getValue(ContextInterface $context)
    {
        if (null === $this->cached) {
            $this->cached = $this->propertyValue->getValue($context);
        }

        return $this->cached;
    }
}
