<?php

namespace Mia\PHPUnit\Mock\Property\Value;

use Mia\PHPUnit\Mock\Property\ContextInterface;

/**
 * Class CallableValue
 * @package Mia\PHPUnit\Mock\Property\Value
 */
class CallableValue extends MixedValue
{
    /**
     * @var callable
     */
    protected $handler;

    /**
     * @param callable $handler
     */
    public function __construct(callable $handler)
    {
        parent::__construct();

        $this->handler = $handler;
    }

    /**
     * @param ContextInterface $context
     */
    protected function prepareValue(ContextInterface $context)
    {
        $this->value = call_user_func($this->handler, $context);
        
        parent::prepareValue($context);
    }
}
