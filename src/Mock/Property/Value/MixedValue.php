<?php

namespace Mia\PHPUnit\Mock\Property\Value;

use Mia\PHPUnit\Mock\Property\ContextInterface;
use Mia\PHPUnit\Mock\Property\PropertyValueInterface;

/**
 * Class MixedValue
 * @package Mia\PHPUnit\Mock\Property\Value
 */
class MixedValue implements PropertyValueInterface
{
    /**
     * @var mixed
     */
    protected $value;

    /**
     * @param mixed $value
     */
    public function __construct($value = null)
    {
        $this->value = $value;
    }

    /**
     * @param ContextInterface $context
     * @return mixed
     */
    public function getValue(ContextInterface $context)
    {
        $this->prepareValue($context);

        return $this->value;
    }

    /**
     * @param ContextInterface $context
     */
    protected function prepareValue(ContextInterface $context)
    {
        if ($this->value instanceof PropertyValueInterface) {
            $this->value = $this->value->getValue($context);
        }

        if (is_array($this->value)) {

            array_walk_recursive(
                $this->value,
                function (&$property) use ($context) {
                    if ($property instanceof PropertyValueInterface) {
                        $property = $property->getValue($context);
                    }
                }
            );
        }
    }
}
