<?php

namespace Mia\PHPUnit\Mock\Property\Value;

use Mia\PHPUnit\Mock\Property\ContextInterface;
use Mia\PHPUnit\Utility\Iterator\GeneratorIterator;

/**
 * Class GeneratorValue
 * @package Mia\PHPUnit\Mock\Property\Value
 */
class GeneratorValue extends CallableValue
{
    /**
     * @var \Generator
     */
    protected $generator;

    /**
     * @var int|array
     */
    protected $countOrKeys;

    /**
     * GeneratorValue constructor.
     * @param \Generator|callable $generator
     * @param int $countOrKeys
     */
    public function __construct($generator, $countOrKeys = 0)
    {
        if (!$generator instanceof \Generator) {

            parent::__construct($generator);

            $this->countOrKeys = $countOrKeys;

            return;
        }

        $this->generator = $generator;
    }

    /**
     * @param ContextInterface $context
     *
     * @return mixed
     */
    public function getValue(ContextInterface $context)
    {
        $value = new GeneratorIterator($this->generator ?: $this->getGenerator($context));

        return $value;
    }

    /**
     * @param $context
     * @return \EmptyIterator|\Generator
     */
    private function getGenerator($context)
    {
        $keys = is_array($this->countOrKeys) ? $this->countOrKeys : range(0, $this->countOrKeys - 1);

        foreach ($keys as $key) {

            yield $key => parent::getValue($context);
        }
    }
}
