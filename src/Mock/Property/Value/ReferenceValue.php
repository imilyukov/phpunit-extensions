<?php

namespace Mia\PHPUnit\Mock\Property\Value;

use Mia\PHPUnit\Mock\Property\ContextInterface;
use Mia\PHPUnit\Utility\Reference\ReferenceWrapper;

/**
 * Class ReferenceValue
 * @package Mia\PHPUnit\Mock\Property\Value
 */
class ReferenceValue extends MixedValue
{
    /**
     * ObjectValue constructor.
     * @param mixed $value
     */
    public function __construct(&$value)
    {
        parent::__construct();

        $this->value = &$value;
    }

    /**
     * @param ContextInterface $context
     * @return mixed
     */
    public function getValue(ContextInterface $context)
    {
        $this->prepareValue($context);

        return new ReferenceWrapper($this->value);
    }
}
