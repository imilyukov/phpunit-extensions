<?php

namespace Mia\PHPUnit\Mock\Property\Value;

/**
 * Class IdentityValue
 * @package Mia\PHPUnit\Mock\Property\Value
 */
class IdentityValue extends CallableValue
{
    /**
     * IdentityValue constructor.
     * @param callable|null $strategy
     */
    public function __construct(callable $strategy = null)
    {
        parent::__construct($strategy ?: [$this, 'getDefaultValue']);
    }

    /**
     * @return string
     */
    protected function getDefaultValue()
    {
        return sprintf(
            '%04X%04X-%04X-%04X-%04X-%04X%04X%04X',
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(16384, 20479),
            mt_rand(32768, 49151),
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(0, 65535)
        );
    }
}
