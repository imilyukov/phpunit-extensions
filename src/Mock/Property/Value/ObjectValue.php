<?php

namespace Mia\PHPUnit\Mock\Property\Value;

use Mia\PHPUnit\Mock\Property\ContextInterface;

/**
 * Class ObjectValue
 * @package Mia\PHPUnit\Mock\Property\Value
 */
class ObjectValue extends MixedValue
{
    /**
     * ObjectValue constructor.
     * @param array $properties
     */
    public function __construct(array $properties)
    {
        parent::__construct($properties);
    }

    /**
     * @param ContextInterface $context
     * @return mixed
     */
    public function getValue(ContextInterface $context)
    {
        $this->value = (Object) parent::getValue($context);

        return $this->value;
    }
}
