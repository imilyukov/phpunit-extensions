<?php

namespace Mia\PHPUnit\Mock\Property\Value;

use Mia\PHPUnit\Mock\Property\ContextInterface;
use Mia\PHPUnit\Mock\Property\PropertyValueInterface;

/**
 * Class ReflectorValue
 * @package Mia\PHPUnit\Mock\Property\Value
 */
class ReflectorValue implements PropertyValueInterface
{
    /**
     * @var \ReflectionProperty
     */
    protected $property;

    /**
     * @var mixed
     */
    protected $object;

    /**
     * ReflectorValue constructor.
     * @param \ReflectionProperty $property
     * @param null $object
     */
    public function __construct(\ReflectionProperty $property, $object = null)
    {
        $this->property = $property;
        $this->object   = $object;
    }

    /**
     * @param ContextInterface $context
     * @return mixed
     */
    public function getValue(ContextInterface $context)
    {
        $this->property->setAccessible(true);

        return $this->property->getValue($this->object ?: $context->getMockObject());
    }
}
