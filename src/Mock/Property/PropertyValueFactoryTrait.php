<?php

namespace Mia\PHPUnit\Mock\Property;

/**
 * Class PropertyValueFactoryTrait
 * @package Mia\PHPUnit\Mock\Property
 */
trait PropertyValueFactoryTrait
{
    /**
     * @param PropertyValueInterface $value
     * @return Value\CachedValue
     */
    public function cachedValue($value)
    {
        return new Value\CachedValue($value);
    }

    /**
     * @param callable $value
     * @return Value\CallableValue
     */
    public function callableValue(callable $value)
    {
        return new Value\CallableValue($value);
    }

    /**
     * @param \Generator|callable $generator
     * @param int $count
     * @return Value\GeneratorValue
     */
    public function generatorValue($generator, $count = 0)
    {
        return new Value\GeneratorValue($generator, $count);
    }

    /**
     * @param callable|null $strategy
     * @return Value\IdentityValue
     */
    public function identityValue(callable $strategy = null)
    {
        return new Value\IdentityValue($strategy);
    }

    /**
     * @param $value
     * @return Value\MixedValue
     */
    public function mixedValue($value)
    {
        return new Value\MixedValue($value);
    }

    /**
     * @param array $value
     * @return Value\ObjectValue
     */
    public function objectValue(array $value)
    {
        return new Value\ObjectValue($value);
    }

    /**
     * @param array $value
     * @return Value\ReferenceValue
     */
    public function referenceValue(&$value)
    {
        return new Value\ReferenceValue($value);
    }

    /**
     * @param \ReflectionProperty $property
     * @param null $object
     * @return Value\ReflectorValue
     */
    public function reflectorValue(\ReflectionProperty $property, $object = null)
    {
        return new Value\ReflectorValue($property, $object);
    }
}
