<?php

namespace Mia\PHPUnit\Mock\Property;

/**
 * Interface PropertyOwnerInterface
 * @package Mia\PHPUnit\Mock\Property
 */
interface PropertyOwnerInterface
{
    /**
     * @param PropertyMockerInterface $property
     * @return $this
     */
    public function addProperty(PropertyMockerInterface $property);

    /**
     * @param $name
     * @return PropertyMockerInterface
     */
    public function getProperty($name);

    /**
     * @param $name
     * @return bool
     */
    public function hasProperty($name);

    /**
     * @param $name
     */
    public function removeProperty($name);

    /**
     * @return PropertyMockerInterface[]
     */
    public function getProperties();
}
