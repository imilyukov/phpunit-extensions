<?php

namespace Mia\PHPUnit\Mock\Property;

/**
 * Interface PropertyValueInterface
 * @package Mia\PHPUnit\Mock\Property
 */
interface PropertyValueInterface
{
    /**
     * @param ContextInterface $context
     * @return mixed
     */
    public function getValue(ContextInterface $context);
}
