<?php

namespace Mia\PHPUnit\Mock\Property;

/**
 * Interface ContextInterface
 * @package Mia\PHPUnit\Mock
 */
interface ContextInterface extends PropertyOwnerInterface
{
    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function getMockObject();

    /**
     * @param \PHPUnit_Framework_MockObject_MockObject $mockObject
     * @return $this
     */
    public function setMockObject(\PHPUnit_Framework_MockObject_MockObject $mockObject);

    /**
     * @return \ReflectionClass
     */
    public function getClassReflector();
    
    /**
     * @param \Closure|null $initializer
     * @return $this
     */
    public function setInitializer(\Closure $initializer = null);

    /**
     * @param $name
     * @return mixed
     */
    public function getPropertyValue($name);
}
