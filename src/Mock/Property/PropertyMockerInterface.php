<?php

namespace Mia\PHPUnit\Mock\Property;

/**
 * Interface PropertyMockerInterface
 * @package Mia\PHPUnit\Mock\Property
 */
interface PropertyMockerInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @param \PHPUnit_Framework_Constraint $constraint
     * @return mixed
     */
    public function with(\PHPUnit_Framework_Constraint $constraint);

    /**
     * @param PropertyValueInterface $value
     *
     * @return $this
     */
    public function will(PropertyValueInterface $value);

    /**
     * @return PropertyValueInterface
     */
    public function getPropertyValue();

    /**
     * @return \PHPUnit_Framework_Constraint
     */
    public function getConstraint();

    /**
     * @param $cached
     * @return mixed
     */
    public function setCached($cached);

    /**
     * @param PropertyOwnerInterface|null $owner
     * @return mixed
     */
    public function setOwner(PropertyOwnerInterface $owner = null);

    /**
     * @return PropertyOwnerInterface
     */
    public function end();
}
