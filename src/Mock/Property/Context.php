<?php

namespace Mia\PHPUnit\Mock\Property;

/**
 * Class Context
 * @package Mia\PHPUnit\Mock\Property
 */
class Context implements ContextInterface
{
    use PropertyOwnerTrait;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $mockObject;

    /**
     * @var \ReflectionClass
     */
    protected $classReflector;

    /**
     * @var \Closure|null
     */
    protected $initializer;

    /**
     * Context constructor.
     * @param \Closure|null $initializer
     */
    public function __construct(\Closure $initializer = null)
    {
        $this->initializer = $initializer;
    }

    /**
     * @param \Closure|null $initializer
     * @return $this
     */
    public function setInitializer(\Closure $initializer = null)
    {
        $this->initializer = $initializer;

        return $this;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function getMockObject()
    {
        $this->initializer && $this->initializer->__invoke($this);

        return $this->mockObject;
    }

    /**
     * @param \PHPUnit_Framework_MockObject_MockObject $mockObject
     * @return $this
     */
    public function setMockObject(\PHPUnit_Framework_MockObject_MockObject $mockObject)
    {
        if (!$this->initializer) {
            throw new \LogicException(
                'You can set mockObject through initializer only.'
            );
        }

        $this->mockObject = $mockObject;

        return $this;
    }

    /**
     * @return \ReflectionClass
     */
    public function getClassReflector()
    {
        if (!$this->classReflector) {
            $this->classReflector = new \ReflectionClass(get_parent_class($this->getMockObject()));
        }

        return $this->classReflector;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getPropertyValue($name)
    {
        $value = $this->getProperty($name)->getPropertyValue()->getValue($this);

        return $value;
    }
}
