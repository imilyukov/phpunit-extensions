<?php

namespace Mia\PHPUnit\Mock\Builder;

use Mia\PHPUnit\Constraint\PropertyContextConstraint;
use Mia\PHPUnit\Mock\Method\MethodMocker;
use Mia\PHPUnit\Mock\Method\MethodMockerInterface;
use Mia\PHPUnit\Mock\Method\MethodOwnerTrait;
use Mia\PHPUnit\Mock\Method\Stub\ReturnConstraintMap;
use Mia\PHPUnit\Mock\Method\Stub\ReturnPropertyContext;
use Mia\PHPUnit\Mock\Property\Context as PropertyContext;
use Mia\PHPUnit\Mock\Property\ContextAwareInterface;
use Mia\PHPUnit\Mock\Property\ContextInterface;
use Mia\PHPUnit\Mock\Property\PropertyMocker;
use Mia\PHPUnit\Mock\Property\PropertyMockerInterface;
use Mia\PHPUnit\Mock\Property\PropertyOwnerTrait;
use Mia\PHPUnit\Mock\Property\Value;

/**
 * Class MockBuilder
 * @package Mia\PHPUnit\Mock\Builder
 */
class MockBuilder implements MockBuilderInterface
{
    use MethodOwnerTrait, PropertyOwnerTrait;

    /**
     * @var array
     */
    protected $callbackQueue = [];

    /**
     * @var bool
     */
    protected $enableMockConstructor = false;

    /**
     * @var array
     */
    protected $mockConstructorArgs = [];

    /**
     * @return mixed
     */
    public function enableMockConstructor()
    {
        $this->enableMockConstructor = true;

        return $this;
    }

    /**
     * @return mixed
     */
    public function disabledMockConstructor()
    {
        $this->enableMockConstructor = false;
        $this->mockConstructorArgs   = [];

        return $this;
    }

    /**
     * @param array $arguments
     * @return mixed
     */
    public function setMockConstructorArgs(array $arguments = [])
    {
        $this->mockConstructorArgs = $arguments;

        return $this;
    }

    /**
     * @param \PHPUnit_Framework_MockObject_Matcher_Invocation $expectation
     * @return MethodMocker
     */
    public function expects(\PHPUnit_Framework_MockObject_Matcher_Invocation $expectation)
    {
        $method = new MethodMocker(null);
        $method->setOwner($this);

        return $method->expects($expectation);
    }

    /**
     * @param string $method
     * @return string|MethodMocker
     */
    public function method($method)
    {
        if (!$method instanceof MethodMockerInterface) {

            $method = new MethodMocker($method);
        }

        $this->addMethod($method);

        return $method;
    }

    /**
     * @param string $property
     * @param bool $cached
     * @return PropertyMocker
     */
    public function property($property, $cached = true)
    {
        if (!$property instanceof PropertyMockerInterface) {
            $property = new PropertyMocker($property);
        }

        $property->setCached($cached);

        $this->addProperty($property);

        return $property;
    }

    /**
     * @param \PHPUnit_Framework_MockObject_MockBuilder $mockBuilder
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function build(\PHPUnit_Framework_MockObject_MockBuilder $mockBuilder)
    {
        if (count($this->properties)) {

            $this
                ->method('__isset')
                ->with(
                    new PropertyContextConstraint(
                        function (ContextInterface $context, $name) {
                            return $context->hasProperty($name);
                        }
                    )
                )
                ->will(
                    new ReturnPropertyContext(
                        function (ContextInterface $context, $name) {
                            return $context->hasProperty($name);
                        }
                    )
                )
            ;

            $this
                ->method('__unset')
                ->with(
                    new PropertyContextConstraint(
                        function (ContextInterface $context, $name) {
                            return $context->hasProperty($name);
                        }
                    )
                )
                ->will(
                    new ReturnPropertyContext(
                        function (ContextInterface $context, $name) {
                            $context->removeProperty($name);
                        }
                    )
                )
            ;

            $this
                ->method('__get')
                ->with(
                    new PropertyContextConstraint(
                        function (ContextInterface $context, $name) {
                            return $context->hasProperty($name);
                        }
                    )
                )
                ->will(
                    new ReturnPropertyContext(
                        function (ContextInterface $context, $name) {
                            if (!$context->hasProperty($name)) {

                                throw new \UnexpectedValueException(
                                    sprintf(
                                        "Property %s doesn't exist or isn't accessable", $name
                                    )
                                );
                            }

                            return $context->getPropertyValue($name);
                        }
                    )
                )
            ;

            $this
                ->method('__set')
                ->with(
                    new PropertyContextConstraint(
                        function (ContextInterface $context, $name) {
                            return $context->hasProperty($name);
                        }
                    )
                )
                ->will(
                    new ReturnPropertyContext(
                        function (ContextInterface $context, $name) {
                            $property = $context->getProperty($name);

                            return new ReturnConstraintMap(
                                [
                                    [
                                        new \PHPUnit_Framework_Constraint_IsEqual($property->getName()),
                                        $property->getConstraint(),
                                        new \PHPUnit_Framework_MockObject_Stub_ReturnCallback(
                                            function ($name, $value) use ($context) {

                                                $property = $context->getProperty($name);
                                                $property->will(new Value\MixedValue($value));
                                            }
                                        )
                                    ]
                                ]
                            );
                        }
                    )
                )
            ;
        }

        $mockBuilder
            ->disableOriginalConstructor()
            ->setMethods(array_keys($this->methods))
        ;

        while (count($this->callbackQueue) > 0) {

            list ($method, $arguments) = array_shift($this->callbackQueue);

            call_user_func_array([$mockBuilder, $method], $arguments);
        }

        // If method __isset has preset already, then should to add proxy for other parameters
        if ($this->hasMethod('__isset')) {

            $this
                ->method('__isset')
                ->will(
                    new ReturnPropertyContext(
                        function (ContextInterface $context, $name) {

                            $mockObject = $context->getMockObject();
                            $reflector  = $context->getClassReflector();

                            if ($reflector && $reflector->hasMethod('__isset')) {
                                return $reflector->getMethod('__isset')->invoke($mockObject, $name);
                            }

                            return isset($mockObject->{$name});
                        }
                    )
                )
            ;
        }

        // If method __unset has preset already, then should to add proxy for other parameters
        if ($this->hasMethod('__unset')) {

            $this
                ->method('__unset')
                ->will(
                    new ReturnPropertyContext(
                        function (ContextInterface $context, $name) {

                            $mockObject = $context->getMockObject();
                            $reflector  = $context->getClassReflector();

                            if ($reflector && $reflector->hasMethod('__unset')) {
                                return $reflector->getMethod('__unset')->invoke($mockObject, $name);
                            }

                            unset($mockObject->{$name});
                        }
                    )
                )
            ;
        }

        // If method __get has preset already, then should to add proxy for other parameters
        if ($this->hasMethod('__get')) {

            $this
                ->method('__get')
                ->will(
                    new ReturnPropertyContext(
                        function (ContextInterface $context, $name) {

                            $mockObject = $context->getMockObject();
                            $reflector  = $context->getClassReflector();

                            if ($reflector && $reflector->hasMethod('__get')) {
                                return $reflector->getMethod('__get')->invoke($mockObject, $name);
                            }

                            return $mockObject->{$name};
                        }
                    )
                )
            ;
        }

        // If method __set has preset already, then should to add proxy for other parameters
        if ($this->hasMethod('__set')) {

            $this
                ->method('__set')
                ->will(
                    new ReturnPropertyContext(
                        function (ContextInterface $context, $name, $value) {

                            $mockObject = $context->getMockObject();
                            $reflector  = $context->getClassReflector();

                            if ($reflector && $reflector->hasMethod('__set')) {
                                return $reflector->getMethod('__set')->invokeArgs($mockObject, [$name, $value]);
                            }

                            $mockObject->{$name} = $value;
                        }
                    )
                )
            ;
        }

        $context = new PropertyContext(
            function (ContextInterface $context) use ($mockBuilder) {

                $mockObject = $mockBuilder->getMock();

                // Remove properties for expectation through __get(), __set(), __isset() or __unset()
                array_walk(
                    $this->properties,
                    \Closure::bind(
                        function (PropertyMockerInterface $property) use ($mockObject, $context) {

                            $property = clone $property;

                            if (null === $property->getConstraint()) {
                                $property
                                    ->with(
                                        new \PHPUnit_Framework_Constraint_IsAnything()
                                    )
                                ;
                            }

                            if (null === $property->getPropertyValue()) {
                                $property
                                    ->will(
                                        new Value\MixedValue($mockObject->{$property->getName()})
                                    )
                                ;
                            }

                            $context->addProperty($property);

                            unset($mockObject->{$property->getName()});
                        },
                        null, $mockObject
                    )
                );

                $context->setMockObject($mockObject);
                $context->setInitializer(null);
            }
        );

        // Initialize mock-object through property context
        $mockObject = $context->getMockObject();

        foreach ($this->methods as $methodName => $methods) {

            $mockObject
                ->method($methodName)
                ->will(
                    new ReturnConstraintMap(
                        array_map(
                            function (MethodMockerInterface $method) use ($context) {
                                $constraints = $method->getConstraints();
                                $constraints = array_map(
                                    function ($constraint) use ($context) {
                                        if ($constraint instanceof ContextAwareInterface) {
                                            $constraint->setContext($context);
                                        }

                                        return $constraint;
                                    },
                                    is_array($constraints) ? $constraints : [$constraints]
                                );

                                $stub = $method->getReturnStub();
                                if ($stub instanceof ContextAwareInterface) {
                                    $stub->setContext($context);
                                }

                                return array_merge($constraints, [$stub]);
                            },
                            $methods
                        ),
                        array_map(
                            function (MethodMockerInterface $method) {
                                return $method->getExpectation();
                            },
                            $methods
                        )
                    )
                )
            ;
        }

        if ($this->enableMockConstructor) {
            $reflector = $context->getClassReflector();
            if ($reflector->hasMethod('__construct')) {
                $reflector->getMethod('__construct')->invokeArgs($mockObject, $this->mockConstructorArgs);
            }
        }

        return $mockObject;
    }

    /**
     * @param $name
     * @param $arguments
     * @return $this
     */
    public function __call($name, $arguments)
    {
        if (!method_exists(\PHPUnit_Framework_MockObject_MockBuilder::class, $name)
            || !preg_match('/^(set|disable|enable)/', $name)) {
            throw new \BadMethodCallException(
                sprintf("Method %s doesn't support for %s", $name, __CLASS__)
            );
        }

        $this->callbackQueue[] = [$name, $arguments];

        return $this;
    }

    /**
     * @param MockBuilderInterface $builder
     * @return MockBuilderInterface
     */
    public function __invoke(MockBuilderInterface $builder)
    {
        $properties = $this->getProperties();
        foreach ($properties as $property) {

            $builder->addProperty(clone $property);
        }

        $methods = $this->getMethods();
        foreach ($methods as $methodSet) {

            array_walk(
                $methodSet,
                function ($method) use ($builder) {
                    $builder->addMethod(clone $method);
                }
            );
        }

        foreach ($this->callbackQueue as $callback) {

            list ($method, $arguments) = $callback;

            call_user_func_array([$builder, $method], $arguments);
        }

        return $builder;
    }
}
