<?php

namespace Mia\PHPUnit\Mock\Builder;

use Mia\PHPUnit\Mock\Method\MethodMockerInterface;
use Mia\PHPUnit\Mock\Method\MethodOwnerInterface;
use Mia\PHPUnit\Mock\Property\PropertyMockerInterface;
use Mia\PHPUnit\Mock\Property\PropertyOwnerInterface;

/**
 * Interface MockBuilderInterface
 * @package Mia\PHPUnit\Mock\Builder
 */
interface MockBuilderInterface extends MethodOwnerInterface, PropertyOwnerInterface
{
    /**
     * @return mixed
     */
    public function enableMockConstructor();

    /**
     * @return mixed
     */
    public function disabledMockConstructor();

    /**
     * @param array $arguments
     * @return mixed
     */
    public function setMockConstructorArgs(array $arguments = []);

    /**
     * @param string $name
     *
     * @return MethodMockerInterface
     */
    public function method($name);

    /**
     * @param string $property
     *
     * @param bool $cached
     * @return PropertyMockerInterface
     */
    public function property($property, $cached = true);

    /**
     * @param \PHPUnit_Framework_MockObject_MockBuilder $mockBuilder
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function build(\PHPUnit_Framework_MockObject_MockBuilder $mockBuilder);

    /**
     * @param MockBuilderInterface $builder
     * @return MockBuilderInterface
     */
    public function __invoke(MockBuilderInterface $builder);
}
