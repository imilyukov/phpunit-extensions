<?php

namespace Mia\PHPUnit\Mock;

/**
 * Interface MockFactoryAwareInterface
 * @package Mia\PHPUnit\Mock
 */
interface MockFactoryAwareInterface
{
    /**
     * @return MockFactoryInterface
     */
    public function getMockFactory();

    /**
     * @param MockFactoryInterface|null $factory
     * @return mixed
     */
    public function setMockFactory(MockFactoryInterface $factory = null);
}
