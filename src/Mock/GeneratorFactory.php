<?php

namespace Mia\PHPUnit\Mock;

/**
 * Class GeneratorFactory
 *
 * Factory for creation of mock-object generator with fix
 *
 * @package Mia\PHPUnit\Mock
 */
class GeneratorFactory
{
    /**
     * @return mixed
     */
    public static function createGenerator()
    {
        $baseReflection = new \ReflectionClass(\PHPUnit_Framework_MockObject_Generator::class);

        $className = 'Generator_' . md5($baseReflection->getName());

        if (!class_exists($className, false)) {

            $code = file_get_contents($baseReflection->getFileName());
            $code = str_replace(
                [
                    'private function',
                    'dirname(__FILE__)',
                    '__DIR__',
                    $baseReflection->getName()
                ],
                [
                    'protected function',
                    sprintf('dirname("%s")', $baseReflection->getFileName()),
                    sprintf('dirname("%s")', $baseReflection->getFileName()),
                    $className
                ],
                $code
            );
            $code = trim($code, '<?php');

            eval($code);
        }

        /** @var \PHPUnit_Framework_MockObject_Generator|\PHPUnit_Framework_MockObject_MockObject $generator */
        $generator = $baseReflection->newInstance()->getMock($className, ['evalClass', 'generateMockedMethodDefinition']);
        $generator
            ->method('evalClass')
            ->will(
                new \PHPUnit_Framework_MockObject_Stub_ReturnCallback(
                    function ($code, $className) {
                        if (!class_exists($className, false)) {

                            // Fixed auto-generated methods for eval's constraints
                            $code = str_replace(
                                [
                                    '__get()',
                                    '__set()',
                                    '__call()',
                                    '__isset()',
                                    '__unset()',
                                ],
                                [
                                    '__get($name)',
                                    '__set($name,  $value)',
                                    '__call($name, $arguments)',
                                    '__isset($name)',
                                    '__unset($name)',
                                ],
                                $code
                            );

                            // Fixed the performance of the method-name matcher
                            $code = str_replace(
                                'PHPUnit_Framework_MockObject_InvocationMocker',
                                InvocationMocker::class,
                                $code
                            );

                            eval($code);
                        }
                    }
                )
            )
        ;

        $generator
            ->method('generateMockedMethodDefinition')
            ->will(
                new \PHPUnit_Framework_MockObject_Stub_ReturnCallback(
                    function (...$parameters) use ($generator, $className) {

                        $methodReflector = new \ReflectionMethod($className, 'generateMockedMethodDefinition');
                        $methodReflector->setAccessible(true);

                        list ($methodName, $reference) = array_reduce(
                            $methodReflector->getParameters(),
                            function ($carry, \ReflectionParameter $parameter) use ($parameters) {
                                if (in_array($parameter->getName(), ['methodName', 'reference'])) {
                                    $argument = isset($parameters[$parameter->getPosition()])
                                        ? $parameters[$parameter->getPosition()]
                                        : $parameter->getDefaultValue()
                                    ;
                                    $carry = array_merge($carry, [$argument]);
                                }

                                return $carry;
                            },
                            []
                        );

                        $definition = $methodReflector->invokeArgs($generator, $parameters);

                        // Referenced methods by default
                        if ('&' != $reference && in_array($methodName, ['__get'])) {

                            $reference  = '&';
                            $definition = preg_replace(
                                "/function\\s+{$methodName}/",
                                "function {$reference}{$methodName}",
                                $definition
                            );
                        }

                        return str_replace(
                            '$result = $this->__phpunit_getInvocationMocker',
                            "\$result = {$reference}\$this->__phpunit_getInvocationMocker",
                            $definition
                        );
                    }
                )
            )
        ;
        
        return $generator;
    }
}
