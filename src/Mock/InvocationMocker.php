<?php

namespace Mia\PHPUnit\Mock;

use Mia\PHPUnit\Mock\Matcher\MethodNameDecorator;
use Mia\PHPUnit\Utility\Reference\ReferenceWrapper;

/**
 * Class InvocationMocker
 * @package Mia\PHPUnit\Mock
 */
class InvocationMocker extends \PHPUnit_Framework_MockObject_InvocationMocker
{
    /**
     * @var array
     */
    protected $mIndexes = [];

    /**
     * @param \PHPUnit_Framework_MockObject_Matcher_Invocation $matcher
     */
    public function addMatcher(\PHPUnit_Framework_MockObject_Matcher_Invocation $matcher)
    {
        $this->matchers[] = $matcher;
        // it should reset indexes when adds new matcher
        $this->mIndexes   = [];
    }

    /**
     * @param \PHPUnit_Framework_MockObject_Invocation $invocation
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function & invoke(\PHPUnit_Framework_MockObject_Invocation $invocation)
    {
        $exception      = null;
        $hasReturnValue = false;
        $returnValue    = null;

        $matchers = $this->getIndexedMatchers($invocation);

        foreach ($matchers as $match) {
            try {
                if ($match->matches($invocation)) {
                    $value = $match->invoked($invocation);

                    if (!$hasReturnValue) {
                        $returnValue    = $value;

                        // Check by reference
                        if ($returnValue instanceof ReferenceWrapper) {
                            $returnValue = &$returnValue->getReference();
                        }

                        $hasReturnValue = true;
                    }
                }
            } catch (\Exception $e) {
                $exception = $e;
            }
        }

        if ($exception !== null) {
            throw $exception;
        }

        if ($hasReturnValue) {
            return $returnValue;
        } elseif (strtolower($invocation->methodName) == '__tostring') {
            return '';
        }

        $returnValue = $invocation->generateReturnValue();

        return $returnValue;
    }

    /**
     * @param \PHPUnit_Framework_MockObject_Invocation $invocation
     *
     * @return bool
     */
    public function matches(\PHPUnit_Framework_MockObject_Invocation $invocation)
    {
        $matchers = $this->getIndexedMatchers($invocation);

        foreach ($matchers as $matcher) {
            if (!$matcher->matches($invocation)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param \PHPUnit_Framework_MockObject_Invocation $invocation
     * @return mixed
     */
    protected function getIndexedMatchers(\PHPUnit_Framework_MockObject_Invocation $invocation)
    {
        if (!array_key_exists($invocation->methodName, $this->mIndexes)) {

            $this->mIndexes[$invocation->methodName] = [];

            foreach ($this->matchers as $index => $matcher) {

                $methodNameDecorator = new MethodNameDecorator($matcher->methodNameMatcher);
                if ($methodNameDecorator->matches($invocation)) {

                    $this->mIndexes[$invocation->methodName][] = $index;
                }
            }
        }

        return array_map(
            function ($index) {
                return $this->matchers[$index];
            },
            $this->mIndexes[$invocation->methodName]
        );
    }
}
