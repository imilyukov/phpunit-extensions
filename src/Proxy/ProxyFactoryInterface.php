<?php

namespace Mia\PHPUnit\Proxy;

/**
 * Interface ProxyFactoryInterface
 * @package Mia\PHPUnit\Proxy
 */
interface ProxyFactoryInterface
{
    /**
     * @param object|string $original
     * @return ProxyInterface
     */
    public function createProxy($original);
}
