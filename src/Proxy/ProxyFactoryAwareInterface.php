<?php

namespace Mia\PHPUnit\Proxy;

/**
 * Interface ProxyFactoryAwareInterface
 * @package Mia\PHPUnit\Proxy
 */
interface ProxyFactoryAwareInterface
{
    /**
     * @return ProxyFactoryInterface
     */
    public function getProxyFactory();

    /**
     * @param ProxyFactoryInterface $factory
     * @return mixed
     */
    public function setProxyFactory(ProxyFactoryInterface $factory = null);
}
