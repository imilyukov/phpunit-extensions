<?php

namespace Mia\PHPUnit\Proxy;

use Mia\PHPUnit\Mock\MockFactoryInterface;

/**
 * Class ProxyFactoryAwareTrait
 * @package Mia\PHPUnit\Proxy
 */
trait ProxyFactoryAwareTrait
{
    /**
     * @var ProxyFactoryInterface
     */
    protected $proxyFactory;

    /**
     * @return MockFactoryInterface
     */
    abstract public function getMockFactory();

    /**
     * @return ProxyFactoryInterface
     */
    public function getProxyFactory()
    {
        if (!$this->proxyFactory) {
            $this->proxyFactory = new ProxyFactory($this->getMockFactory());
        }

        return $this->proxyFactory;
    }

    /**
     * @param ProxyFactoryInterface $factory
     * @return $this
     */
    public function setProxyFactory(ProxyFactoryInterface $factory = null)
    {
        $this->proxyFactory = $factory;

        return $this;
    }
}
