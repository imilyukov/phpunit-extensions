<?php

namespace Mia\PHPUnit\Proxy;

use Mia\PHPUnit\Mock\Builder\MockBuilderInterface;
use Mia\PHPUnit\Mock\Method\MethodStubFactoryTrait;
use Mia\PHPUnit\Mock\MockFactoryAwareInterface;
use Mia\PHPUnit\Mock\MockFactoryInterface;
use Mia\PHPUnit\Mock\Property\ContextInterface;
use Mia\PHPUnit\Mock\Property\PropertyValueFactoryTrait;
use Mia\PHPUnit\Utility\Pipeline\PipelineBuilder;
use Mia\PHPUnit\Utility\Pipeline\PipelineInterface;

/**
 * Class ProxyFactory
 * @package Mia\PHPUnit\Proxy
 */
class ProxyFactory implements ProxyFactoryInterface
{
    use PropertyValueFactoryTrait, MethodStubFactoryTrait;

    /**
     * @var MockFactoryInterface
     */
    protected $mockFactory;

    /**
     * @var array
     */
    protected $excludedPrefixes = [
        'expects',
        'method',
        'getMockBuilder',
        '__clone',
        '__phpunit'
    ];

    /**
     * @var array
     */
    protected $proxies = [];

    /**
     * ProxyFactory constructor.
     * @param MockFactoryInterface $mockFactory
     */
    public function __construct(MockFactoryInterface $mockFactory)
    {
        $this->mockFactory = $mockFactory;
    }

    /**
     * @param object $original
     * @return mixed
     */
    public function createProxy($original)
    {
        $oid = is_object($original) ? spl_object_hash($original) : uniqid($original);

        if (!isset($this->proxies[$oid])) {

            /** @var ProxyInterface $proxy */
            $proxy = $this->mockFactory->createMock(
                $proxyClass = $this->createProxyClass($original),
                $this->createProxyPipeline($original)
            );

            // Define original mock
            $mockDefinition = $this->mockFactory->getMockDefinitionFactory()->createDefinition(
                get_parent_class($proxyClass)
            );

            if ($mockDefinition instanceof MockFactoryAwareInterface) {
                $mockDefinition->setMockFactory($this->mockFactory);
            }

            $mockDefinition->define($proxy->__phpunit_getMockBuilder());

            // Added default method mocks
            $proxy->__phpunit_setInitializer(
                function (ProxyInterface $proxy) {

                    $originalMock = $this->mockFactory->createMock(
                        $proxy->__phpunit_getOriginal()
                            ? get_class($proxy->__phpunit_getOriginal())
                            : get_parent_class(get_parent_class($proxy)),
                        $proxy->__phpunit_getMockBuilder()
                    );

                    $proxy->__phpunit_setOriginalMock($originalMock);
                }
            );

            if (is_object($original)) {
                $proxy->__phpunit_setOriginal($original);
            }

            $this->proxies[$oid]                    = $proxy;
            $this->proxies[spl_object_hash($proxy)] = $proxy;
        }

        return $this->proxies[$oid];
    }

    /**
     * @param $original
     * @return PipelineInterface
     */
    protected function createProxyPipeline($original)
    {
        $propertyPipelineBuilder = new PipelineBuilder();
        $propertyPipelineBuilder
            ->add(
                function (MockBuilderInterface $builder, array $propertyNames) {

                    foreach ($propertyNames as $propertyName) {

                        $builder
                            ->property($propertyName)
                            ->will(
                                $this->callableValue(
                                    function (ContextInterface $context) use ($propertyName) {

                                        /** @var ProxyInterface $proxy */
                                        $proxy    = $context->getMockObject();
                                        $builder  = $proxy->__phpunit_getMockBuilder();
                                        $original = $proxy->__phpunit_getOriginal();
                                        $property = $context->getClassReflector()->getProperty($propertyName);
                                        $property->setAccessible(true);

                                        if (!$original || $builder->hasProperty($property->getName())) {

                                            $originalMock = $proxy->__phpunit_getOriginalMock();

                                            $property = new \ReflectionProperty($originalMock, $property->getName());
                                            $property->setAccessible(true);

                                            $extractor = \Closure::bind(
                                                function & (\ReflectionProperty $property) {
                                                    $v = & $this->{$property->getName()};
                                                    return $v;
                                                },
                                                $originalMock, $originalMock
                                            );

                                            $result = & $extractor($property);

                                            return $this->referenceValue($result);
                                        }

                                        $result = $property->getValue($original);

                                        if (!is_object($result)) {
                                            return $this->referenceValue($result);
                                        }

                                        $reflector = new \ReflectionClass($result);

                                        if ($reflector->isUserDefined()) {
                                            $result = $this->createProxy($result);
                                        }

                                        return $result;
                                    }
                                )
                            )
                        ;
                    }
                }
            )
        ;

        $methodPipelineBuilder = new PipelineBuilder();
        $methodPipelineBuilder
            ->add(
                function (MockBuilderInterface $builder, array $methodNames) {

                    foreach ($methodNames as $methodName) {

                        $builder
                            ->method($methodName)
                            ->will(
                                $this->returnPropertyContext(
                                    function (ContextInterface $context, ...$parameters) use ($methodName) {

                                        /** @var ProxyInterface $proxy */
                                        $proxy   = $context->getMockObject();
                                        $builder = $proxy->__phpunit_getMockBuilder();
                                        $method  = $context->getClassReflector()->getMethod($methodName);
                                        $method->setAccessible(true);

                                        if (!$proxy->__phpunit_getOriginal() || $builder->hasMethod($method->getName())) {

                                            $originalMock = $proxy->__phpunit_getOriginalMock();

                                            $method = new \ReflectionMethod($originalMock, $method->getName());
                                            $method->setAccessible(true);

                                            return $method->invokeArgs($originalMock, $this->resolveParameters($method, $parameters));
                                        }

                                        $result = $method->invokeArgs($proxy, $this->resolveParameters($method, $parameters));

                                        if (!is_object($result)) {
                                            return $result;
                                        }

                                        $reflector = new \ReflectionClass($result);

                                        if ($reflector->isUserDefined()) {
                                            $result = $this->createProxy($result);
                                        }

                                        return $result;
                                    }
                                )
                            )
                        ;
                    }
                }
            )
        ;

        $proxyPipelineBuilder = new PipelineBuilder();
        $proxyPipelineBuilder
            ->add(
                function (MockBuilderInterface $builder) use ($original, $propertyPipelineBuilder, $methodPipelineBuilder) {

                    $originalReflector = new \ReflectionClass($original);

                    $propertyNames = array_map(
                        function (\ReflectionProperty $property) {
                            return $property->getName();
                        },
                        array_filter(
                            $originalReflector->getProperties(),
                            function (\ReflectionProperty $property) {
                                return !$this->isExcludedName($property->getName());
                            }
                        )
                    );

                    $propertyPipelineBuilder->build()->process([$builder, $propertyNames]);

                    $methodNames = array_map(
                        function (\ReflectionMethod $method) {
                            return $method->getName();
                        },
                        array_filter(
                            $originalReflector->getMethods(),
                            function (\ReflectionMethod $method) {
                                $condition = $method->isConstructor() ||
                                    $method->isFinal() ||
                                    $method->isPrivate() ||
                                    $this->isExcludedName($method->getName())
                                ;

                                return !$condition;
                            }
                        )
                    );

                    $methodPipelineBuilder->build()->process([$builder, $methodNames]);
                }
            )
        ;

        return $proxyPipelineBuilder->build();
    }

    /**
     * @param $original
     * @return string
     */
    protected function createProxyClass($original)
    {
        if (is_object($original)) {
            $original = get_class($original);
        }

        $proxyClass = 'Proxy_' . md5($original);

        if (!class_exists($proxyClass, false)) {

            $reflection  = new \ReflectionClass(\PHPUnit_Framework_MockObject_Generator::class);
            $templateDir = dirname($reflection->getFileName()) . DIRECTORY_SEPARATOR . 'Generator' . DIRECTORY_SEPARATOR;

            $classTemplate = new \Text_Template($templateDir . 'trait_class.tpl');
            $classTemplate->setVar(
                [
                    'prologue'   => '',
                    'class_name' => sprintf(
                        '%s extends %s implements %s',
                        $proxyClass,
                        $original,
                        ProxyInterface::class
                    ),
                    'trait_name' => ProxyTrait::class
                ]
            );

            eval($classTemplate->render());
        }

        return $proxyClass;
    }

    /**
     * @param \ReflectionMethod $method
     * @param array $parameters
     * @return array
     */
    protected function resolveParameters(\ReflectionMethod $method, array $parameters)
    {
        // Create reference by signature
        foreach ($method->getParameters() as $parameter) {
            if (isset($parameters[$parameter->getPosition()])) {
                if (!$parameter->isPassedByReference()) {
                    continue;
                }

                list ($parameters[$parameter->getPosition()]) = [&$parameters[$parameter->getPosition()]];
            }
        }

        return $parameters;
    }

    /**
     * @param $name
     * @return int
     */
    protected function isExcludedName($name)
    {
        $pattern = sprintf(
            '/^(%s)/',
            implode('|', $this->excludedPrefixes)
        );

        return preg_match($pattern, $name);
    }
}
