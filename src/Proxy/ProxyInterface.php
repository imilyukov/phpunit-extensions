<?php

namespace Mia\PHPUnit\Proxy;

use Mia\PHPUnit\Mock\Builder\MockBuilderInterface;

/**
 * Interface ProxyInterface
 * @package Mia\PHPUnit\Proxy
 */
interface ProxyInterface
{
    /**
     * @return MockBuilderInterface
     */
    public function getMockBuilder();

    /**
     * @return MockBuilderInterface
     */
    public function __phpunit_getMockBuilder();

    /**
     * @param MockBuilderInterface $builder
     * @return mixed
     */
    public function __phpunit_setMockBuilder(MockBuilderInterface $builder);

    /**
     * @param \Closure|null $initializer
     * @return mixed
     */
    public function __phpunit_setInitializer(\Closure $initializer = null);

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function __phpunit_getOriginalMock();

    /**
     * @param \PHPUnit_Framework_MockObject_MockObject $originalMock
     * @return mixed
     */
    public function __phpunit_setOriginalMock(\PHPUnit_Framework_MockObject_MockObject $originalMock);

    /**
     * @return object
     */
    public function __phpunit_getOriginal();

    /**
     * @param $original
     * @return $this
     */
    public function __phpunit_setOriginal($original);
}
