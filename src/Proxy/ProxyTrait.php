<?php

namespace Mia\PHPUnit\Proxy;

use Mia\PHPUnit\Mock\Builder\MockBuilder as ProxyMockBuilder;
use Mia\PHPUnit\Mock\Builder\MockBuilderInterface;

/**
 * Class ProxyTrait
 * @package Mia\PHPUnit\Proxy
 */
trait ProxyTrait
{
    /**
     * @var \Closure|null
     */
    private $__phpunit_initializer;

    /**
     * @var MockBuilderInterface
     */
    private $__phpunit_mockBuilder;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $__phpunit_originalMock;

    /**
     * @var object
     */
    private $__phpunit_original;

    /**
     * @return MockBuilderInterface
     */
    public function getMockBuilder()
    {
        $this->__phpunit_originalMock = null; // reset mock

        return $this->__phpunit_getMockBuilder();
    }

    /**
     * @return MockBuilderInterface
     */
    public function __phpunit_getMockBuilder()
    {
        if (!$this->__phpunit_mockBuilder) {
            $this->__phpunit_mockBuilder = new ProxyMockBuilder();
        }

        return $this->__phpunit_mockBuilder;
    }

    /**
     * @param MockBuilderInterface $builder
     * @return mixed
     */
    public function __phpunit_setMockBuilder(MockBuilderInterface $builder)
    {
        $this->__phpunit_mockBuilder = $builder;

        return $this;
    }

    /**
     * @param \Closure|null $initializer
     * @return $this
     */
    public function __phpunit_setInitializer(\Closure $initializer = null)
    {
        $this->__phpunit_initializer = $initializer;

        return $this;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function __phpunit_getOriginalMock()
    {
        !$this->__phpunit_originalMock && $this->__phpunit_initializer->__invoke($this);

        return $this->__phpunit_originalMock;
    }

    /**
     * @param \PHPUnit_Framework_MockObject_MockObject $originalMock
     * @return mixed
     */
    public function __phpunit_setOriginalMock(\PHPUnit_Framework_MockObject_MockObject $originalMock)
    {
        $this->__phpunit_originalMock = $originalMock;

        return $this;
    }

    /**
     * @return object
     */
    public function __phpunit_getOriginal()
    {
        return $this->__phpunit_original;
    }

    /**
     * @param $original
     * @return $this
     */
    public function __phpunit_setOriginal($original)
    {
        $this->__phpunit_original = $original;

        return $this;
    }

    /**
     * Clone
     */
    public function __clone()
    {
        $originalReflector = new \ReflectionClass(
            $this->__phpunit_getOriginal()
                ? get_class($this->__phpunit_getOriginal())
                : get_parent_class(get_parent_class($this))
        );

        if (static::class !== $originalReflector->getName() && $originalReflector->hasMethod('__clone')) {
            $originalReflector->getMethod('__clone')->invoke($this);
        }

        $this->__phpunit_mockBuilder = clone $this->__phpunit_getMockBuilder();
    }
}
