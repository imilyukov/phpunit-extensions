<?php

namespace Mia\PHPUnit\Comparator;

/**
 * Interface PriorityAwareInterface
 * @package Mia\PHPUnit\Comparator
 */
interface PriorityAwareInterface
{
    /**
     * @return integer
     */
    public function getPriority();
}
