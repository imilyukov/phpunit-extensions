<?php

namespace Mia\PHPUnit\Comparator;

/**
 * Class ConstraintComparator
 * @package Mia\PHPUnit\Comparator
 */
class ConstraintComparator
{
    /**
     * @return array
     */
    public static function getConstraintPriorityList()
    {
        return [
            \PHPUnit_Framework_Constraint_IsEqual::class,
            \PHPUnit_Framework_Constraint_IsIdentical::class,
            \PHPUnit_Framework_Constraint_Callback::class,
            \PHPUnit_Framework_Constraint_IsType::class,
            \PHPUnit_Framework_Constraint_IsInstanceOf::class,
            \PHPUnit_Framework_Constraint_Callback::class,
            \PHPUnit_Framework_Constraint::class,
            \PHPUnit_Framework_Constraint_IsAnything::class,
        ];
    }

    /**
     * @param \PHPUnit_Framework_Constraint $current
     * @param \PHPUnit_Framework_Constraint $next
     * @return int
     */
    public static function compare(\PHPUnit_Framework_Constraint $current, \PHPUnit_Framework_Constraint $next)
    {
        $constraintPriorityList = self::getConstraintPriorityList();

        $currentPriority = $nextPriority = 0;

        foreach ($constraintPriorityList as $priority => $className) {

            if ($current instanceof $className) {
                $currentPriority += $priority;
            }

            if ($current instanceof PriorityAwareInterface) {
                $currentPriority += $current->getPriority();
            }

            if ($next instanceof $className) {
                $nextPriority += $priority;
            }

            if ($next instanceof PriorityAwareInterface) {
                $nextPriority += $next->getPriority();
            }
        }

        return $currentPriority - $nextPriority;
    }
}
