<?php

namespace Mia\PHPUnit\Assert;

use Mia\PHPUnit\Constraint\AggregateConstraint;
use Mia\PHPUnit\Constraint\Definition\Builder\ConstraintBuilder;
use Mia\PHPUnit\Constraint\Tree\Builder\NodeBuilderAwareTrait;
use Mia\PHPUnit\Constraint\Tree\Builder\TreeBuilder;
use Mia\PHPUnit\Constraint\TreeConstraint;

/**
 * Class ConstraintAssertTrait
 * @package Mia\PHPUnit\Assert
 *
 * @author Ivan Miliukou
 */
trait ConstraintAssertTrait
{
    use NodeBuilderAwareTrait;

    /**
     * @param $actual
     * @param callable|TreeBuilder $expected
     * @param string $message
     *
     * @example
     *
     * <code>
     *
     * use Mia\PHPUnit\Constraint\Tree\Builder\TreeBuilder;
     *
     * $json = '{
     *     settings: {
     *         share: {
     *             shortenerURL: "http://www.mtv-l.mtvi.com/feeds/bitly",
     *             twitter: {
     *                 name: "MTV"
     *             },
     *             email: {
     *                 subject: "Watch {{title}} on www.mtv.com"
     *             },
     *             options: [
     *                 {
     *                     title: "Facebook",
     *                     typeClass: "facebook"
     *                 },
     *                 {
     *                     title: "Twitter",
     *                     typeClass: "twitter"
     *                 },
     *                 {
     *                     title: "Email",
     *                     typeClass: "email"
     *                 },
     *                 {
     *                     title: "Embed",
     *                     typeClass: "embed"
     *                 }
     *             ]
     *         }
     *     }
     * }';
     *
     * $objectGraph = json_decode($json);
     *
     * $this->assertSameDefinitionTree(
     *     $objectGraph,
     *     function (TreeBuilder $builder) {
     *         // Configure builder
     *         $builder->root()
     *             ->children()
     *                 ->objectNode('settings')
     *                     ->children()
     *                         ->objectNode('share')
     *                             ->children()
     *                                 ->scalarNode('shortenerURL')->end()
     *                                 ->objectNode('twitter')
     *                                     ->children()
     *                                         ->scalarNode('name')
     *                                             ->constraints()
     *                                                 ->assertIsEqual('MTV')
     *                                                     ->message('Is not equal "MTV"')
     *                                                 ->end()
     *                                             ->end()
     *                                         ->end()
     *                                     ->end()
     *                                 ->end()
     *                                 ->objectNode('email')
     *                                     ->children()
     *                                         ->scalarNode('subject')->end()
     *                                     ->end()
     *                                 ->end()
     *                                 ->arrayNode('options')
     *                                     ->constraints()
     *                                         ->assertCount(4)
     *                                             ->message('Count is not equal 4')
     *                                         ->end()
     *                                     ->end()
     *                                     ->prototype('object')
     *                                         ->constraints()
     *                                             ->assertCallback(
     *                                                 function ($other) {
     *                                                     return $other['typeClass'] === strtolower($other['title']);
     *                                                 }
     *                                             )
     *                                                 ->message('Properties "title" and "typeClass" are not similar')
     *                                             ->end()
     *                                         ->end()
     *                                         ->children()
     *                                             ->scalarNode('title')->end()
     *                                             ->scalarNode('typeClass')
     *                                                 ->constraints()
     *                                                     ->assertOr()
     *                                                         ->constraints()
     *                                                             ->assertIsEqual('facebook')->end()
     *                                                             ->assertIsEqual('twitter')->end()
     *                                                             ->assertIsEqual('email')->end()
     *                                                             ->assertIsEqual('embed')->end()
     *                                                         ->end()
     *                                                         ->message('The value is not in the list: "facebook", "twitter", "email", "embed"')
     *                                                     ->end()
     *                                                 ->end()
     *                                             ->end()
     *                                         ->end()
     *                                     ->end()
     *                                 ->end()
     *                             ->end()
     *                         ->end()
     *                     ->end()
     *                 ->end()
     *             ->end()
     *         ;
     *     }
     * );
     *
     * </code>
     */
    public function assertSameDefinitionTree($actual, callable $expected, $message = '')
    {
        \PHPUnit_Framework_Assert::assertThat($actual, $this->treeConstraint($expected), $message);
    }

    /**
     * @param callable|TreeBuilder $builder
     * @return TreeConstraint
     */
    public function treeConstraint(callable $builder = null)
    {
        $treeBuilder = new TreeBuilder();
        $treeBuilder->setNodeBuilder($this->getNodeBuilder());

        if ($builder) {
            call_user_func($builder, $treeBuilder);
        }

        return new TreeConstraint($treeBuilder->buildTree());
    }

    /**
     * @param $actual
     * @param callable|ConstraintBuilder $expected
     * @param string $message
     */
    public function assertSameDefinition($actual, callable $expected, $message = '')
    {
        \PHPUnit_Framework_Assert::assertThat($actual, $this->aggregateConstraint($expected), $message);
    }

    /**
     * @param callable|ConstraintBuilder|null $builder
     * @return AggregateConstraint
     */
    public function aggregateConstraint(callable $builder = null)
    {
        $constraint = new AggregateConstraint();

        if ($builder) {
            call_user_func($builder, $constraint);
        }

        return $constraint;
    }
}
