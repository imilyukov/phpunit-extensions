<?php

namespace Mia\PHPUnit\Utility\Pipeline;

/**
 * Class PipelineBuilder
 * @package Mia\PHPUnit\Utility\Pipeline
 */
class PipelineBuilder implements PipelineBuilderInterface
{
    /**
     * @var StageInterface[]
     */
    protected $stages = [];

    /**
     * @param \callable[] ...$handlers
     * @return mixed
     */
    public function add(callable ...$handlers)
    {
        $this->stages[] = new Stage($handlers);

        return $this;
    }

    /**
     * @param ProcessorInterface|null $processor
     * @return PipelineInterface
     */
    public function build(ProcessorInterface $processor = null)
    {
        return new Pipeline($this->stages, $processor);
    }

    /**
     * @param array ...$payload
     * @return mixed
     */
    public function __invoke(...$payload)
    {
        return $this->build();
    }
}
