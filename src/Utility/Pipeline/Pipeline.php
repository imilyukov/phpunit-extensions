<?php

namespace Mia\PHPUnit\Utility\Pipeline;

/**
 * Class Pipeline
 * @package Mia\PHPUnit\Utility\Pipeline
 */
class Pipeline implements PipelineInterface
{
    /**
     * @var StageInterface[]
     */
    protected $stages = [];

    /**
     * @var ProcessorInterface
     */
    protected $processor;

    /**
     * Pipeline constructor.
     * @param array $stages
     * @param ProcessorInterface|null $processor
     */
    public function __construct(array $stages = [], ProcessorInterface $processor = null)
    {
        $this->stages    = $stages;
        $this->processor = $processor ?: new Processor();
    }

    /**
     * @param StageInterface $stage
     * @return PipelineInterface
     */
    public function pipe(StageInterface $stage)
    {
        $pipeline = clone $this;
        $pipeline->stages[] = $stage;

        return $pipeline;
    }

    /**
     * @param array $payload
     * @return mixed
     */
    public function process(array $payload = [])
    {
        return $this->processor->process($this, $payload);
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->stages);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->stages);
    }

    /**
     * @param array ...$payload
     * @return array
     */
    public function __invoke(...$payload)
    {
        return $this->process($payload);
    }
}
