<?php

namespace Mia\PHPUnit\Utility\Pipeline;

/**
 * Interface PipelineInterface
 * @package Mia\PHPUnit\Utility\Pipeline
 */
interface PipelineInterface extends InvocationInterface, \IteratorAggregate, \Countable
{
    /**
     * @param StageInterface $stage
     * @return PipelineInterface
     */
    public function pipe(StageInterface $stage);

    /**
     * @param array $payload
     * @return mixed
     */
    public function process(array $payload = []);
}
