<?php

namespace Mia\PHPUnit\Utility\Pipeline;

/**
 * Interface StageInterface
 * @package Mia\PHPUnit\Utility\Pipeline
 */
interface StageInterface extends InvocationInterface, \Countable
{
    /**
     * @param array $handlers
     * @return mixed
     */
    public function setHandlers(array $handlers);
}
