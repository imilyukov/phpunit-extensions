<?php

namespace Mia\PHPUnit\Utility\Pipeline;

/**
 * Class Stage
 * @package Mia\PHPUnit\Utility\Pipeline
 */
class Stage implements StageInterface
{
    /**
     * @var callable[]
     */
    protected $handlers;

    /**
     * Stage constructor.
     * @param callable[] $handlers
     */
    public function __construct(array $handlers)
    {
        $this->handlers = $handlers;
    }

    /**
     * @param array $handlers
     * @return $this
     */
    public function setHandlers(array $handlers)
    {
        $this->handlers = $handlers;

        return $this;
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->handlers);
    }

    /**
     * @param array ...$payload
     * @return array
     */
    public function __invoke(...$payload)
    {
        return array_map(
            function (callable $handler) use ($payload) {
                return call_user_func_array($handler, $payload);
            },
            $this->handlers
        );
    }
}
