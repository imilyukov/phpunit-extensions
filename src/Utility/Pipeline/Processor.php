<?php

namespace Mia\PHPUnit\Utility\Pipeline;

/**
 * Class Processor
 * @package Mia\PHPUnit\Utility\Pipeline
 */
class Processor implements ProcessorInterface
{
    /**
     * @param PipelineInterface $pipeline
     * @param array $payload
     * @return mixed
     */
    public function process(PipelineInterface $pipeline, array $payload)
    {
        if (!count($pipeline)) {

            return $payload;
        }

        /** @var \Iterator $iterator */
        $iterator = $pipeline->getIterator();

        do {

            $stage   = $iterator->current();
            $payload = call_user_func_array($stage, $payload);

            $iterator->next();

        } while ($iterator->valid());

        return count($stage) > 1 ? $payload : array_shift($payload);
    }
}
