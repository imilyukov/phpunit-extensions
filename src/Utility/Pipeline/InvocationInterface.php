<?php

namespace Mia\PHPUnit\Utility\Pipeline;

/**
 * Interface InvocationInterface
 * @package Mia\PHPUnit\Utility\Pipeline
 */
interface InvocationInterface
{
    /**
     * @param array ...$payload
     * @return mixed
     */
    public function __invoke(...$payload);
}
