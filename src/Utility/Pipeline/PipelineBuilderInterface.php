<?php

namespace Mia\PHPUnit\Utility\Pipeline;

/**
 * Interface PipelineBuilderInterface
 * @package Mia\PHPUnit\Utility\Pipeline
 */
interface PipelineBuilderInterface
{
    /**
     * @param \callable[] ...$handlers
     * @return mixed
     */
    public function add(callable ...$handlers);

    /**
     * @param ProcessorInterface|null $processor
     * @return mixed
     */
    public function build(ProcessorInterface $processor = null);
}
