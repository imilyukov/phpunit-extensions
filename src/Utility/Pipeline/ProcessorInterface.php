<?php

namespace Mia\PHPUnit\Utility\Pipeline;

/**
 * Interface ProcessorInterface
 * @package Mia\PHPUnit\Utility\Pipeline
 */
interface ProcessorInterface
{
    /**
     * @param PipelineInterface $pipeline
     * @param array $payload
     * @return mixed
     */
    public function process(PipelineInterface $pipeline, array $payload);
}
