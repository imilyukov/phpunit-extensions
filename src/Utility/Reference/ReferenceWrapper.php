<?php

namespace Mia\PHPUnit\Utility\Reference;

/**
 * Class ReferenceWrapper
 * @package Mia\PHPUnit\Utility\Reference
 */
class ReferenceWrapper
{
    /**
     * @var mixed
     */
    protected $reference;

    /**
     * ReferenceWrapper constructor.
     * @param mixed $reference
     */
    public function __construct(&$reference)
    {
        $this->reference = &$reference;
    }

    /**
     * @return mixed
     */
    public function & getReference()
    {
        $reference = &$this->reference;

        if (is_array($reference)) {
            array_walk_recursive(
                $reference,
                function (&$property) {
                    list ($property) = [&$property];
                }
            );
        }
        
        return $reference;
    }
}
