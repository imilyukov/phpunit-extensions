<?php

namespace Mia\PHPUnit\Utility\Iterator;

/**
 * Class GeneratorIterator
 * @package Mia\PHPUnit\Utility\Iterator
 */
class GeneratorIterator implements \ArrayAccess, \Countable, \OuterIterator, \SeekableIterator
{
    /**
     * @var \ArrayAccess|\Countable|\Iterator|
     */
    protected $caching;

    /**
     * @var \Iterator
     */
    protected $iterator;

    /**
     * GeneratorIterator constructor.
     * @param \Generator $generator
     */
    public function __construct(\Generator $generator)
    {
        $this->caching  = new \CachingIterator($generator, \CachingIterator::FULL_CACHE);
        $this->rewind();
    }

    /**
     * @return \Generator
     */
    public function getInnerIterator()
    {
        foreach ($this->caching->getCache() as $key => $value) {
            yield $key => $value;
        }

        while ($this->caching->getInnerIterator()->valid()) {

            $this->caching->next();

            $key   = $this->caching->key();
            $value = $this->caching->current();

            yield $key => $value;
        }
    }

    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        return $this->iterator->current();
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        $this->iterator->next();
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        return $this->iterator->key();
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid()
    {
        return $this->iterator->valid();
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        $this->iterator = $this->getInnerIterator();
    }

    /**
     * Seeks to a position
     * @link http://php.net/manual/en/seekableiterator.seek.php
     * @param int $position <p>
     * The position to seek to.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function seek($position)
    {
        while ($this->caching->getInnerIterator()->valid()) {

            $this->caching->next();

            if ($this->caching->offsetExists($position)) {
                return;
            }
        }
    }

    /**
     * @return int
     */
    public function count()
    {
        while ($this->caching->getInnerIterator()->valid()) {

            $this->caching->next();
        }

        return $this->caching->count();
    }

    /**
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        $this->seek($offset);

        return $this->caching->offsetExists($offset);
    }

    /**
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        $this->seek($offset);

        return $this->caching->offsetGet($offset);
    }

    /**
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        $this->seek($offset);

        $this->caching->offsetSet($offset, $value);
    }

    /**
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        $this->seek($offset);

        $this->caching->offsetUnset($offset);
    }
}
