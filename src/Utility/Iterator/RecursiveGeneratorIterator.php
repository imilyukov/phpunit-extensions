<?php

namespace Mia\PHPUnit\Utility\Iterator;

/**
 * Class RecursiveGeneratorIterator
 * @package Mia\PHPUnit\Utility\Iterator
 */
class RecursiveGeneratorIterator implements \RecursiveIterator
{
    /**
     * @var \ArrayIterator
     */
    protected $iterator;

    /**
     * RecursiveGeneratorIterator constructor.
     * @param GeneratorIterator $iterator
     */
    public function __construct(GeneratorIterator $iterator)
    {
        $this->iterator = $iterator;
    }

    /**
     * {@inheritdoc}
     */
    public function getChildren()
    {
        return new self($this->current());
    }

    /**
     * {@inheritdoc}
     */
    public function hasChildren()
    {
        return $this->current() instanceof GeneratorIterator;
    }

    /**
     * {@inheritdoc}
     */
    public function current()
    {
        return $this->iterator->current();
    }

    /**
     * {@inheritdoc}
     */
    public function next()
    {
        $this->iterator->next();
    }

    /**
     * {@inheritdoc}
     */
    public function key()
    {
        return $this->iterator->key();
    }

    /**
     * {@inheritdoc}
     */
    public function valid()
    {
        return $this->iterator->valid();
    }

    /**
     * {@inheritdoc}
     */
    public function rewind()
    {
        $this->iterator->rewind();
    }
}
