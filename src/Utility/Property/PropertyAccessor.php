<?php

namespace Mia\PHPUnit\Utility\Property;

use Mia\PHPUnit\Utility\Iterator\GeneratorIterator;
use Mia\PHPUnit\Utility\Iterator\RecursiveGeneratorIterator;

/**
 * Class PropertyAccessor
 * @package Mia\PHPUnit\Utility\Property
 *
 * @author Ivan Miliukou
 */
class PropertyAccessor
{
    const ARRAY_ACCESSOR = '[]';

    const OBJECT_ACCESSOR = '.';

    /**
     * @var string
     */
    private $prefixPath = '';

    /**
     * @param $objectOrArray
     * @param $propertyPath
     *
     * @example
     *
     * <code>
     * $objectOrArray = [
     *     'name'  => 'Post Name',
     *     'type'  => 'Post',
     *     'items' => [
     *        [
     *            'type'   => 'Gallery',
     *            'images' => [
     *                [
     *                    'alt' => 'Image Alt1',
     *                    'url' => 'http://image1'
     *                ],
     *                [
     *                    'alt' => 'Image Alt2',
     *                    'url' => 'http://image2'
     *                ],
     *            ]
     *        ]
     *     ]
     * ];
     * $propertyPath = '.items[0].images[].url';
     *
     * $propertyAccessor = new PropertyAccessor();
     * $result = $propertyAccessor->getValue($objectOrArray, $propertyPath);
     * print_r($result instanceof \Iterator); // true
     * echo PHP_EOL;
     * print_r(iterator_to_array($result));
     * // Print out returns:
     * // Array (
     * //     [.items[0].images[0].url] => http://image1
     * //     [.items[0].images[1].url] => http://image2
     * // )
     * </code>
     *
     * @return mixed
     */
    public function getValue($objectOrArray, $propertyPath)
    {
        $remaining = $propertyPath;
        $position  = 0;
        $pattern   = '/^([\.]{0,1}([^\.|\[]++)|\[([^\]]++)\]|(\[\]))(.*)$/';
        $value     = $objectOrArray;

        while (preg_match($pattern, $remaining, $matches)) {

            list ($element, $property, $index, $traversable, $remaining) = array_slice($matches, 1);

            $position += strlen($element);

            switch (true) {
                // If .anyObjectOrArray.{property}...
                case '' !== $property:
                    if (is_array($value) || $value instanceof \ArrayAccess) {
                        if (!array_key_exists($property, $value)) {
                            throw new \UnexpectedValueException(
                                sprintf(
                                    "Key %s doesn't exist by path %s",
                                    $property,
                                    $this->prefixPath . $propertyPath
                                )
                            );
                        }

                        $value = $value[$property];
                    } else {
                        if (!property_exists($value, $property)) {
                            throw new \UnexpectedValueException(
                                sprintf(
                                    "Property %s doesn't exist or isn't accessable by path %s",
                                    $property,
                                    $this->prefixPath . $propertyPath
                                )
                            );
                        }

                        $value = $value->$property;
                    }
                    break;
                // If .anyArrayOrArrayAccess[{index}]...
                case '' !== $index:
                    if (!array_key_exists($index, $value)) {
                        throw new \UnexpectedValueException(
                            sprintf(
                                "Index %s doesn't exist by path %s",
                                $index,
                                $this->prefixPath . $propertyPath
                            )
                        );
                    }

                    $value = $value[$index];
                    break;
                // If .anyArrayOrTraversable{[]}...
                case '' !== $traversable:
                    return new GeneratorIterator($this->getGenerator($value, $propertyPath, $position));
                default:
                    throw new \UnexpectedValueException(
                        sprintf(
                            'Could not parse property path "%s". Unexpected token "%s" at position %d',
                            $this->prefixPath . $propertyPath,
                            $remaining[0],
                            $position
                        )
                    );
            }
        }

        $this->prefixPath = '';

        return $value;
    }

    /**
     * @param $iterator
     * @param $propertyPath
     * @param $position
     * @return \Generator
     */
    private function getGenerator($iterator, $propertyPath, $position)
    {
        foreach ($iterator as $index => $element) {

            $elementPath = substr_replace($propertyPath, '[' . $index . ']', strpos($propertyPath, '[]'), strlen('[]'));

            if (!is_scalar($element)) {

                // For correct tracing path in exception messages
                $this->prefixPath = substr($elementPath, 0, $position + strlen($index . ''));

                $element = $this->getValue($element, substr($propertyPath, $position));
                if ($element instanceof GeneratorIterator) {

                    $iterator = new \RecursiveIteratorIterator(
                        new RecursiveGeneratorIterator($element),
                        \RecursiveIteratorIterator::SELF_FIRST
                    );

                    foreach ($iterator as $subPath => $item) {

                        $fullPath = str_replace(preg_replace('/\[\d+\]/', '[]', $subPath), '', $elementPath) . $subPath;

                        yield $fullPath => $item;
                    }

                    continue;
                }
            }

            yield $elementPath => $element;
        }
    }
}
