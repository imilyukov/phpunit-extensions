<?php

namespace Mia\PHPUnit\Tests\Proxy;

use Mia\PHPUnit\Mock\Builder\MockBuilderInterface;
use Mia\PHPUnit\Mock\Definition\MockDefinitionRegistryInterface;
use Mia\PHPUnit\Mock\Definition\MockDefinitionRegistryTrait;
use Mia\PHPUnit\Mock\Method\MethodStubFactoryTrait;
use Mia\PHPUnit\Mock\MockBuilderFactoryTrait;
use Mia\PHPUnit\Mock\MockFactoryAwareInterface;
use Mia\PHPUnit\Mock\MockFactoryAwareTrait;
use Mia\PHPUnit\Mock\Property\ContextInterface;
use Mia\PHPUnit\Mock\Property\PropertyValueFactoryTrait;
use Mia\PHPUnit\Proxy\ProxyFactoryAwareInterface;
use Mia\PHPUnit\Proxy\ProxyFactoryAwareTrait;
use Mia\PHPUnit\Proxy\ProxyInterface;
use Mia\PHPUnit\Tests\Proxy\Data\TestNode;
use Mia\PHPUnit\Tests\Proxy\Data\TestTree;
use Psr\Container\ContainerInterface;

/**
 * Class ProxyTest
 * @package Mia\PHPUnit\Tests\Proxy
 */
class ProxyTest extends \PHPUnit_Framework_TestCase
    implements MockFactoryAwareInterface, ProxyFactoryAwareInterface, MockDefinitionRegistryInterface
{
    use MockBuilderFactoryTrait,
        MockFactoryAwareTrait,
        ProxyFactoryAwareTrait,
        PropertyValueFactoryTrait,
        MethodStubFactoryTrait,
        MockDefinitionRegistryTrait;

    public function testProxyFactory()
    {
        $container = $this->getProxyFactory()->createProxy(
            $this->getMockFactory()->createMock(ContainerInterface::class)
        );

        $mockBuilder = $container->getMockBuilder();
        $mockBuilder
            ->property('container')
            ->with(
                new \PHPUnit_Framework_Constraint_IsInstanceOf(ContainerInterface::class)
            )
            ->will(
                $this->callableValue(
                    function (ContextInterface $context) {
                        return $context->getMockObject();
                    }
                )
            )
        ;

        $mockBuilder
            ->method('has')
            ->with(
                new \PHPUnit_Framework_Constraint_IsType(\PHPUnit_Framework_Constraint_IsType::TYPE_STRING)
            )
            ->will(
                $this->returnPropertyContext(
                    function (ContextInterface $context, $name) {

                        return $context->hasProperty($name);
                    }
                )
            )
        ;

        $mockBuilder
            ->method('get')
            ->with(
                new \PHPUnit_Framework_Constraint_IsType(\PHPUnit_Framework_Constraint_IsType::TYPE_STRING)
            )
            ->will(
                $this->returnPropertyContext(
                    function (ContextInterface $context, $name) {

                        /** @var ContainerInterface $container */
                        $container = $context->getMockObject();

                        if (!$container->has($name)) {

                            return new \PHPUnit_Framework_MockObject_Stub_Exception(
                                new \RuntimeException(
                                    sprintf("Service with name \"%s\" doesn't exist", $name)
                                )
                            );
                        }

                        return $context->getPropertyValue($name);
                    }
                )
            )
        ;

        $this->assertInstanceOf(ContainerInterface::class, $container->get('container'));
        $this->assertInstanceOf(\PHPUnit_Framework_MockObject_MockObject::class, $container->get('container'));
    }

    public function testForTreeStructures()
    {
        $proxyNode = $this->getProxyFactory()->createProxy(new TestNode());

        $tree = new TestTree($proxyNode, 12);
        $root = $tree->getRoot();

        $this->assertInstanceOf(TestNode::class,       $root);
        $this->assertInstanceOf(ProxyInterface::class, $root);

        /** @var TestNode|ProxyInterface $node */
        foreach ($tree as $index => $node) {

            /** @var TestNode|ProxyInterface $proxyNode */
            $proxyNode = $this->getProxyFactory()->createProxy($node);

            $this->assertInstanceOf(TestNode::class,       $proxyNode);
            $this->assertInstanceOf(ProxyInterface::class, $proxyNode);

            $mockBuilder = $proxyNode->getMockBuilder();
            $mockBuilder
                ->property('uuid')
                ->will(
                    $this->callableValue(
                        function () {
                            return 'TestId';
                        }
                    )
                )
            ;

            $mockBuilder
                ->method('getId')
                ->will(
                    $this->returnPropertyContext(
                        function (ContextInterface $context) {
                            return $context->getPropertyValue('uuid');
                        }
                    )
                )
            ;

            $this->assertInstanceOf(\DateTime::class, $proxyNode->getCreatedAt());
            $this->assertEquals('TestId',             $proxyNode->getId());
        }

        /** @var TestNode|ProxyInterface $proxyNode */
        $proxyNode = $this->getProxyFactory()->createProxy(TestNode::class);

        $this->assertInstanceOf(ProxyInterface::class, $proxyNode);

        $mockBuilder = $proxyNode->getMockBuilder();
        $mockBuilder
            ->property('uuid')
            ->will(
                $this->callableValue(
                    function () {
                        return 'TestId';
                    }
                )
            )
        ;

        $mockBuilder
            ->method('getId')
            ->will(
                $this->returnPropertyContext(
                    function (ContextInterface $context) {
                        return $context->getPropertyValue('uuid');
                    }
                )
            )
        ;

        $this->assertEquals('TestId', $proxyNode->getId());
    }

    public function testProxyVsMock()
    {
        $tree = new TestTree();
        $root = $tree->getRoot();

        /** @var TestNode|\PHPUnit_Framework_MockObject_MockObject $rootMock */
        $rootMock = $this->getMockBuilder(get_class($root))
            ->setMethods(get_class_methods($root))
            //->enableProxyingToOriginalMethods()
            ->setProxyTarget($root)
            ->getMock()
        ;

        $rootMock
            ->method('getId')
            ->will(
                $this->returnCallback(
                    function () {
                        return uniqid();
                    }
                )
            )
        ;

        $this->assertNotEquals($root->getId(),        $rootMock->getId());
        $this->assertNotEquals($root->getCreatedAt(), $rootMock->getCreatedAt());
        $this->assertNotEquals($root->getUpdatedAt(), $rootMock->getUpdatedAt());

        /** @var TestNode|ProxyInterface $rootProxy */
        $rootProxy = $this->getProxyFactory()->createProxy($root);
        $rootProxy->getMockBuilder()
            ->method('getId')
            ->will(
                $this->returnCallback(
                    function () {
                        return uniqid();
                    }
                )
            )
        ;

        $this->assertNotEquals($root->getId(),     $rootProxy->getId());
        $this->assertEquals($root->getCreatedAt(), $rootProxy->getCreatedAt());
        $this->assertEquals($root->getUpdatedAt(), $rootProxy->getUpdatedAt());
    }
}
