<?php

namespace Mia\PHPUnit\Tests\Proxy\Data;

use Traversable;

/**
 * Class TestTree
 * @package Mia\PHPUnit\Tests\Proxy\Data
 */
class TestTree extends TestC implements \IteratorAggregate, \Countable
{
    /**
     * @var TestNode
     */
    protected $root;

    /**
     * TestTree constructor.
     * @param TestNode|null $node
     * @param int $count
     */
    public function __construct(TestNode $node = null, $count = 12)
    {
        parent::__construct();

        $this->root = $this->build($node, $count);
    }

    /**
     * @return TestNode
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * @param TestNode|null $node
     * @param int $count
     * @return TestNode
     */
    protected function build(TestNode $node = null, $count = 12)
    {
        if (!$node) {
            $node = new TestNode();
        }

        while ($count-- > 0) {

            $node->addChild(
                $this->build(new TestNode(), mt_rand(0, $count))
            );
        }

        return $node;
    }

    /**
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     * @since 5.0.0
     */
    public function getIterator()
    {
        return new \RecursiveIteratorIterator(new TestNodeIterator($this->root), \RecursiveIteratorIterator::SELF_FIRST);
    }

    /**
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     * @since 5.1.0
     */
    public function count()
    {
        $count = 0;

        $iterator = $this->getIterator();

        while ($iterator->valid()) {
            $iterator->next();
            $count++;
        }

        return $count;
    }
}
