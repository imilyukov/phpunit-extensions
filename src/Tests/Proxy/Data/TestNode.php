<?php

namespace Mia\PHPUnit\Tests\Proxy\Data;

/**
 * Class TestNode
 * @package Mia\PHPUnit\Tests\Proxy\Data
 */
class TestNode extends TestC implements \IteratorAggregate
{
    /**
     * @var TestNode
     */
    protected $parent;

    /**
     * @var TestNode[]
     */
    protected $children = [];

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->children);
    }

    /**
     * @return TestNode
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param TestNode|null $parent
     * @return $this
     */
    public function setParent(TestNode $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @param TestNode $node
     * @return $this
     */
    public function addChild(TestNode $node)
    {
        $this->children[] = $node->setParent($this);

        return $this;
    }

    /**
     * @return TestNode[]
     */
    public function getChildren()
    {
        return $this->children;
    }
}
