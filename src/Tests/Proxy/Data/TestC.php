<?php

namespace Mia\PHPUnit\Tests\Proxy\Data;

/**
 * Class TestC
 * @package Mia\PHPUnit\Tests\Proxy\Data
 */
class TestC extends TestB
{
    /**
     * @var string
     */
    protected $hash;

    /**
     * TestC constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->hash = spl_object_hash($this);
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param $hash
     * @return $this
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }
}
