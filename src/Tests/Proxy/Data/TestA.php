<?php

namespace Mia\PHPUnit\Tests\Proxy\Data;

/**
 * Class TestA
 * @package Mia\PHPUnit\Tests\Proxy\Data
 */
class TestA
{
    /**
     * @var string
     */
    protected $uuid;

    /**
     * @var string
     */
    protected $name;

    /**
     * TestA constructor.
     */
    public function __construct()
    {
        $this->uuid = uniqid();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->uuid;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}
