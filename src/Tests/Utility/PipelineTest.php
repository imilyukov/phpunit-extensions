<?php

namespace Mia\PHPUnit\Tests\Utility;

use Mia\PHPUnit\Mock\Builder\MockBuilderInterface;
use Mia\PHPUnit\Mock\Definition\MockDefinitionRegistryInterface;
use Mia\PHPUnit\Mock\Definition\MockDefinitionRegistryTrait;
use Mia\PHPUnit\Mock\Method\MethodStubFactoryTrait;
use Mia\PHPUnit\Mock\MockBuilderFactoryTrait;
use Mia\PHPUnit\Mock\MockFactoryAwareInterface;
use Mia\PHPUnit\Mock\MockFactoryAwareTrait;
use Mia\PHPUnit\Mock\Property\ContextInterface;
use Mia\PHPUnit\Mock\Property\PropertyValueFactoryTrait;
use Mia\PHPUnit\Utility\Iterator\GeneratorIterator;
use Mia\PHPUnit\Utility\Pipeline\PipelineBuilderInterface;
use Mia\PHPUnit\Utility\Pipeline\PipelineInterface;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Mia\PHPUnit\Utility\Pipeline\Pipeline;
use Mia\PHPUnit\Utility\Pipeline\PipelineBuilder;
use Mia\PHPUnit\Utility\Pipeline\Stage;

/**
 * Class PipelineTest
 * @package Mia\PHPUnit\Tests\Utility
 */
class PipelineTest extends \PHPUnit_Framework_TestCase implements MockFactoryAwareInterface, MockDefinitionRegistryInterface
{
    use MockBuilderFactoryTrait, MockFactoryAwareTrait, PropertyValueFactoryTrait, MethodStubFactoryTrait, MockDefinitionRegistryTrait;

    public function testPipeline()
    {
        $builder = new PipelineBuilder();
        $builder
            ->add(
                function ($a, $b) {
                    return $a + $b;
                },
                function ($a, $b) {
                    return $a * $b;
                }
            )
            ->add(
                function ($a, $b) {
                    return $a - $b;
                }
            );

        $pipeline = (new Pipeline())
            ->pipe(
                new Stage([
                    function ($a, $b) {
                        return $a + $b;
                    },
                    function ($a, $b) {
                        return $a * $b;
                    }
                ])
            )
            ->pipe(
                new Stage([
                    function ($a, $b) {
                        return $a - $b;
                    }
                ])
            );

        $this->assertEquals(-23, $pipeline->process([7, 5]));
        $this->assertEquals(-23, $builder->build()->process([7, 5]));
    }

    public function testOtherMockObjects()
    {
        /** @var ContainerInterface|\PHPUnit_Framework_MockObject_MockObject $container */
        $container = $this->getMockBuilder(ContainerInterface::class)
            ->setMethods(get_class_methods(ContainerInterface::class))
            ->getMock();

        $container->messages = [];

        $container
            ->method('has')
            ->with(
                $this->isType('string')
            )
            ->will(
                $this->returnCallback(
                    function ($name) {
                        return in_array($name, ['container', 'logger']);
                    }
                )
            );

        $container
            ->method('get')
            ->with(
                $this->equalTo('logger')
            )
            ->will(
                $this->returnCallback(
                    function () use ($container) {

                        $logger = $this->getMockBuilder(LoggerInterface::class)
                            ->setMethods(get_class_methods(LoggerInterface::class))
                            ->getMock();

                        $logger
                            ->method('info')
                            ->with(
                                $this->isType('string')
                            )
                            ->will(
                                $this->returnCallback(
                                    function ($message) use ($container) {
                                        $container->messages[] = $message;
                                    }
                                )
                            );

                        return $logger;
                    }
                )
            );

        $logger = $container->get('logger');
        $logger->info('Hello, world #1');
        $logger->info('Hello, world #2');

        $this->assertEquals('Hello, world #1', array_shift($container->messages));
        $this->assertEquals('Hello, world #2', array_shift($container->messages));
    }

    public function testPipelineOtherMockObjects()
    {
        $builder = new PipelineBuilder();
        $builder
            ->add(
                function () {

                    /** @var ContainerInterface|\PHPUnit_Framework_MockObject_MockObject $container */
                    $container = $this->getMockBuilder(ContainerInterface::class)
                        ->setMethods(get_class_methods(ContainerInterface::class))
                        ->getMock();

                    return $container;
                }
            )
            ->add(
                function (ContainerInterface $container) {

                    $container->messages = [];

                    return $container;
                },
                function (ContainerInterface $container) {

                    return $this->returnCallback(
                        function ($message) use ($container) {
                            $container->messages[] = $message;
                        }
                    );
                }
            )
            ->add(
                function (ContainerInterface $container) {

                    $container
                        ->method('has')
                        ->with(
                            $this->isType('string')
                        )
                        ->will(
                            $this->returnCallback(
                                function ($name) {
                                    return in_array($name, ['container', 'logger']);
                                }
                            )
                        );

                    return $container;
                },
                function (ContainerInterface $container, \PHPUnit_Framework_MockObject_Stub $stub) {

                    return $this->returnCallback(
                        function () use ($container, $stub) {

                            $logger = $this->getMockBuilder(LoggerInterface::class)
                                ->setMethods(get_class_methods(LoggerInterface::class))
                                ->getMock();

                            $logger
                                ->method('info')
                                ->with(
                                    $this->isType('string')
                                )
                                ->will($stub);

                            return $logger;
                        }
                    );
                }
            )
            ->add(
                function (ContainerInterface $container, \PHPUnit_Framework_MockObject_Stub $stub) {

                    $container
                        ->method('get')
                        ->with(
                            $this->equalTo('logger')
                        )
                        ->will($stub);

                    return $container;
                }
            );

        $container = $builder->build()->process();

        $logger = $container->get('logger');
        $logger->info('Hello, world #1');
        $logger->info('Hello, world #2');

        $this->assertEquals('Hello, world #1', array_shift($container->messages));
        $this->assertEquals('Hello, world #2', array_shift($container->messages));
    }

    public function testWithMockFactory()
    {
        $containerBuilder = new PipelineBuilder();
        $containerBuilder
            ->add(
                function (PipelineInterface ...$pipelines) {

                    $container = $this->getMockFactory()->createContainerMock(
                        function (MockBuilderInterface $builder) use ($pipelines) {

                            foreach ($pipelines as $pipeline) {

                                $pipeline->process([$builder]);
                            }
                        }
                    );

                    return $container;
                }
            )
        ;

        $loggerBuilder = new PipelineBuilder();
        $loggerBuilder
            ->add(
                function () {

                    $logger = $this->getMockFactory()->createLoggerMock();

                    return $logger;
                }
            )
        ;

        $testerBuilder = new PipelineBuilder();
        $testerBuilder
            ->add(
                function (ContainerInterface $container) {

                    $tester = $this->getMockFactory()->createMock(
                        \stdClass::class,
                        function (MockBuilderInterface $builder) use ($container) {

                            $builder
                                ->property('items')
                                ->will(
                                    $this->generatorValue(
                                        function () {
                                            return mt_rand(0, PHP_INT_MAX);
                                        }, 1000
                                    )
                                )
                            ;

                            $builder
                                ->method('test')
                                ->will(
                                    $this->returnPropertyContext(
                                        function (ContextInterface $context) use ($container) {

                                            $container->get('logger')->info('Run test...');

                                            return $context->getMockObject();
                                        }
                                    )
                                )
                            ;
                        }
                    );

                    return $tester;
                }
            )
        ;

        $registeredBuilders = [
            'logger' => $loggerBuilder,
            'tester' => $testerBuilder,
        ];

        $compilerBuilder = new PipelineBuilder();
        $compilerBuilder
            ->add(
                function (MockBuilderInterface $builder) use ($registeredBuilders) {

                    /**
                     * @var string $name
                     * @var PipelineBuilderInterface $registeredBuilder
                     */
                    foreach ($registeredBuilders as $name => $registeredBuilder) {

                        $builder
                            ->property($name)
                            ->will(
                                $this->callableValue(
                                    function (ContextInterface $context) use ($registeredBuilder) {

                                        $container = $context->getMockObject();

                                        return $registeredBuilder->build()->process([$container]);
                                    }
                                )
                            )
                        ;
                    }
                }
            )
        ;

        $container = $containerBuilder->build()->process([$compilerBuilder->build()]);

        $this->assertInstanceOf(ContainerInterface::class, $container);
        $this->assertInstanceOf(ContainerInterface::class, $container->get('container'));

        $logger = $container->get('logger');

        $this->assertInstanceOf(LoggerInterface::class, $logger);
        $this->assertInstanceOf(\Iterator::class,       $logger->messages);

        $logger->info('Hello, world!');
        $logger->warning('We are dangerous!');

        $this->assertCount(2, $logger->messages);
        $this->assertCount(1, $logger->messages['info']);
        $this->assertCount(1, $logger->messages['warning']);

        $tester = $container->get('tester');

        $this->assertInstanceOf(\stdClass::class, $tester);
        $this->assertEquals($tester, $tester->test());

        $this->assertInstanceOf(GeneratorIterator::class, $tester->items);

        foreach ($tester->items as $item) {
            $this->assertInternalType('integer', $item);
        }

        $this->assertCount(1000, $tester->items);
    }
}
