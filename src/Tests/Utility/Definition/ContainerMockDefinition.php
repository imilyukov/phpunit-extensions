<?php

namespace Mia\PHPUnit\Tests\Utility\Definition;

use Mia\PHPUnit\Mock\Builder\MockBuilderInterface;
use Mia\PHPUnit\Mock\Definition\AbstractMockDefinition;
use Mia\PHPUnit\Mock\Method\MethodStubFactoryTrait;
use Mia\PHPUnit\Mock\Property\ContextInterface;
use Mia\PHPUnit\Mock\Property\PropertyValueFactoryTrait;
use Psr\Container\ContainerInterface;

/**
 * Class ContainerMockDefinition
 * @package Mia\PHPUnit\Tests\Utility\Definition
 */
class ContainerMockDefinition extends AbstractMockDefinition
{
    use PropertyValueFactoryTrait, MethodStubFactoryTrait;

    const NAME = 'container';

    const TYPE = ContainerInterface::class;

    /**
     * @param MockBuilderInterface $builder
     */
    public function define(MockBuilderInterface $builder)
    {
        $builder
            ->property('container')
            ->with(
                new \PHPUnit_Framework_Constraint_IsInstanceOf(ContainerInterface::class)
            )
            ->will(
                $this->callableValue(
                    function (ContextInterface $context) {

                        return $context->getMockObject();
                    }
                )
            )
        ;

        $builder
            ->method('has')
            ->with(
                new \PHPUnit_Framework_Constraint_IsType(\PHPUnit_Framework_Constraint_IsType::TYPE_STRING)
            )
            ->will(
                $this->returnPropertyContext(
                    function (ContextInterface $context, $name) {

                        return $context->hasProperty($name);
                    }
                )
            )
        ;

        $builder
            ->method('get')
            ->with(
                new \PHPUnit_Framework_Constraint_IsType(\PHPUnit_Framework_Constraint_IsType::TYPE_STRING)
            )
            ->will(
                $this->returnPropertyContext(
                    function (ContextInterface $context, $name) {

                        /** @var ContainerInterface $container */
                        $container = $context->getMockObject();

                        if (!$container->has($name)) {

                            return new \PHPUnit_Framework_MockObject_Stub_Exception(
                                new \RuntimeException(
                                    sprintf("Service with name \"%s\" doesn't exist", $name)
                                )
                            );
                        }

                        return $context->getPropertyValue($name);
                    }
                )
            )
        ;
    }
}
