<?php

namespace Mia\PHPUnit\Tests\Utility\Definition;

use Mia\PHPUnit\Mock\Builder\MockBuilderInterface;
use Mia\PHPUnit\Mock\Definition\AbstractMockDefinition;
use Mia\PHPUnit\Mock\Method\MethodStubFactoryTrait;
use Mia\PHPUnit\Mock\Property\ContextInterface;
use Mia\PHPUnit\Mock\Property\PropertyValueFactoryTrait;
use Psr\Log\AbstractLogger;

/**
 * Class ContainerMockDefinition
 * @package Mia\PHPUnit\Tests\Utility\Definition
 */
class LoggerMockDefinition extends AbstractMockDefinition
{
    use PropertyValueFactoryTrait, MethodStubFactoryTrait;

    const NAME = 'logger';

    const TYPE = AbstractLogger::class;

    /**
     * @param MockBuilderInterface $builder
     */
    public function define(MockBuilderInterface $builder)
    {
        $builder
            ->property('messages')
            ->will(
                $this->callableValue(
                    function () {
                        return new \ArrayIterator();
                    }
                )
            )
        ;

        $builder
            ->method('log')
            ->with(
                new \PHPUnit_Framework_Constraint_IsType(\PHPUnit_Framework_Constraint_IsType::TYPE_STRING),
                new \PHPUnit_Framework_Constraint_IsType(\PHPUnit_Framework_Constraint_IsType::TYPE_STRING)
            )
            ->will(
                $this->returnPropertyContext(
                    function (ContextInterface $context, $level, $message) {

                        $messages = $context->getPropertyValue('messages');
                        $messages[$level][] = $message;
                    }
                )
            )
        ;
    }
}
