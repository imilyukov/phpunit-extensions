<?php

namespace Mia\PHPUnit\Tests\Constraint\Definition;

use Mia\PHPUnit\Constraint\Tree\Definition\NodeDefinition;

/**
 * Class StringNodeDefinition
 * @package Mia\PHPUnit\Tests\Constraint\Definition
 */
class StringNodeDefinition extends NodeDefinition
{
    const DEFINITION_TYPE = 'string';

    /**
     * NodeDefinition constructor.
     * @param $name
     */
    public function __construct($name)
    {
        parent::__construct($name);

        $this
            ->constraints()
                ->assertIsType(\PHPUnit_Framework_Constraint_IsType::TYPE_STRING)->end()
            ->end()
        ;
    }
}
