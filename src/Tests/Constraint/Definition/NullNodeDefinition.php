<?php

namespace Mia\PHPUnit\Tests\Constraint\Definition;

use Mia\PHPUnit\Constraint\Tree\Definition\NodeDefinition;

/**
 * Class NullableNodeDefinition
 * @package Mia\PHPUnit\Tests\Constraint\Definition
 */
class NullNodeDefinition extends NodeDefinition
{
    const DEFINITION_TYPE = 'null';

    /**
     * NodeDefinition constructor.
     * @param $name
     */
    public function __construct($name)
    {
        parent::__construct($name);

        $this
            ->constraints()->clear()
                ->assertIsType(\PHPUnit_Framework_Constraint_IsType::TYPE_NULL)->end()
            ->end()
        ;
    }
}
