<?php

namespace Mia\PHPUnit\Tests\Constraint\Definition;

use Mia\PHPUnit\Constraint\Tree\Definition\NodeDefinition;

/**
 * Class CallableNodeDefinition
 * @package Mia\PHPUnit\Tests\Constraint\Definition
 */
class CallableNodeDefinition extends NodeDefinition
{
    const DEFINITION_TYPE = 'callable';

    /**
     * NodeDefinition constructor.
     * @param $name
     */
    public function __construct($name)
    {
        parent::__construct($name);

        $this
            ->constraints()
                ->assertIsType(\PHPUnit_Framework_Constraint_IsType::TYPE_CALLABLE)->end()
            ->end()
        ;
    }
}
