<?php

namespace Mia\PHPUnit\Tests\Constraint;

use Mia\PHPUnit\Assert\ConstraintAssertTrait;
use Mia\PHPUnit\Constraint\Definition\ConstraintDefinitionOwnerInterface;
use Mia\PHPUnit\Constraint\Tree\Builder\TreeBuilder;
use Mia\PHPUnit\Constraint\Tree\Definition\ArrayNodeDefinition;
use Mia\PHPUnit\Constraint\Tree\Definition\NodeDefinitionFactory;
use Mia\PHPUnit\Mock\Builder\MockBuilderInterface;
use Mia\PHPUnit\Mock\Definition\MockDefinitionRegistryInterface;
use Mia\PHPUnit\Mock\Definition\MockDefinitionRegistryTrait;
use Mia\PHPUnit\Mock\Method\MethodStubFactoryTrait;
use Mia\PHPUnit\Mock\MockBuilderFactoryTrait;
use Mia\PHPUnit\Mock\MockFactoryAwareInterface;
use Mia\PHPUnit\Mock\MockFactoryAwareTrait;
use Mia\PHPUnit\Mock\Property\ContextInterface;
use Mia\PHPUnit\Mock\Property\PropertyValueFactoryTrait;
use Mia\PHPUnit\Proxy\ProxyFactoryAwareInterface;
use Mia\PHPUnit\Proxy\ProxyFactoryAwareTrait;

/**
 * Class TreeConstraint
 * @package Mia\PHPUnit\Tests\Constraint
 */
class TreeConstraintTest extends \PHPUnit_Framework_TestCase
    implements MockFactoryAwareInterface, ProxyFactoryAwareInterface, MockDefinitionRegistryInterface
{
    use ConstraintAssertTrait,
        MockBuilderFactoryTrait,
        MockFactoryAwareTrait,
        ProxyFactoryAwareTrait,
        PropertyValueFactoryTrait,
        MethodStubFactoryTrait,
        MockDefinitionRegistryTrait;

    public function testWithTreeConstraint()
    {
        $dataGraph = [
            [
                'name' => 'Some name #1',
                'uuid' => uniqid('#1_'),
                'items' => [
                    [
                        'name' => 'Some name #11',
                        'uuid' => uniqid('#11_'),
                    ],
                    [
                        'name' => 'Some name #12',
                        'uuid' => uniqid('#12_'),
                    ]
                ]
            ],
            [
                'name' => 'Some name #2',
                'uuid' => uniqid('#2_'),
                'items' => [
                    [
                        'name' => 'Some name #21',
                        'uuid' => uniqid('#21_'),
                    ],
                    [
                        'name' => 'Some name #22',
                        'uuid' => uniqid('#22_'),
                    ]
                ]
            ],
        ];

        $treeBuilder = new TreeBuilder();
        $treeBuilder
            ->root(ArrayNodeDefinition::DEFINITION_TYPE)
                ->prototype('array')
                    ->children()
                        ->scalarNode('name')->end()
                        ->scalarNode('uuid')->end()
                        ->arrayNode('items')
                            ->prototype('array')
                                ->children()
                                    ->scalarNode('name')->end()
                                    ->scalarNode('uuid')->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        $node = $treeBuilder->find('.[].items[].uuid');
        $node
            ->constraints()
                ->assertIsType('string')->end()
            ->end()
        ;

        $node = $treeBuilder->find('.[].name');
        $node
            ->constraints()
                ->assertIsType('string')->end()
            ->end()
        ;

        $this->assertSameDefinitionTree($dataGraph, $treeBuilder);
        $this->assertSameDefinitionTree(
            $dataGraph,
            function (TreeBuilder $builder) {
                // Configure builder
                $builder
                    ->root(ArrayNodeDefinition::DEFINITION_TYPE)
                        ->prototype('array')
                            ->children()
                                ->scalarNode('name')->end()
                                ->scalarNode('uuid')->end()
                                ->arrayNode('items')
                                    ->prototype('array')
                                        ->children()
                                            ->scalarNode('name')->end()
                                            ->scalarNode('uuid')->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ;
            }
        );
    }

    public function testRegisterNewNodeDefinition()
    {
        NodeDefinitionFactory::addType(
            Definition\StringNodeDefinition::class,
            Definition\StringNodeDefinition::DEFINITION_TYPE
        );
        NodeDefinitionFactory::addType(
            Definition\NullNodeDefinition::class,
            Definition\NullNodeDefinition::DEFINITION_TYPE
        );
        NodeDefinitionFactory::addType(
            Definition\CallableNodeDefinition::class,
            Definition\CallableNodeDefinition::DEFINITION_TYPE
        );

        $dataGraph = [
            [
                'name' => 'Some name #1',
                'uuid' => uniqid('#1_'),
                'call' => function () {},
                'data' => null
            ]
        ];

        $this->assertSameDefinitionTree(
            $dataGraph,
            function (TreeBuilder $builder) {
                // Configure builder
                $builder
                    ->root(ArrayNodeDefinition::DEFINITION_TYPE)
                        ->prototype('array')
                            ->children()
                                ->stringNode('name')->end()
                                ->stringNode('uuid')->end()
                                ->callableNode('call')->end()
                                ->nullNode('data')->end()
                            ->end()
                        ->end()
                    ->end()
                ;
            }
        );
    }

    public function testWithAggregateConstraint()
    {
        $this->assertSameDefinition(
            [
                'name' => 'Some name'
            ],
            function (ConstraintDefinitionOwnerInterface $owner) {

                $owner
                    ->constraints()
                        ->assertIsType('array')->end()
                        ->assertCount(1)->end()
                        ->assertAnd()->end()
                        ->assertOr()
                            ->constraints()
                                ->assertIsType('array')->end()
                                ->assertIsType('object')->end()
                            ->end()
                        ->end()
                        ->assertNot()
                            ->constraints()
                                ->assertIsType('scalar')->end()
                            ->end()
                        ->end()
                    ->end()
                ;
            }
        );
    }

    public function testWithTreeAndAggregateConstraints()
    {
        $mock = $this->getMockFactory()->createMock(
            \stdClass::class,
            function (MockBuilderInterface $builder) {

                $constraint = $this->aggregateConstraint();
                $constraint
                    ->constraints()
                        ->assertOr()
                            ->constraints()
                                ->assertIsType('array')->end()
                                ->assertIsType('object')->end()
                            ->end()
                        ->end()
                    ->end()
                ;

                $builder
                    ->property('complexValue')
                    ->with(
                        $constraint
                    )
                    ->will(
                        $this->mixedValue(null)
                    )
                ;

                $builder
                    ->method('setComplexValue')
                    ->with(
                        $constraint
                    )
                    ->will(
                        $this->returnPropertyContext(
                            function (ContextInterface $context, $value) {

                                $mock = $context->getMockObject();
                                $mock->complexValue = $value;

                                return $mock;
                            }
                        )
                    )
                ;

                $builder
                    ->method('getComplexValue')
                    ->will(
                        $this->returnPropertyContext(
                            function (ContextInterface $context) {

                                $mock = $context->getMockObject();

                                return $mock->complexValue;
                            }
                        )
                    )
                ;
            }
        );

        $mock->setComplexValue([]);
        $this->assertInternalType('array', $mock->getComplexValue());

        $mock->setComplexValue((object)[]);
        $this->assertInternalType('object', $mock->getComplexValue());
    }
}
