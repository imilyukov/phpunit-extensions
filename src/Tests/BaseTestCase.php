<?php

namespace Mia\PHPUnit\Tests;

use Mia\PHPUnit\Assert\ConstraintAssertTrait;
use Mia\PHPUnit\Mock\Definition\MockDefinitionRegistryInterface;
use Mia\PHPUnit\Mock\Definition\MockDefinitionRegistryTrait;
use Mia\PHPUnit\Mock\Method\MethodStubFactoryTrait;
use Mia\PHPUnit\Mock\MockBuilderFactoryTrait;
use Mia\PHPUnit\Mock\MockFactoryAwareInterface;
use Mia\PHPUnit\Mock\MockFactoryAwareTrait;
use Mia\PHPUnit\Mock\Property\PropertyValueFactoryTrait;
use Mia\PHPUnit\Proxy\ProxyFactoryAwareInterface;
use Mia\PHPUnit\Proxy\ProxyFactoryAwareTrait;

/**
 * Class BaseTestCase
 * @package Mia\PHPUnit\Tests
 */
abstract class BaseTestCase extends \PHPUnit_Framework_TestCase
    implements MockFactoryAwareInterface, ProxyFactoryAwareInterface, MockDefinitionRegistryInterface
{
    use ConstraintAssertTrait,
        MockBuilderFactoryTrait,
        MockFactoryAwareTrait,
        ProxyFactoryAwareTrait,
        PropertyValueFactoryTrait,
        MethodStubFactoryTrait,
        MockDefinitionRegistryTrait;
}
