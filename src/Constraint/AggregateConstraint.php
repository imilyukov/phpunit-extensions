<?php

namespace Mia\PHPUnit\Constraint;

use Mia\PHPUnit\Constraint\Definition\ConstraintDefinitionOwnerInterface;
use Mia\PHPUnit\Constraint\Definition\ConstraintDefinitionOwnerTrait;

/**
 * Class AggregateConstraint
 * @package Mia\PHPUnit\Constraint
 */
class AggregateConstraint extends \PHPUnit_Framework_Constraint implements ConstraintDefinitionOwnerInterface
{
    use ConstraintDefinitionOwnerTrait;

    /**
     * @var Factory\ConstraintFactoryInterface
     */
    protected $factory;

    /**
     * @var string[]
     */
    protected $failureDescriptions = [];

    /**
     * AggregateConstraint constructor.
     * @param Factory\ConstraintFactoryInterface|null $factory
     */
    public function __construct(Factory\ConstraintFactoryInterface $factory = null)
    {
        parent::__construct();

        $this->factory = $factory ?: new Factory\ConstraintFactory();
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->getConstraints());
    }

    /**
     * Evaluates the constraint for parameter $other. Returns true if the
     * constraint is met, false otherwise.
     *
     * @param  mixed $other Value or object to evaluate.
     * @return bool
     */
    protected function matches($other)
    {
        $matches = [];

        $constraints = $this->getConstraints();

        foreach ($constraints as $constraint) {

            try {
                $this->factory
                    ->createConstraint(
                        $constraint->getType(),
                        $constraint->getParameters(),
                        $constraint->getInitializer()
                    )
                    ->evaluate(
                        $other,
                        $constraint->getMessage()
                    )
                ;
            } catch (\PHPUnit_Framework_ExpectationFailedException $ex) {

                $this->failureDescriptions[] = $ex->getMessage();

                $matches[] = false;
            }
        }

        return !in_array(false, $matches, true);
    }

    /**
     * Return additional failure description where needed
     *
     * The function can be overridden to provide additional failure
     * information like a diff
     *
     * @param  mixed  $other Evaluated value or object.
     * @return string
     */
    protected function additionalFailureDescription($other)
    {
        return implode(PHP_EOL, $this->failureDescriptions);
    }

    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return '';
    }
}
