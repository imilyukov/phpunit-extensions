<?php

namespace Mia\PHPUnit\Constraint\Factory;

/**
 * Interface ConstraintFactoryInterface
 * @package Mia\PHPUnit\Constraint\Factory
 */
interface ConstraintFactoryInterface
{
    /**
     * @param $type
     * @param array $parameters
     * @param \Closure $initializer
     * @return mixed
     */
    public function createConstraint($type, array $parameters = [], \Closure $initializer = null);
}
