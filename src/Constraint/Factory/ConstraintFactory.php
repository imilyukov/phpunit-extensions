<?php

namespace Mia\PHPUnit\Constraint\Factory;

/**
 * Class ConstraintFactory
 * @package Mia\PHPUnit\Constraint\Factory
 */
class ConstraintFactory implements ConstraintFactoryInterface
{
    /**
     * @param $type
     * @param array $parameters
     * @param \Closure $initializer
     * @return \PHPUnit_Framework_Constraint
     */
    public function createConstraint($type, array $parameters = [], \Closure $initializer = null)
    {
        $className = sprintf('%s_%s', \PHPUnit_Framework_Constraint::class, ucfirst($type));
        if (!class_exists($className)) {
            throw new \RuntimeException(
                sprintf(
                    "Constraint class %s doesn't exist in namespace %s. " .
                    "Try add constraint with method %s::addConstraint",
                    $className,
                    \PHPUnit_Framework_Constraint::class,
                    __CLASS__
                )
            );
        }

        $reflection = new \ReflectionClass($className);
        /** @var \PHPUnit_Framework_Constraint $constraint */
        $constraint = $reflection->newInstanceWithoutConstructor();
        if ($parameters) {
            $reflection->getConstructor()->invokeArgs($constraint, $parameters);
        }

        if ($initializer) {
            $initializer->__invoke($constraint, $this);
        }

        return $constraint;
    }
}
