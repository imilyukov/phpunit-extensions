<?php

namespace Mia\PHPUnit\Constraint;

use Mia\PHPUnit\Comparator\PriorityAwareInterface;
use Mia\PHPUnit\Mock\Property\ContextAwareInterface;
use Mia\PHPUnit\Mock\Property\ContextInterface;

/**
 * Class PropertyContextConstraint
 * @package Mia\PHPUnit\Constraint
 */
class PropertyContextConstraint extends \PHPUnit_Framework_Constraint implements ContextAwareInterface, PriorityAwareInterface
{
    /**
     * @var callable
     */
    protected $callback;

    /**
     * @var ContextInterface|null
     */
    protected $context;

    /**
     * ReturnPropertyContext constructor.
     * @param callable $callback
     * @param ContextInterface $context
     */
    public function __construct(callable $callback, ContextInterface $context = null)
    {
        parent::__construct();

        $this->callback = $callback;
        $this->context  = $context;
    }

    /**
     * @param ContextInterface|null $context
     * @return $this
     */
    public function setContext(ContextInterface $context = null)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Evaluates the constraint for parameter $value. Returns true if the
     * constraint is met, false otherwise.
     *
     * @param mixed $other Value or object to evaluate.
     *
     * @return bool
     */
    protected function matches($other)
    {
        return call_user_func_array($this->callback, [$this->context, $other]);
    }

    /**
     * Returns a string representation of the constraint.
     *
     * @return string
     */
    public function toString()
    {
        return '';
    }

    /**
     * @return integer
     */
    public function getPriority()
    {
        return -100;
    }
}
