<?php

namespace Mia\PHPUnit\Constraint;

use Mia\PHPUnit\Constraint\Tree\NodeInterface;
use Mia\PHPUnit\Constraint\Tree\NodeIterator;
use Mia\PHPUnit\Utility\Property\PropertyAccessor;

/**
 * Class TreeConstraint
 * @package Mia\PHPUnit\Constraint
 */
class TreeConstraint extends \PHPUnit_Framework_Constraint implements \IteratorAggregate
{
    /**
     * @var NodeInterface
     */
    protected $node;

    /**
     * @var PropertyAccessor
     */
    protected $propertyAccessor;

    /**
     * @var string[]
     */
    protected $failureDescriptions = [];

    /**
     * TreeConstraint constructor.
     * @param NodeInterface $node
     */
    public function __construct(NodeInterface $node)
    {
        parent::__construct();

        $this->node = $node;
    }

    /**
     * @return int
     */
    public function count()
    {
        $count = 0;

        $iterator = $this->getIterator();

        /** @var NodeInterface $node */
        foreach ($iterator as $node) {

            foreach ($node->getConstraints() as $constraint) {

                $count += count($constraint);
            }
        }

        return $count;
    }

    /**
     * Evaluates the constraint for parameter $other. Returns true if the
     * constraint is met, false otherwise.
     *
     * @param  mixed $other Value or object to evaluate.
     * @return bool
     */
    protected function matches($other)
    {
        $matches = [];

        $iterator = $this->getIterator();

        /** @var NodeInterface $node */
        foreach ($iterator as $node) {

            $path = $node->getPath();

            try {
                $part = $this->getPropertyAccessor()->getValue($other, $path);
                $part = $part instanceof \Traversable ? $part : [$part];

                foreach ($part as $partPath => $value) {

                    $constraints = $node->getConstraints();

                    foreach ($constraints as $constraint) {

                        try {
                            $message = sprintf(
                                'For that ' . $this->exporter->export($value) . ' in path %s:' . PHP_EOL, $partPath
                            );

                            $constraint->evaluate($value, $message);
                        } catch (\PHPUnit_Framework_ExpectationFailedException $ex) {

                            $this->failureDescriptions[] = $ex->getMessage();

                            $matches[] = false;
                        }
                    }
                }

            } catch (\UnexpectedValueException $ex) {

                $this->failureDescriptions[] = $ex->getMessage();

                $matches[] = false;
            }
        }

        return !in_array(false, $matches, true);
    }

    /**
     * @return \RecursiveIteratorIterator
     */
    public function getIterator()
    {
        return new \RecursiveIteratorIterator(new NodeIterator($this->node), \RecursiveIteratorIterator::SELF_FIRST);
    }

    /**
     * Return additional failure description where needed
     *
     * The function can be overridden to provide additional failure
     * information like a diff
     *
     * @param  mixed  $other Evaluated value or object.
     * @return string
     */
    protected function additionalFailureDescription($other)
    {
        return implode(PHP_EOL, $this->failureDescriptions);
    }

    /**
     * @return PropertyAccessor
     */
    protected function getPropertyAccessor()
    {
        if (!$this->propertyAccessor) {
            $this->propertyAccessor = new PropertyAccessor();
        }

        return $this->propertyAccessor;
    }

    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'with next failures: ' . PHP_EOL;
    }
}
