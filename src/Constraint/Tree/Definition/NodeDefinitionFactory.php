<?php

namespace Mia\PHPUnit\Constraint\Tree\Definition;

/**
 * Class NodeDefinitionFactory
 * @package Mia\PHPUnit\Constraint\Tree\Definition
 */
class NodeDefinitionFactory
{
    /**
     * @var array
     */
    protected static $typeMapping = [
        'array'  => ArrayNodeDefinition::class,
        'object' => ObjectNodeDefinition::class,
        'scalar' => ScalarNodeDefinition::class,
    ];

    /**
     * @param $className
     * @param $type
     */
    public static function addType($className, $type)
    {
        $reflectionClass = new \ReflectionClass($className);

        if (!$reflectionClass->implementsInterface(NodeDefinitionInterface::class)) {

            throw new \LogicException(
                sprintf(
                    'Node definition class %s has not implementation of %s',
                    $className,
                    NodeDefinitionInterface::class
                )
            );
        }

        self::$typeMapping[$type] = $className;
    }

    /**
     * @param $type
     * @param array $args
     * @return object
     */
    public function createNodeDefinition($type, array $args = [])
    {
        if (!isset(self::$typeMapping[$type])) {
            throw new \RuntimeException(
                sprintf(
                    "Registered types can be created only. Please, add your custom type with method %s::addType",
                    __CLASS__
                )
            );
        }

        $reflectionClass = new \ReflectionClass(self::$typeMapping[$type]);

        return $reflectionClass->newInstanceArgs($args);
    }
}
