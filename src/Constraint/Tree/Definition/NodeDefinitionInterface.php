<?php

namespace Mia\PHPUnit\Constraint\Tree\Definition;

use Mia\PHPUnit\Constraint\Definition\ConstraintDefinitionOwnerInterface;
use Mia\PHPUnit\Constraint\Tree\Builder\NodeBuilderInterface;

/**
 * Interface NodeDefinitionInterface
 * @package Mia\PHPUnit\Constraint\Tree\Definition
 */
interface NodeDefinitionInterface extends ConstraintDefinitionOwnerInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getType();

    /**
     * @param ParentNodeDefinitionInterface $parent
     * @return mixed
     */
    public function setParent(ParentNodeDefinitionInterface $parent = null);

    /**
     * @return NodeBuilderInterface
     */
    public function end();
}
