<?php

namespace Mia\PHPUnit\Constraint\Tree\Definition;

/**
 * Class ObjectNodeDefinition
 * @package Mia\PHPUnit\Constraint\Tree\Definition
 */
class ObjectNodeDefinition extends NodeDefinition implements ParentNodeDefinitionInterface
{
    use ParentNodeDefinitionTrait;

    const DEFINITION_TYPE = 'object';

    /**
     * NodeDefinition constructor.
     * @param $name
     */
    public function __construct($name)
    {
        parent::__construct($name);

        $this
            ->constraints()
                ->assertIsType(\PHPUnit_Framework_Constraint_IsType::TYPE_OBJECT)->end()
            ->end()
        ;
    }

    /**
     * @return \Iterator|\ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->children);
    }
}
