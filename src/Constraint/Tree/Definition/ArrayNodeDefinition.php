<?php

namespace Mia\PHPUnit\Constraint\Tree\Definition;

/**
 * Class ArrayNodeDefinition
 * @package Mia\PHPUnit\Constraint\Tree\Definition
 */
class ArrayNodeDefinition extends NodeDefinition implements ParentNodeDefinitionInterface, PrototypedDefinitionInterface
{
    use ParentNodeDefinitionTrait;

    const DEFINITION_TYPE = 'array';

    /**
     * NodeDefinition constructor.
     * @param $name
     */
    public function __construct($name)
    {
        parent::__construct($name);

        $this
            ->constraints()
                ->clear()
                ->assertIsType(\PHPUnit_Framework_Constraint_IsType::TYPE_ARRAY)->end()
            ->end()
        ;
    }

    /**
     * Sets a prototype for child nodes.
     *
     * @param string $type the type of node
     *
     * @return NodeDefinitionInterface
     */
    public function prototype($type)
    {
        $this->children = \SplFixedArray::fromArray([$this->getNodeBuilder()->node(null, $type)]);
        $this->children->rewind();

        $prototypeNode = new PrototypeNodeDefinition($this->children->current());
        $prototypeNode->setParent($this);

        return $prototypeNode;
    }

    /**
     * @return \Iterator|\ArrayIterator
     */
    public function getIterator()
    {
        return $this->children instanceof \Iterator ? $this->children : new \ArrayIterator($this->children);
    }
}
