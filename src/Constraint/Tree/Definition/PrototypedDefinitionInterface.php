<?php

namespace Mia\PHPUnit\Constraint\Tree\Definition;

/**
 * Interface PrototypedDefinitionInterface
 * @package Mia\PHPUnit\Constraint\Tree\Definition
 */
interface PrototypedDefinitionInterface
{
    /**
     * @param $type
     * @return mixed
     */
    public function prototype($type);
}
