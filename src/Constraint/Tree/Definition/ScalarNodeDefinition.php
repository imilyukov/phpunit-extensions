<?php

namespace Mia\PHPUnit\Constraint\Tree\Definition;

/**
 * Class ScalarNodeDefinition
 * @package Mia\PHPUnit\Constraint\Tree\Definition
 */
class ScalarNodeDefinition extends NodeDefinition
{
    const DEFINITION_TYPE = 'scalar';

    /**
     * NodeDefinition constructor.
     * @param $name
     */
    public function __construct($name)
    {
        parent::__construct($name);

        $this
            ->constraints()
                ->assertIsType(\PHPUnit_Framework_Constraint_IsType::TYPE_SCALAR)->end()
            ->end()
        ;
    }
}
