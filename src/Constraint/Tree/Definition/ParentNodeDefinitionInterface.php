<?php

namespace Mia\PHPUnit\Constraint\Tree\Definition;

use Mia\PHPUnit\Constraint\Tree\Builder\NodeBuilderAwareInterface;
use Mia\PHPUnit\Constraint\Tree\Builder\NodeBuilderInterface;

/**
 * Interface ParentNodeDefinitionInterface
 * @package Mia\PHPUnit\Constraint\Tree\Definition
 */
interface ParentNodeDefinitionInterface extends NodeDefinitionInterface, NodeBuilderAwareInterface, \IteratorAggregate, \Countable
{
    /**
     * @return NodeBuilderInterface
     */
    public function children();

    /**
     * @param NodeDefinitionInterface $node
     * @return mixed
     */
    public function addChild(NodeDefinitionInterface $node);

    /**
     * @return NodeDefinition[]
     */
    public function getChildren();

    /**
     * @param NodeDefinition[] $children
     * @return mixed
     */
    public function setChildren(array $children);
}
