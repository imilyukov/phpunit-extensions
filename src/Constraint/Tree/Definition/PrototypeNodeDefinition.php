<?php

namespace Mia\PHPUnit\Constraint\Tree\Definition;

use Mia\PHPUnit\Constraint\Definition\ConstraintDefinitionInterface;
use Mia\PHPUnit\Constraint\Definition\Builder\ConstraintBuilderInterface;
use Mia\PHPUnit\Constraint\Tree\Builder\NodeBuilderInterface;

/**
 * Class PrototypeNodeDefinition
 * @package Mia\PHPUnit\Constraint\Tree\Definition
 */
class PrototypeNodeDefinition implements ParentNodeDefinitionInterface
{
    /**
     * @var NodeDefinitionInterface|ParentNodeDefinitionInterface
     */
    protected $node;

    /**
     * @var ParentNodeDefinitionInterface
     */
    protected $parent;

    /**
     * PrototypeNodeDefinition constructor.
     * @param NodeDefinitionInterface $node
     */
    public function __construct(NodeDefinitionInterface $node)
    {
        $this->node = $node;
        $this->node->setParent($this);
    }

    /**
     * @return ConstraintBuilderInterface
     */
    public function getConstraintBuilder()
    {
        return $this->node->getConstraintBuilder();
    }

    /**
     * @param ConstraintBuilderInterface|null $builder
     * @return mixed
     */
    public function setConstraintBuilder(ConstraintBuilderInterface $builder = null)
    {
        $this->node->setConstraintBuilder($builder);

        return $this;
    }

    /**
     * @return ConstraintBuilderInterface
     */
    public function constraints()
    {
        return $this->node->constraints();
    }

    /**
     * @param ConstraintDefinitionInterface $constraint
     * @return mixed
     */
    public function addConstraint(ConstraintDefinitionInterface $constraint)
    {
        $this->node->addConstraint($constraint);

        return $this;
    }

    /**
     * @return ConstraintDefinitionInterface[]
     */
    public function getConstraints()
    {
        return $this->node->getConstraints();
    }

    /**
     * @param array $constraints
     * @return mixed
     */
    public function setConstraints(array $constraints)
    {
        $this->node->setConstraints($constraints);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->node->getName();
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->node->getType();
    }

    /**
     * @param ParentNodeDefinitionInterface $parent
     * @return mixed
     */
    public function setParent(ParentNodeDefinitionInterface $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return mixed
     */
    public function end()
    {
        $this->node->setParent($this->parent);

        return $this->parent;
    }

    /**
     * @return mixed
     */
    public function getNodeBuilder()
    {
        return $this->end();
    }

    /**
     * @return mixed
     */
    public function children()
    {
        if (!$this->node instanceof ParentNodeDefinitionInterface) {

            throw new \BadMethodCallException(
                sprintf(
                    "Prototype node doesn't have node instance of %s for calling of %s",
                    ParentNodeDefinitionInterface::class,
                    __METHOD__
                )
            );
        }

        return $this->node->children();
    }

    /**
     * @param NodeDefinitionInterface $node
     * @return mixed
     */
    public function addChild(NodeDefinitionInterface $node)
    {
        if (!$this->node instanceof ParentNodeDefinitionInterface) {

            throw new \BadMethodCallException(
                sprintf(
                    "Prototype node doesn't have node instance of %s for calling of %s",
                    ParentNodeDefinitionInterface::class,
                    __METHOD__
                )
            );
        }

        $this->node->addChild($node);

        return $this;
    }

    /**
     * @return NodeDefinition[]
     */
    public function getChildren()
    {
        if (!$this->node instanceof ParentNodeDefinitionInterface) {

            throw new \BadMethodCallException(
                sprintf(
                    "Prototype node doesn't have node instance of %s for calling of %s",
                    ParentNodeDefinitionInterface::class,
                    __METHOD__
                )
            );
        }

        return $this->node->getChildren();
    }

    /**
     * @param NodeDefinition[] $children
     * @return mixed
     */
    public function setChildren(array $children)
    {
        if (!$this->node instanceof ParentNodeDefinitionInterface) {

            throw new \BadMethodCallException(
                sprintf(
                    "Prototype node doesn't have node instance of %s for calling of %s",
                    ParentNodeDefinitionInterface::class,
                    __METHOD__
                )
            );
        }

        $this->node->setChildren($children);

        return $this;
    }

    /**
     * @return \Iterator|\Countable
     */
    public function getIterator()
    {
        if (!$this->node instanceof ParentNodeDefinitionInterface) {

            throw new \BadMethodCallException(
                sprintf(
                    "Prototype node doesn't have node instance of %s for calling of %s",
                    ParentNodeDefinitionInterface::class,
                    __METHOD__
                )
            );
        }

        return $this->node->getIterator();
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->getIterator()->count();
    }

    /**
     * @param NodeBuilderInterface|null $builder
     * @return mixed|void
     */
    public function setNodeBuilder(NodeBuilderInterface $builder = null)
    {
        throw new \BadMethodCallException(
            sprintf(
                "Method %s::setNodeBuilder() doesn't support for class %s",
                ParentNodeDefinitionInterface::class,
                __CLASS__
            )
        );
    }

    /**
     * @param NodeDefinitionInterface $node
     * @return mixed|void
     */
    public function append(NodeDefinitionInterface $node)
    {
        if (!$this->node instanceof ParentNodeDefinitionInterface) {

            throw new \BadMethodCallException(
                sprintf(
                    "Prototype node doesn't have node instance of %s for calling of %s",
                    ParentNodeDefinitionInterface::class,
                    __METHOD__
                )
            );
        }

        return $this->node->children()->append($node);
    }
}
