<?php

namespace Mia\PHPUnit\Constraint\Tree\Definition;

use Mia\PHPUnit\Constraint\Tree\Builder\NodeBuilder;
use Mia\PHPUnit\Constraint\Tree\Builder\NodeBuilderInterface;

/**
 * Class ParentNodeDefinitionTrait
 * @package Mia\PHPUnit\Constraint\Tree\Definition
 */
trait ParentNodeDefinitionTrait
{
    /**
     * @var NodeBuilder
     */
    protected $nodeBuilder;

    /**
     * @var NodeDefinitionInterface[]
     */
    protected $children = [];

    /**
     * @return \Iterator|\Countable
     */
    abstract public function getIterator();

    /**
     * @return NodeBuilderInterface
     */
    public function children()
    {
        return $this->getNodeBuilder();
    }

    /**
     * @param NodeDefinitionInterface $node
     * @return mixed
     */
    public function addChild(NodeDefinitionInterface $node)
    {
        $this->children[$node->getName()] = $node->setParent($this);

        return $this;
    }

    /**
     * @return NodeDefinitionInterface[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param array $children
     * @return $this
     */
    public function setChildren(array $children = [])
    {
        $this->children = [];

        foreach ($children as $child) {

            $this->addChild($child);
        }

        return $this;
    }

    /**
     * @return NodeBuilder
     */
    public function getNodeBuilder()
    {
        if (!$this->nodeBuilder) {
            $this->nodeBuilder = new NodeBuilder();
            $this->nodeBuilder->setParent($this);
        }

        return $this->nodeBuilder;
    }

    /**
     * @param NodeBuilderInterface $builder
     * @return mixed
     */
    public function setNodeBuilder(NodeBuilderInterface $builder = null)
    {
        $this->nodeBuilder = $builder;

        return $this;
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->getIterator()->count();
    }

    /**
     * Clone
     */
    public function __clone()
    {
        $this->setNodeBuilder(null);

        $children = $this->getChildren();

        $this->children()->clear();

        foreach ($children as $child) {

            $this->children()->append(clone $child);
        }
    }
}
