<?php

namespace Mia\PHPUnit\Constraint\Tree\Definition;

/**
 * Class ParentNodeDefinitionIterator
 * @package Mia\PHPUnit\Constraint\Tree\Definition
 */
class ParentNodeDefinitionIterator implements \RecursiveIterator
{
    /**
     * @var \ArrayIterator
     */
    protected $iterator;

    /**
     * ParentNodeDefinitionIterator constructor.
     * @param ParentNodeDefinitionInterface $definition
     */
    public function __construct(ParentNodeDefinitionInterface $definition)
    {
        $this->iterator = $definition->getIterator();
    }

    /**
     * {@inheritdoc}
     */
    public function getChildren()
    {
        return new self($this->current());
    }

    /**
     * {@inheritdoc}
     */
    public function hasChildren()
    {
        return $this->current() instanceof ParentNodeDefinitionInterface && $this->current()->count() > 0;
    }

    /**
     * {@inheritdoc}
     */
    public function current()
    {
        return $this->iterator->current();
    }

    /**
     * {@inheritdoc}
     */
    public function next()
    {
        $this->iterator->next();
    }

    /**
     * {@inheritdoc}
     */
    public function key()
    {
        return $this->current()->getName();
    }

    /**
     * {@inheritdoc}
     */
    public function valid()
    {
        return $this->iterator->valid();
    }

    /**
     * {@inheritdoc}
     */
    public function rewind()
    {
        $this->iterator->rewind();
    }
}
