<?php

namespace Mia\PHPUnit\Constraint\Tree\Definition;

use Mia\PHPUnit\Constraint\Definition\ConstraintDefinitionOwnerTrait;
use Mia\PHPUnit\Constraint\Tree\Factory\NodePathFactoryInterface;
use Mia\PHPUnit\Constraint\Tree\Factory\NodePathFactoryTrait;

/**
 * Class NodeDefinition
 * @package Mia\PHPUnit\Constraint\Tree\Definition
 */
abstract class NodeDefinition implements NodeDefinitionInterface, NodePathFactoryInterface
{
    use ConstraintDefinitionOwnerTrait, NodePathFactoryTrait;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var ParentNodeDefinitionInterface
     */
    protected $parent;

    /**
     * NodeDefinition constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;

        $this
            ->constraints()
                ->assertNot()
                    ->constraints()
                        ->assertIsEmpty()->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param ParentNodeDefinitionInterface $parent
     * @return $this
     */
    public function setParent(ParentNodeDefinitionInterface $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return ParentNodeDefinitionInterface
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return mixed
     */
    public function end()
    {
        return $this->parent ? $this->parent->getNodeBuilder() : null;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        if (!defined('static::DEFINITION_TYPE')) {

            throw new \BadMethodCallException(
                "Class %s doesn't has the declaration of const DEFINITION_TYPE",
                static::class
            );
        }

        return static::DEFINITION_TYPE;
    }
}
