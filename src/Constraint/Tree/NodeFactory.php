<?php

namespace Mia\PHPUnit\Constraint\Tree;

use Mia\PHPUnit\Constraint\AggregateConstraint;
use Mia\PHPUnit\Constraint\Definition\ConstraintDefinition;
use Mia\PHPUnit\Constraint\Tree\Definition\NodeDefinitionInterface;
use Mia\PHPUnit\Constraint\Tree\Definition\ParentNodeDefinitionInterface;

/**
 * Class NodeFactory
 * @package Mia\PHPUnit\Constraint\Tree
 */
class NodeFactory implements NodeFactoryInterface
{
    /**
     * @param NodeDefinitionInterface $definition
     * @return NodeInterface
     */
    public function createNode(NodeDefinitionInterface $definition)
    {
        $node = new Node($definition->getName(), $definition->getType());

        $aggregateConstraint = new AggregateConstraint();

        /** @var ConstraintDefinition[] $constraints */
        $constraints = $definition->getConstraints();
        foreach ($constraints as $constraint) {

            $aggregateConstraint->constraints()->append(clone $constraint);
        }

        $node->addConstraint($aggregateConstraint);

        if ($definition instanceof ParentNodeDefinitionInterface) {

            foreach ($definition as $childDefinition) {

                $childNode = $this->createNode($childDefinition);
                $childNode->setParent($node);
            }
        }

        return $node;
    }
}
