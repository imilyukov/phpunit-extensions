<?php

namespace Mia\PHPUnit\Constraint\Tree\Builder;

/**
 * Interface NodeBuilderAwareInterface
 * @package Mia\PHPUnit\Constraint\Tree\Builder
 */
interface NodeBuilderAwareInterface
{
    /**
     * @return NodeBuilderInterface
     */
    public function getNodeBuilder();

    /**
     * @param NodeBuilderInterface $builder
     * @return mixed
     */
    public function setNodeBuilder(NodeBuilderInterface $builder = null);
}
