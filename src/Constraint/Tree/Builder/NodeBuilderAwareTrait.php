<?php

namespace Mia\PHPUnit\Constraint\Tree\Builder;

/**
 * Class NodeBuilderAwareTrait
 * @package Mia\PHPUnit\Constraint\Tree\Builder
 */
trait NodeBuilderAwareTrait
{
    /**
     * @var NodeBuilderInterface|null
     */
    protected $nodeBuilder;

    /**
     * @return NodeBuilderInterface
     */
    public function getNodeBuilder()
    {
        if (!$this->nodeBuilder) {
            $this->nodeBuilder = new NodeBuilder();
        }

        return $this->nodeBuilder;
    }

    /**
     * @param NodeBuilderInterface $builder
     * @return mixed
     */
    public function setNodeBuilder(NodeBuilderInterface $builder = null)
    {
        $this->nodeBuilder = $builder;

        return $this;
    }
}
