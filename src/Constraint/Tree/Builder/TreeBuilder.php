<?php

namespace Mia\PHPUnit\Constraint\Tree\Builder;

use Mia\PHPUnit\Constraint\Tree\Definition\NodeDefinitionInterface;
use Mia\PHPUnit\Constraint\Tree\Definition\ObjectNodeDefinition;
use Mia\PHPUnit\Constraint\Tree\Definition\ParentNodeDefinitionInterface;
use Mia\PHPUnit\Constraint\Tree\Definition\ParentNodeDefinitionIterator;
use Mia\PHPUnit\Constraint\Tree\Definition\PrototypedDefinitionInterface;
use Mia\PHPUnit\Constraint\Tree\Factory\NodePathFactoryInterface;
use Mia\PHPUnit\Constraint\Tree\NodeFactory;
use Mia\PHPUnit\Constraint\Tree\NodeFactoryInterface;
use Mia\PHPUnit\Constraint\Tree\NodeInterface;
use Mia\PHPUnit\Utility\Property\PropertyAccessor;

/**
 * Class TreeBuilder
 * @package Mia\PHPUnit\Constraint\Tree\Builder
 */
class TreeBuilder implements NodeBuilderAwareInterface
{
    use NodeBuilderAwareTrait;

    /**
     * @var NodeFactoryInterface
     */
    protected $factory;

    /**
     * @var NodeDefinitionInterface|ParentNodeDefinitionInterface
     */
    protected $root;

    /**
     * TreeBuilder constructor.
     * @param NodeFactoryInterface|null $factory
     */
    public function __construct(NodeFactoryInterface $factory = null)
    {
        $this->factory = $factory ?: new NodeFactory();
    }

    /**
     * @param string $type
     * @return NodeDefinitionInterface|ParentNodeDefinitionInterface|PrototypedDefinitionInterface
     */
    public function root($type = ObjectNodeDefinition::DEFINITION_TYPE)
    {
        return $this->root = $this->getNodeBuilder()->node(null, $type);
    }

    /**
     * @return NodeDefinitionInterface|ParentNodeDefinitionInterface
     */
    public function getRoot()
    {
        if (!$this->root) {
            $this->root = $this->root();
        }

        return $this->root;
    }

    /**
     * @param string $path
     * @return NodeDefinitionInterface|ParentNodeDefinitionInterface|NodePathFactoryInterface
     */
    public function find($path)
    {
        $path = trim($path, PropertyAccessor::OBJECT_ACCESSOR);

        $iterator = new \RecursiveIteratorIterator(
            new ParentNodeDefinitionIterator(
                $this->getRoot()
            ),
            \RecursiveIteratorIterator::SELF_FIRST
        );

        /** @var NodeDefinitionInterface|NodePathFactoryInterface $definition */
        foreach ($iterator as $definition) {

            if ($path === $definition->getPath()) {

                return $definition;
            }
        }

        throw new \RuntimeException(
            sprintf("Node definition hasn't been found by path: %s", $path)
        );
    }

    /**
     * @return NodeInterface
     */
    public function buildTree()
    {
        return $this->factory->createNode($this->getRoot());
    }

    /**
     * @param TreeBuilder $builder
     */
    public function __invoke(TreeBuilder $builder)
    {
        $root = $builder->root($this->getRoot()->getType());

        $constraints = $this->getRoot()->getConstraints();
        foreach ($constraints as $constraint) {

            $root->constraints()->append(clone $constraint);
        }

        $children = $this->getRoot()->getChildren();
        foreach ($children as $node) {

            $root->children()->append(clone $node);
        }
    }
}
