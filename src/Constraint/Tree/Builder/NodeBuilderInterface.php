<?php

namespace Mia\PHPUnit\Constraint\Tree\Builder;

use Mia\PHPUnit\Constraint\Tree\Definition\NodeDefinitionInterface;
use Mia\PHPUnit\Constraint\Tree\Definition\ParentNodeDefinitionInterface;

/**
 * Interface NodeBuilderInterface
 * @package Mia\PHPUnit\Constraint\Tree\Builder
 */
interface NodeBuilderInterface
{
    /**
     * @param ParentNodeDefinitionInterface|null $parent
     * @return mixed
     */
    public function setParent(ParentNodeDefinitionInterface $parent = null);

    /**
     * @param NodeDefinitionInterface $node
     */
    public function append(NodeDefinitionInterface $node);

    /**
     * @param $name
     * @param $type
     * @return NodeDefinitionInterface
     */
    public function node($name, $type);

    /**
     * @param $name
     * @return NodeDefinitionInterface|ParentNodeDefinitionInterface
     */
    public function arrayNode($name);

    /**
     * @param $name
     * @return NodeDefinitionInterface|ParentNodeDefinitionInterface
     */
    public function objectNode($name);

    /**
     * @param $name
     * @return NodeDefinitionInterface
     */
    public function scalarNode($name);

    /**
     * @return NodeBuilderInterface
     */
    public function clear();

    /**
     * @return ParentNodeDefinitionInterface
     */
    public function end();
}
