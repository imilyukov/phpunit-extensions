<?php

namespace Mia\PHPUnit\Constraint\Tree\Builder;

use Mia\PHPUnit\Constraint\Tree\Definition\ArrayNodeDefinition;
use Mia\PHPUnit\Constraint\Tree\Definition\NodeDefinitionFactory;
use Mia\PHPUnit\Constraint\Tree\Definition\NodeDefinitionInterface;
use Mia\PHPUnit\Constraint\Tree\Definition\ObjectNodeDefinition;
use Mia\PHPUnit\Constraint\Tree\Definition\ParentNodeDefinitionInterface;
use Mia\PHPUnit\Constraint\Tree\Definition\ScalarNodeDefinition;

/**
 * Class NodeBuilder
 * @package Mia\PHPUnit\Constraint\Tree\Builder
 */
class NodeBuilder implements NodeBuilderInterface
{
    /**
     * @var ParentNodeDefinitionInterface
     */
    protected $parent;

    /**
     * @var NodeDefinitionFactory
     */
    protected $factory;

    /**
     * NodeBuilder constructor.
     * @param NodeDefinitionFactory|null $factory
     */
    public function __construct(NodeDefinitionFactory $factory = null)
    {
        $this->factory = $factory ?: new NodeDefinitionFactory();
    }

    /**
     * @param ParentNodeDefinitionInterface|null $parent
     * @return $this
     */
    public function setParent(ParentNodeDefinitionInterface $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @param NodeDefinitionInterface $node
     */
    public function append(NodeDefinitionInterface $node)
    {
        if ($node instanceof ParentNodeDefinitionInterface) {
            $builder = clone $this;
            $builder->setParent($node);
            $node->setNodeBuilder($builder);
        }

        if ($this->parent) {
            $this->parent->addChild($node);
        }
    }

    /**
     * @param $name
     * @param $type
     * @return NodeDefinitionInterface
     */
    public function node($name, $type)
    {
        $node = $this->factory->createNodeDefinition($type, [$name]);

        $this->append($node);

        return $node;
    }

    /**
     * @param $name
     * @return NodeDefinitionInterface
     */
    public function arrayNode($name)
    {
        return $this->node($name, ArrayNodeDefinition::DEFINITION_TYPE);
    }

    /**
     * @param $name
     * @return NodeDefinitionInterface
     */
    public function objectNode($name)
    {
        return $this->node($name, ObjectNodeDefinition::DEFINITION_TYPE);
    }

    /**
     * @param $name
     * @return NodeDefinitionInterface
     */
    public function scalarNode($name)
    {
        return $this->node($name, ScalarNodeDefinition::DEFINITION_TYPE);
    }

    /**
     * @return NodeBuilderInterface
     */
    public function clear()
    {
        $this->parent->setChildren([]);

        return $this;
    }

    /**
     * @return ParentNodeDefinitionInterface
     */
    public function end()
    {
        return $this->parent;
    }

    /**
     * @param $name
     * @param $arguments
     * @return NodeDefinitionInterface
     */
    public function __call($name, $arguments)
    {
        if (!preg_match('/^(.+)Node$/', $name, $match)) {
            throw new \BadMethodCallException(
                sprintf("Method %s doesn't support for %s", $name, __CLASS__)
            );
        }

        return $this->node(array_shift($arguments), $match[1]);
    }

    /**
     * @param NodeBuilderAwareInterface $builderAware
     */
    public function __invoke(NodeBuilderAwareInterface $builderAware)
    {
        $children = $this->parent->getChildren();
        foreach ($children as $node) {

            $builderAware->getNodeBuilder()->append(clone $node);
        }
    }
}
