<?php

namespace Mia\PHPUnit\Constraint\Tree;

use Mia\PHPUnit\Constraint\Tree\Definition\NodeDefinitionInterface;

/**
 * Interface NodeFactoryInterface
 * @package Mia\PHPUnit\Constraint\Tree
 */
interface NodeFactoryInterface
{
    /**
     * @param NodeDefinitionInterface $definition
     * @return NodeInterface
     */
    public function createNode(NodeDefinitionInterface $definition);
}
