<?php

namespace Mia\PHPUnit\Constraint\Tree;

/**
 * Interface NodeInterface
 * @package Mia\PHPUnit\Constraint\Tree
 */
interface NodeInterface extends \ArrayAccess, \IteratorAggregate, \Countable
{
    /**
     * @return mixed
     */
    public function getName();

    /**
     * @return mixed
     */
    public function getType();

    /**
     * @return mixed
     */
    public function getPath();

    /**
     * @param NodeInterface $parent
     * @return NodeInterface
     */
    public function setParent(NodeInterface $parent = null);

    /**
     * @return \PHPUnit_Framework_Constraint[]
     */
    public function getConstraints();

    /**
     * @param \PHPUnit_Framework_Constraint $constraint
     * @return mixed
     */
    public function addConstraint(\PHPUnit_Framework_Constraint $constraint);
}
