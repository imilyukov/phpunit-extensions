<?php

namespace Mia\PHPUnit\Constraint\Tree;

use Mia\PHPUnit\Constraint\Tree\Factory\NodePathFactoryInterface;
use Mia\PHPUnit\Constraint\Tree\Factory\NodePathFactoryTrait;

/**
 * Class BaseNode
 * @package Mia\PHPUnit\Constraint\Tree
 */
class Node implements NodeInterface, NodePathFactoryInterface
{
    use NodePathFactoryTrait;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var \ArrayIterator|NodeInterface[]
     */
    protected $iterator;

    /**
     * @var NodeInterface|null
     */
    protected $parent;

    /**
     * @var \PHPUnit_Framework_Constraint[]
     */
    protected $constraints = [];

    /**
     * Node constructor.
     * @param $name
     * @param $type
     */
    public function __construct($name, $type)
    {
        $this->name = $name;
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param \PHPUnit_Framework_Constraint $constraint
     * @return $this
     */
    public function addConstraint(\PHPUnit_Framework_Constraint $constraint)
    {
        $this->constraints[] = $constraint;

        return $this;
    }

    /**
     * @return \PHPUnit_Framework_Constraint[]
     */
    public function getConstraints()
    {
        return $this->constraints;
    }

    /**
     * @param NodeInterface $parent
     * @return NodeInterface
     */
    public function setParent(NodeInterface $parent = null)
    {
        $this->parent = $parent;
        $this->parent->offsetSet($this->getName(), $this);

        return $this;
    }

    /**
     * @return null|NodeInterface
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->getIterator()->count();
    }

    /**
     * @return \ArrayIterator|NodeInterface[]
     */
    public function getIterator()
    {
        if (!$this->iterator) {
            $this->iterator = new \ArrayIterator();
        }

        return $this->iterator;
    }

    /**
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return $this->getIterator()->offsetExists($offset);
    }

    /**
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        return $this->getIterator()->offsetGet($offset);
    }

    /**
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        $this->getIterator()->offsetSet($offset, $value);
    }

    /**
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        $this->getIterator()->offsetUnset($offset);
    }
}
