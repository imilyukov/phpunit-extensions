<?php

namespace Mia\PHPUnit\Constraint\Tree\Factory;

use Mia\PHPUnit\Utility\Property\PropertyAccessor;

/**
 * Class NodePathFactoryTrait
 * @package Mia\PHPUnit\Constraint\Tree\Factory
 */
trait NodePathFactoryTrait
{
    /**
     * @return string
     */
    abstract public function getName();

    /**
     * @return NodePathFactoryInterface
     */
    abstract public function getParent();

    /**
     * @return string
     */
    public function getPath()
    {
        $path   = $this->getName();
        $parent = $this->getParent();

        if ($parent instanceof NodePathFactoryInterface) {

            $path = $path ? PropertyAccessor::OBJECT_ACCESSOR . $path : PropertyAccessor::ARRAY_ACCESSOR;
            $path = $parent->getPath() . $path;
        }

        return $path;
    }
}
