<?php

namespace Mia\PHPUnit\Constraint\Tree\Factory;

/**
 * Interface NodePathFactoryInterface
 * @package Mia\PHPUnit\Constraint\Tree\Factory
 */
interface NodePathFactoryInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return NodePathFactoryInterface
     */
    public function getParent();

    /**
     * @return string
     */
    public function getPath();
}
