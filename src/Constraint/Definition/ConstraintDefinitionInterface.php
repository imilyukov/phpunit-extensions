<?php

namespace Mia\PHPUnit\Constraint\Definition;

/**
 * Interface ConstraintDefinitionInterface
 * @package Mia\PHPUnit\Constraint\Definition
 */
interface ConstraintDefinitionInterface
{
    /**
     * @param ConstraintDefinitionOwnerInterface|null $owner
     * @return mixed
     */
    public function setOwner(ConstraintDefinitionOwnerInterface $owner = null);

    /**
     * @return mixed
     */
    public function getType();

    /**
     * @return mixed
     */
    public function getParameters();

    /**
     * @return mixed
     */
    public function getInitializer();

    /**
     * @param \Closure|null $initializer
     * @return mixed
     */
    public function setInitializer(\Closure $initializer = null);

    /**
     * @return mixed
     */
    public function getMessage();

    /**
     * @param $message
     * @return mixed
     */
    public function message($message);

    /**
     * @return mixed
     */
    public function end();
}
