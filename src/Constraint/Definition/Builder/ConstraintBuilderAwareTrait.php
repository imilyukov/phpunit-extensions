<?php

namespace Mia\PHPUnit\Constraint\Definition\Builder;

/**
 * Class ConstraintBuilderAwareTrait
 * @package Mia\PHPUnit\Constraint\Definition\Builder
 */
trait ConstraintBuilderAwareTrait
{
    /**
     * @var ConstraintBuilderInterface|null
     */
    protected $constraintBuilder;

    /**
     * @return ConstraintBuilderInterface
     */
    public function getConstraintBuilder()
    {
        if (!$this->constraintBuilder) {
            $this->constraintBuilder = new ConstraintBuilder();
        }

        return $this->constraintBuilder;
    }

    /**
     * @param ConstraintBuilderInterface|null $builder
     * @return mixed
     */
    public function setConstraintBuilder(ConstraintBuilderInterface $builder = null)
    {
        $this->constraintBuilder = $builder;

        return $this;
    }
}
