<?php

namespace Mia\PHPUnit\Constraint\Definition\Builder;

use Mia\PHPUnit\Constraint\Definition\ConstraintDefinitionInterface;
use Mia\PHPUnit\Constraint\Definition\ConstraintDefinitionOwnerInterface;

/**
 * Interface ConstraintBuilderInterface
 * @package Mia\PHPUnit\Constraint\Definition\Builder
 */
interface ConstraintBuilderInterface
{
    /**
     * @param ConstraintDefinitionOwnerInterface|null $owner
     * @return mixed
     */
    public function setOwner(ConstraintDefinitionOwnerInterface $owner = null);

    /**
     * @param ConstraintDefinitionInterface $constraint
     */
    public function append(ConstraintDefinitionInterface $constraint);

    /**
     * @param $type
     * @param array $parameters
     * @return mixed
     */
    public function assert($type, array $parameters = []);

    /**
     * @return mixed
     */
    public function clear();

    /**
     * @return ConstraintDefinitionOwnerInterface
     */
    public function end();
}
