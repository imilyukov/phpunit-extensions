<?php

namespace Mia\PHPUnit\Constraint\Definition\Builder;

use Mia\PHPUnit\Constraint\Definition\ConstraintDefinitionFactory;
use Mia\PHPUnit\Constraint\Definition\ConstraintDefinitionFactoryInterface;
use Mia\PHPUnit\Constraint\Definition\ConstraintDefinitionInterface;
use Mia\PHPUnit\Constraint\Definition\ConstraintDefinitionOwnerInterface;

/**
 * Class ConstraintBuilder
 * @package Mia\PHPUnit\Constraint\Definition\Builder
 */
class ConstraintBuilder implements ConstraintBuilderInterface
{
    /**
     * @var ConstraintDefinitionOwnerInterface
     */
    protected $owner;

    /**
     * @var null|ConstraintDefinitionFactory
     */
    protected $factory;

    /**
     * ConstraintBuilder constructor.
     * @param ConstraintDefinitionFactoryInterface|null $factory
     */
    public function __construct(ConstraintDefinitionFactoryInterface $factory = null)
    {
        $this->factory = $factory ?: new ConstraintDefinitionFactory();
    }

    /**
     * @param ConstraintDefinitionOwnerInterface|null $owner
     * @return $this
     */
    public function setOwner(ConstraintDefinitionOwnerInterface $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @param ConstraintDefinitionInterface $constraint
     */
    public function append(ConstraintDefinitionInterface $constraint)
    {
        if ($constraint instanceof ConstraintDefinitionOwnerInterface) {
            $builder = clone $this;
            $builder->setOwner($constraint);
            $constraint->setConstraintBuilder($builder);
        }

        if ($this->owner) {
            $this->owner->addConstraint($constraint);
        }
    }

    /**
     * @param $type
     * @param array $parameters
     * @return ConstraintDefinitionInterface
     */
    public function assert($type, array $parameters = [])
    {
        $constraint = $this->factory->createConstraintDefinition($type, $parameters);

        $this->append($constraint);

        return $constraint;
    }

    /**
     * @return ConstraintDefinitionOwnerInterface
     */
    public function end()
    {
        return $this->owner;
    }

    /**
     * @param $name
     * @param $arguments
     * @return ConstraintDefinitionInterface
     */
    public function __call($name, $arguments)
    {
        if (!preg_match('/^assert(.+)$/', $name, $match)) {
            throw new \BadMethodCallException(
                sprintf("Method %s doesn't support for %s", $name, __CLASS__)
            );
        }

        return $this->assert($match[1], $arguments);
    }

    /**
     * @return mixed
     */
    public function clear()
    {
        $this->owner->setConstraints([]);

        return $this;
    }

    /**
     * @param ConstraintBuilderAwareInterface $builderAware
     */
    public function __invoke(ConstraintBuilderAwareInterface $builderAware)
    {
        foreach ($this->owner->getConstraints() as $constraint) {

            $builderAware->getConstraintBuilder()->append(clone $constraint);
        }
    }
}
