<?php

namespace Mia\PHPUnit\Constraint\Definition\Builder;

/**
 * Interface ConstraintBuilderAwareInterface
 * @package Mia\PHPUnit\Constraint\Definition\Builder
 */
interface ConstraintBuilderAwareInterface
{
    /**
     * @return ConstraintBuilderInterface
     */
    public function getConstraintBuilder();

    /**
     * @param ConstraintBuilderInterface|null $builder
     * @return mixed
     */
    public function setConstraintBuilder(ConstraintBuilderInterface $builder = null);
}
