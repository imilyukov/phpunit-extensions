<?php

namespace Mia\PHPUnit\Constraint\Definition;

use Mia\PHPUnit\Constraint\Factory\ConstraintFactoryInterface;

/**
 * Class ConstraintDefinitionFactory
 * @package Mia\PHPUnit\Constraint\Definition
 */
class ConstraintDefinitionFactory implements ConstraintDefinitionFactoryInterface
{
    /**
     * @param $type
     * @param array $parameters
     *
     * @return ConstraintDefinitionInterface
     */
    public function createConstraintDefinition($type, array $parameters = [])
    {
        if (in_array($type, AggregateConstraintDefinition::getAggregationTypes())) {

            $definition = new AggregateConstraintDefinition(
                $type,
                $parameters
            );
            $definition->setInitializer(
                function (\PHPUnit_Framework_Constraint $constraint, ConstraintFactoryInterface $factory) use ($definition) {

                    if (!$definition->getIterator()->count()) {
                        return;
                    }

                    if (method_exists($constraint, 'setConstraints')) {

                        $constraints = [];

                        foreach ($definition->getConstraints() as $child) {

                            $constraints[] = $factory->createConstraint(
                                $child->getType(),
                                $child->getParameters(),
                                $child->getInitializer()
                            );
                        }

                        $constraint->__construct();
                        $constraint->setConstraints($constraints);

                        return;
                    }

                    // For constraint "Not"
                    if ($constraint instanceof \PHPUnit_Framework_Constraint_Not) {

                        $current = $definition->getIterator()->current();

                        $constraint->__construct(
                            $factory->createConstraint(
                                $current->getType(),
                                $current->getParameters(),
                                $current->getInitializer()
                            )
                        );
                    }
                }
            );

            return $definition;
        }

        return new ConstraintDefinition($type, $parameters);
    }
}
