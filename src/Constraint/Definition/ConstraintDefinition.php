<?php

namespace Mia\PHPUnit\Constraint\Definition;

/**
 * Class ConstraintDefinition
 * @package Mia\PHPUnit\Constraint\Definition
 */
class ConstraintDefinition implements ConstraintDefinitionInterface
{
    /**
     * @var ConstraintDefinitionOwnerInterface
     */
    protected $owner;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var array
     */
    protected $parameters;

    /**
     * @var \Closure|null
     */
    protected $initializer;

    /**
     * @var string
     */
    protected $message;

    /**
     * ConstraintDefinition constructor.
     * @param $type
     * @param array $parameters
     */
    public function __construct($type, array $parameters = [])
    {
        $this->type       = $type;
        $this->parameters = $parameters;
    }

    /**
     * @return \PHPUnit_Framework_Constraint
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @return \Closure
     */
    public function getInitializer()
    {
        return $this->initializer;
    }

    /**
     * @param \Closure|null $initializer
     * @return mixed
     */
    public function setInitializer(\Closure $initializer = null)
    {
        $this->initializer = $initializer;

        return $this;
    }

    /**
     * @param ConstraintDefinitionOwnerInterface|null $owner
     * @return $this
     */
    public function setOwner(ConstraintDefinitionOwnerInterface $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param $message
     * @return mixed
     */
    public function message($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return mixed
     */
    public function end()
    {
        return $this->owner ? $this->owner->getConstraintBuilder() : null;
    }

    /**
     * @param \PHPUnit_Framework_Constraint $constraint
     */
    public function __invoke(\PHPUnit_Framework_Constraint $constraint)
    {
        $this->getInitializer()->__invoke($constraint);
    }
}
