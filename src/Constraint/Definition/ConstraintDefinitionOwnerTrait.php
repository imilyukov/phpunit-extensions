<?php

namespace Mia\PHPUnit\Constraint\Definition;

/**
 * Class ConstraintDefinitionOwnerTrait
 * @package Mia\PHPUnit\Constraint\Definition
 */
trait ConstraintDefinitionOwnerTrait
{
    /**
     * @var Builder\ConstraintBuilderInterface|null
     */
    protected $constraintBuilder;

    /**
     * @var ConstraintDefinitionInterface[]
     */
    protected $constraints = [];

    /**
     * @return Builder\ConstraintBuilderInterface
     */
    public function getConstraintBuilder()
    {
        if (!$this->constraintBuilder) {
            $this->constraintBuilder = new Builder\ConstraintBuilder();
            $this->constraintBuilder->setOwner($this);
        }

        return $this->constraintBuilder;
    }

    /**
     * @param Builder\ConstraintBuilderInterface|null $builder
     * @return mixed
     */
    public function setConstraintBuilder(Builder\ConstraintBuilderInterface $builder = null)
    {
        $this->constraintBuilder = $builder;

        return $this;
    }

    /**
     * @param ConstraintDefinitionInterface $constraint
     * @return $this
     */
    public function addConstraint(ConstraintDefinitionInterface $constraint)
    {
        $this->constraints[] = $constraint->setOwner($this);

        return $this;
    }

    /**
     * @return ConstraintDefinitionInterface[]
     */
    public function getConstraints()
    {
        return $this->constraints;
    }

    /**
     * @param ConstraintDefinitionInterface[] $constraints
     * @return $this
     */
    public function setConstraints(array $constraints)
    {
        $this->constraints = [];

        foreach ($constraints as $constraint) {

            $this->addConstraint($constraint);
        }

        return $this;
    }

    /**
     * @return Builder\ConstraintBuilderInterface
     */
    public function constraints()
    {
        return $this->getConstraintBuilder();
    }

    /**
     * Clone
     */
    public function __clone()
    {
        $this->setConstraintBuilder(null);

        $constraints = $this->getConstraints();

        $this->constraints()->clear();

        foreach ($constraints as $constraint) {

            $this->constraints()->append(clone $constraint);
        }
    }
}
