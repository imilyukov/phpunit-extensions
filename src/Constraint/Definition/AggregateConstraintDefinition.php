<?php

namespace Mia\PHPUnit\Constraint\Definition;

/**
 * Class AggregateConstraintDefinition
 * @package Mia\PHPUnit\Constraint\Definition
 */
class AggregateConstraintDefinition extends ConstraintDefinition implements ConstraintDefinitionOwnerInterface,
    \IteratorAggregate
{
    use ConstraintDefinitionOwnerTrait;

    /**
     * @return array
     */
    public static function getAggregationTypes()
    {
        return ['And', 'Or', 'Xor', 'Not'];
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->getConstraints());
    }
}
