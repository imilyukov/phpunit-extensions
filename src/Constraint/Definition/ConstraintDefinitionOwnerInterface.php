<?php

namespace Mia\PHPUnit\Constraint\Definition;

/**
 * Interface ConstraintDefinitionOwnerInterface
 * @package Mia\PHPUnit\Constraint\Definition
 */
interface ConstraintDefinitionOwnerInterface extends Builder\ConstraintBuilderAwareInterface
{
    /**
     * @return Builder\ConstraintBuilderInterface
     */
    public function constraints();

    /**
     * @param ConstraintDefinitionInterface $constraint
     * @return mixed
     */
    public function addConstraint(ConstraintDefinitionInterface $constraint);

    /**
     * @return ConstraintDefinitionInterface[]
     */
    public function getConstraints();

    /**
     * @param ConstraintDefinitionInterface[] $constraints
     * @return mixed
     */
    public function setConstraints(array $constraints);
}
