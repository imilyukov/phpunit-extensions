<?php

namespace Mia\PHPUnit\Constraint\Definition;

/**
 * Interface ConstraintDefinitionFactoryInterface
 * @package Mia\PHPUnit\Constraint\Definition
 */
interface ConstraintDefinitionFactoryInterface
{
    /**
     * @param $type
     * @param array $parameters
     *
     * @return ConstraintDefinitionInterface
     */
    public function createConstraintDefinition($type, array $parameters = []);
}
